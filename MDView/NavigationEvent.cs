﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView
{
  /// <summary>
  /// Event handler for navigation requests
  /// </summary>
  /// <param name="sender"></param>
  /// <param name="args"></param>
  public delegate void NavigationEventHandler(object sender, NavigationEventArgs args);

  /// <summary>
  /// Event data for navigation requests
  /// </summary>
  public class NavigationEventArgs : EventArgs
  {
    /// <summary>
    /// Control that should be displayed
    /// </summary>
    public readonly Util.Either<INavigableControl, NavigationDestination> Destination;

    /// <summary>
    /// Request screen to switch to the supplied control
    /// </summary>
    /// <param name="destination"></param>
    public NavigationEventArgs(INavigableControl destination)
    {
      Destination = new Util.Either<INavigableControl, NavigationDestination>.Left(destination);
    }

    /// <summary>
    /// Request screen switch to the relative control
    /// </summary>
    /// <param name="destination"></param>
    public NavigationEventArgs(NavigationDestination destination)
    {
      Destination = new Util.Either<INavigableControl, NavigationDestination>.Right(destination);
    }
  }

  /// <summary>
  /// Should be an abstract class extending UserControl but it's not worth the effort
  /// </summary>
  public interface INavigableControl
  {
    event NavigationEventHandler NavigationRequested;

    Control Control { get; }

    String Title { get; }

    void OnShow();
  }

  /// <summary>
  /// Implicit navigation target
  /// </summary>
  public enum NavigationDestination {
    /// <summary>
    /// Previous page
    /// </summary>
    Back,
    /// <summary>
    /// Root page
    /// </summary>
    Root
  }
}
