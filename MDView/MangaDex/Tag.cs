﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Content tag
  /// </summary>
  [DataContract]
  public class Tag
  {
    [DataMember(Name = "name")]
    public Dictionary<string, string> Name { get; set; }

    // Omitting the remaining fields, these do not appear to be used presently
    // and the "description" field in the response does not match the spec.
  }
}
