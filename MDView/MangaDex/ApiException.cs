﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.MangaDex
{
  /// <summary>
  /// Details of exception thrown from MangaDex API
  /// </summary>
  public class ApiException : Exception
  {
    /// <summary>
    /// API error data
    /// </summary>
    public readonly Error Error;

    public ApiException(Error error)
      : base(error.Title + ": " + error.Detail)
    {
      Error = error;
    }

    /// <summary>
    /// If multiple errors are returned from the API, produce an AggregateException.
    /// 
    /// Otherwise produce an ApiException.
    /// </summary>
    /// <param name="errorResponse"></param>
    /// <returns></returns>
    public static Exception FromResponse(ErrorResponse errorResponse)
    {
      var exs = errorResponse.Errors.Select(e => new ApiException(e));

      if (exs.Count() == 1)
        return exs.First();
      else
        return new AggregateException(exs);
    }
  }
}
