﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  [DataContract]
  public class Relationship
  {
    /// <summary>
    /// Identifier
    /// </summary>
    [DataMember(Name = "id")]
    public Guid Id { get; set; }

    /// <summary>
    /// Type as received from the API.
    /// 
    /// Prefer the ObjectType field unless returned as Unknown.
    /// </summary>
    [DataMember(Name = "type")]
    public string Type { get; set; }

    /// <summary>
    /// Type of object being referenced
    /// </summary>
    public ObjectType ObjectType
    {
      get
      {
        return Type.ToObjectType();
      }
    }

    /// <summary>
    /// Type of the relation.
    /// 
    /// Prefer the RelationshipType field unless returned as Unknown
    /// </summary>
    [DataMember(Name = "related")]
    public string Related { get; set; }

    /// <summary>
    /// Type of the relation.
    /// 
    /// Only meaningful when both objects are of type Manga.
    /// </summary>
    public RelationshipType RelationshipType
    {
      get
      {
        return Related.ToRelationshipType();
      }
    }
  }
}
