﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.MangaDex
{
  /// <summary>
  /// Manga content rating
  /// </summary>
  public enum ContentRating
  {
    Safe,
    Suggestive,
    Erotica,
    Pornographic,
    Unknown,
  }
}
