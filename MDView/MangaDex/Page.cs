﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MDView.MangaDex
{
  /// <summary>
  /// Details of a Page loaded from an MD@H server
  /// </summary>
  public class Page
  {
    /// <summary>
    /// Ellapsed time
    /// </summary>
    internal TimeSpan Duration;

    /// <summary>
    /// Data stream
    /// </summary>
    internal System.IO.Stream Stream;

    /// <summary>
    /// URL loaded
    /// </summary>
    internal string Url;

    /// <summary>
    /// True if request was served from cache
    /// </summary>
    internal bool Cached;

    /// <summary>
    /// Image file loaded from the stream
    /// </summary>
    public Image Image;

    /// <summary>
    /// Create page data from HTTP response
    /// </summary>
    /// <param name="url"></param>
    /// <param name="response"></param>
    /// <param name="stream"></param>
    /// <param name="duration"></param>
    public Page(string url, HttpResponseMessage response, System.IO.Stream stream, TimeSpan duration)
    {
      Url = url;
      Duration = duration;
      Stream = stream;
      Image = Image.FromStream(stream);

      if (response.Headers.TryGetValues("X-Cache", out var values)) {
        Cached = values.Contains("HIT");
      }
    }
  }
}
