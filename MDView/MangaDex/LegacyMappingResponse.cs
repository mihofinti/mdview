﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Response from the Legacy Mapping API
  /// </summary>
  [DataContract]
  public class LegacyMappingResponse
  {
    /// <summary>
    /// Type of object translated.
    /// 
    /// One of the LegacyMappingType constants
    /// </summary>
    [DataMember(Name = "type")]
    public string Type;

    /// <summary>
    /// Legacy ID
    /// </summary>
    [DataMember(Name = "legacyId")]
    public int LegacyId;

    /// <summary>
    /// New ID
    /// </summary>
    [DataMember(Name = "newId")]
    public Guid NewId;
  }
}
