﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// MangaDex user account
  /// </summary>
  [DataContract]
  public class User
  {
    /// <summary>
    /// Name of the user
    /// </summary>
    [DataMember(Name ="username")]
    public string UserName { get; set; }
  }
}
