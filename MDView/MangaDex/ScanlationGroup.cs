﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Scanlation group
  /// </summary>
  [DataContract]
  public class ScanlationGroup
  {
    /// <summary>
    /// Group name
    /// </summary>
    [DataMember(Name = "name")]
    public string Name { get; set; }

    /// <summary>
    /// User account that administers the group
    /// </summary>
    [DataMember(Name = "leader")]
    public ObjectData<User> Leader { get; set; }

    [DataMember(Name = "createdAt")]
    private string CreatedAtApi
    {
      get
      {
        return CreatedAt.HasValue ? CreatedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          CreatedAt = dateTime;
        else
          CreatedAt = null;
      }
    }

    /// <summary>
    /// Date created
    /// </summary>
    public DateTime? CreatedAt { get; set; }

    [DataMember(Name = "updatedAt")]
    private string UpdatedAtApi
    {
      get
      {
        return UpdatedAt.HasValue ? UpdatedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          UpdatedAt = dateTime;
        else
          UpdatedAt = null;
      }
    }

    /// <summary>
    /// Date last updated
    /// </summary>
    public DateTime? UpdatedAt { get; set; }

    /// <summary>
    /// Group website
    /// </summary>
    [DataMember(Name = "website")]
    public string Website { get; set; }

    /// <summary>
    /// IRC server
    /// </summary>
    [DataMember(Name = "ircServer")]
    public string IrcServer { get; set; }

    /// <summary>
    /// IRC Channel name
    /// </summary>
    [DataMember(Name = "ircChannel")]
    public string IrcChannel { get; set; }

    /// <summary>
    /// Discord group id
    /// </summary>
    [DataMember(Name = "discord")]
    public string Discord { get; set; }

    /// <summary>
    /// Email address
    /// </summary>
    [DataMember(Name = "contactEmail")]
    public string ContactEmail { get; set; }

    /// <summary>
    /// Group Twitter
    /// </summary>
    [DataMember(Name = "twitter")]
    public string Twitter { get; set; }

    /// <summary>
    /// Additional group information
    /// </summary>
    [DataMember(Name = "description")]
    public string Description { get; set; }

    /// <summary>
    /// Is this an official translator
    /// </summary>
    [DataMember(Name = "official")]
    public bool Official { get; set; }

    public override string ToString()
    {
      return String.Format("ScanlationGroup:{0}", Name);
    }
  }
}
