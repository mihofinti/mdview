﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.MangaDex
{
  /// <summary>
  /// Identifies the type of data referred to by an identifier
  /// </summary>
  public enum ObjectType
  {
    Manga,
    Chapter,
    CoverArt,
    Author,
    Artist,
    ScanlationGroup,
    Tag,
    User,
    Unknown,
  }

  public static partial class EnumTranslations
  {
    private static readonly string[] ObjectTypeApiValues = { "manga", "chapter", "cover_art", "author", "artist", "scanlation_group", "tag", "user" };

    public static string ToApi(this ObjectType objectType)
    {
      if ((int)objectType >= 0 && (int)objectType < ObjectTypeApiValues.Length)
        return ObjectTypeApiValues[(int)objectType];
      else
        return null;
    }

    public static ObjectType ToObjectType(this string objectType) {
      for (int i = 0; i < ObjectTypeApiValues.Length; i++) {
        if (ObjectTypeApiValues[i] == objectType)
          return (ObjectType)i;
      }
      return ObjectType.Unknown;
    }
  }
}
