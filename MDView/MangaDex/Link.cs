﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.MangaDex
{
  /// <summary>
  /// Defined link targets
  /// </summary>
  public enum LinkTarget
  {
    AniList,
    AnimePlanet,
    BookWalkerJp,
    MangaUpdates,
    NovelUpdates,
    KitsuIo,
    Amazon,
    EBookJapan,
    MyAnimeList,
    CdJapan,
    Raw,
    OfficialTranslation,
    Unknown,
  }

  /// <summary>
  /// Encapsulates link data
  /// </summary>
  public class Link
  {
    /// <summary>
    /// External site the link targets
    /// </summary>
    public readonly LinkTarget Target;

    /// <summary>
    /// Raw value from the API
    /// </summary>
    private string Id;

    /// <summary>
    /// Linked URL
    /// </summary>
    public string Url
    {
      get
      {
        switch (Target) {
          case LinkTarget.AniList:
            return "https://anilist.co/manga/" + Id;

          case LinkTarget.AnimePlanet:
            return "https://www.anime-planet.com/manga/" + Id;

          case LinkTarget.BookWalkerJp:
            return "https://bookwalker.jp/" + Id;

          case LinkTarget.MangaUpdates:
            return "https://www.mangaupdates.com/series.html?id=" + Id;

          case LinkTarget.NovelUpdates:
            return "https://www.novelupdates.com/series/" + Id;

          case LinkTarget.KitsuIo:
            // Possibly wrong? don't have an example that uses Kitsu.io to validate
            return "https://kitsu.io/api/edge/manga/" + (Id.All(Char.IsDigit) ? Id : ("?filter[slug]=" + Id));

          case LinkTarget.MyAnimeList:
            return "https://myanimelist.net/manga/" + Id;

          default:
            return Id;
        }
      }
    }

    /// <summary>
    /// Initialize from API response
    /// </summary>
    /// <param name="code"></param>
    /// <param name="link"></param>
    public Link(string code, string link)
    {
      Target = ApiToTarget(code);
      Id = link;
    }

    /// Initialize from API response
    /// </summary>
    /// <param name="code"></param>
    /// <param name="link"></param>
    public Link(LinkTarget code, string link)
    {
      Target = code;
      Id = link;
    }
    
    /// <summary>
         /// Convert API codes to enumeration value
         /// </summary>
         /// <param name="target"></param>
         /// <returns></returns>
    public static LinkTarget ApiToTarget(string target)
    {
      switch (target) {
        case "al": return LinkTarget.AniList;
        case "ap": return LinkTarget.AnimePlanet;
        case "bw": return LinkTarget.BookWalkerJp;
        case "mu": return LinkTarget.MangaUpdates;
        case "nu": return LinkTarget.NovelUpdates;
        case "kt": return LinkTarget.KitsuIo;
        case "amz": return LinkTarget.Amazon;
        case "ebj": return LinkTarget.EBookJapan;
        case "mal": return LinkTarget.MyAnimeList;
        case "cdj": return LinkTarget.CdJapan;
        case "raw": return LinkTarget.Raw;
        case "engtl": return LinkTarget.OfficialTranslation;
        default: return LinkTarget.Unknown;
      }
    }

    /// <summary>
    /// Convert enumeration value to API code
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public static string TargetToApi(LinkTarget target)
    {
      switch (target) {
        case LinkTarget.AniList: return "al";
        case LinkTarget.AnimePlanet: return "ap";
        case LinkTarget.BookWalkerJp: return "bw";
        case LinkTarget.MangaUpdates: return "mu";
        case LinkTarget.NovelUpdates: return "nu";
        case LinkTarget.KitsuIo: return "kt";
        case LinkTarget.Amazon: return "amz";
        case LinkTarget.EBookJapan: return "ebj";
        case LinkTarget.MyAnimeList: return "mal";
        case LinkTarget.Raw: return "raw";
        case LinkTarget.OfficialTranslation: return "engtl";
        default: return null;
      }
    }

  }
}
