using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Http;
using System.Runtime.Serialization.Json;

using MDView.Util;

namespace MDView.MangaDex
{
  /// <summary>
  /// Proxy usage
  /// </summary>
  public enum UseProxy
  {
    /// <summary>
    /// Use proxy for all connections
    /// </summary>
    Always,
    /// <summary>
    /// Use proxy for mangadex.org only
    /// </summary>
    Mangadex
  }

  /// <summary>
  /// Wrapper around the MangaDex API
  /// </summary>
  public class Service : IDisposable
  {
    /// <summary>
    /// HTTP client
    /// </summary>
    private HttpClient HttpClient;

    /// <summary>
    /// HTTP client without proxy
    /// </summary>
    private HttpClient HttpClientPages;

    /// <summary>
    /// Authentication token
    /// </summary>
    private Token Token;

    /// <summary>
    /// API base URI
    /// </summary>
    private const string BASE_URI = "https://api.mangadex.org/";

    /// <summary>
    /// Timestamp of last request
    /// </summary>
    private DateTime LastQuery = DateTime.MinValue;

    /// <summary>
    /// MangaDex API is just that far off from using one of the .NET standard patterns
    /// </summary>
    public const string DATE_TIME_PATTERN = "yyyy'-'MM'-'dd'T'HH':'mm':'ssK";

    /// <summary>
    /// Make the JSON serializer behave reasonable with Objects
    /// </summary>
    private static DataContractJsonSerializerSettings DefaultJsonSettings = new DataContractJsonSerializerSettings() {
      UseSimpleDictionaryFormat = true
    };

    /// <summary>
    /// Proxy usage
    /// </summary>
    private UseProxy UseProxy;

    /// <summary>
    /// Minimum delay between requests.
    /// 
    /// The most restricted call we make is to GET/at-home/server/{id} which is restricted to 60/minute
    /// That can only happen in response to a UI request, so it should be safe to drop the limit to the 5/second
    /// global limit.
    /// </summary>
    private static readonly TimeSpan RateLimit = TimeSpan.FromSeconds(.2);

    /// <summary>
    /// Create new HTTP handler for the MangaDex API
    /// </summary>
    public Service( UseProxy useProxy, string proxyAddress, int proxyPort )
    {
      UseProxy = useProxy;

      var handler = new HttpClientHandler();
      handler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12;
      if( proxyAddress != null )
        handler.Proxy = new MihaZupan.HttpToSocks5Proxy(proxyAddress, proxyPort);

      HttpClient = new HttpClient(handler);
      HttpClient.Timeout = new TimeSpan(0, 1, 0);

      // Create non proxy connection pool if needed
      if (proxyAddress != null && UseProxy == UseProxy.Mangadex) {
        var h2 = new HttpClientHandler();
        h2.SslProtocols = System.Security.Authentication.SslProtocols.Tls12;
        HttpClientPages = new HttpClient(h2);
        HttpClientPages.Timeout = new TimeSpan(0, 1, 0);
      }
      else {
        HttpClientPages = HttpClient;
      }
    }

    public void Dispose()
    {
      HttpClient.Dispose();
      if (HttpClient != HttpClientPages)
        HttpClientPages.Dispose();
    }

    /// <summary>
    /// Wait until RateLimit has ellapsed since the last request
    /// </summary>
    private void ApplyRateLimit()
    {
      while (LastQuery.Add(RateLimit) > DateTime.Now) {
        System.Threading.Thread.Sleep(10);
      }

      LastQuery = DateTime.Now;
    }

    #region Utility
    /// <summary>
    /// Parse a MangaDex ID.
    /// 
    /// Accepts input in the form of:
    /// a legacy mangadex URL,
    /// a legacy all-numeric ID,
    /// a new UUID.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static Either<int, Guid> ParseUriOrId(string id)
    {
      if (id.Contains("mangadex")) {
        // URL, format varied over time and there mere multiple hostnames
        // So don't try to parse, just grab the first numeric string
        var start = id.IndexOfAny("0123456789".ToCharArray());
        if (start == -1) {
          throw new ArgumentException("URL does not contain a numeric ID");
        }

        int end;
        for (end = 0; start + end < id.Length; end++) {
          if (!Char.IsDigit(id[start + end]))
            break;
        }

        id = id.Substring(start, end);
      }

      if (id.All(Char.IsDigit))
        return new Either<int, Guid>.Left(int.Parse(id));
      else
        return new Either<int, Guid>.Right(Guid.Parse(id));
    }
    #endregion

    #region API Calls
    /// <summary>
    /// Login with the specified user credentials
    /// </summary>
    /// <param name="user"></param>
    /// <param name="password"></param>
    public void Login(string user, string password)
    {
      if (Token != null)
        return;

      var result = Request<Login, TokenResult>(HttpMethod.Post, BASE_URI + "auth/login", new Login() { UserName = user, Password = password });

      if (result.HasRight)
        Token = result.RightValue.Token;
      else
        throw ApiException.FromResponse(result.LeftValue);
    }

    /// <summary>
    /// Translate legacy ID to new ID
    /// </summary>
    /// <param name="type"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public Guid TranslateLegacyId(ObjectType type, int id)
    {
      var request = new LegacyMappingRequest(type, id);
      var result = Request<LegacyMappingRequest, SearchResponse<LegacyMappingResponse>>(HttpMethod.Post, BASE_URI + "legacy/mapping", request);

      return result.Bind(
        e => throw ApiException.FromResponse(e),
        r => r.Data.First().Attributes.NewId
        );
    }

    /// <summary>
    /// Get details of a manga with the specified ID
    /// </summary>
    /// <param name="mangaId"></param>
    /// <returns></returns>
    public ObjectData<Manga> GetManga(Guid mangaId)
    {
      var result = Request<ObjectResponse<Manga>>(HttpMethod.Get, BASE_URI + "manga/" + mangaId);

      return result.Bind(
        e => throw ApiException.FromResponse(e),
        r => r.Data);
    }

    /// <summary>
    /// API says maximum of 100 IDs/request, many HTTP libs assume a maximum length of URLs less than 4K
    /// In particular, their own gateway appears to be one of them and throws a 502 somewhere between 70-90
    /// So limit request length to prevent errors
    /// </summary>
    public const int MAX_BATCH_REQUESTS = 50;

    /// <summary>
    /// Get details of a manga with the specified ID using a bulk request.
    /// </summary>
    /// <param name="mangaId"></param>
    /// <returns></returns>
    public IEnumerable<ObjectData<Manga>> GetManga(IEnumerable<Guid> mangaIds)
    {
      var rv = new List<ObjectData<Manga>>();

      foreach (var idList in Util.Lists.TakeSubsets(MAX_BATCH_REQUESTS, mangaIds) ) {
        var query = "?limit=100&ids[]=" + string.Join("&ids[]=", idList);

        var result = Request<SearchResponse<Manga>>(HttpMethod.Get, BASE_URI + "manga" + query);

        result.Bind(
          e => throw ApiException.FromResponse(e),
          r => rv.AddRange( r.Data )
          );
      }

      return rv;
    }

    /// <summary>
    /// Search using the specified criteria.
    /// 
    /// The searchQuery's offset will be updated for the number of results received
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="searchQuery"></param>
    /// <returns></returns>
    public IEnumerable<ObjectData<T>> Search<T>(SearchQuery<T> searchQuery)
    {
      var rv = new List<ObjectData<T>>();

      var result = Request<SearchResponse<T>>(HttpMethod.Get, BASE_URI + searchQuery.BuildUri());

      result.Bind(
        e => throw ApiException.FromResponse(e),
        r => {
          searchQuery.Advance(r);
          rv.AddRange(r.Data );
        } );

      return rv;
    }

    /// <summary>
    /// Get chapters for a manga with the specified ID
    /// </summary>
    /// <param name="mangaId"></param>
    /// <returns></returns>
    public IEnumerable<ObjectData<Chapter>> GetChapters(Guid mangaId)
    {
      var query = new SearchQuery<Chapter>(String.Format("manga/{0}/feed", mangaId));
      query.Add("translatedLanguage[]", "en");
      query.Add("order[updatedAt]", "desc");
      query.Add("includeFutureUpdates", "0");

      var rv = new List<ObjectData<Chapter>>();

      while (query.HasMore)
        rv.AddRange(Search<Chapter>(query));

      return rv;
    }

    /// <summary>
    /// Get MD@H server to use for reading the specified chapter
    /// </summary>
    /// <param name="chapter"></param>
    /// <returns></returns>
    public AtHomeServerForChapter GetServerForChapter(Guid chapter)
    {
      var result = Request<AtHomeServerForChapter>(HttpMethod.Get, BASE_URI + "at-home/server/" + chapter);

      return result.Bind(
        e => throw ApiException.FromResponse(e),
        r => r);
    }

    /// <summary>
    /// Get page for a chapter from the MD@H server
    /// </summary>
    /// <param name="server"></param>
    /// <param name="chapter"></param>
    /// <param name="page"></param>
    /// <returns></returns>
    public Page GetPage(AtHomeServerForChapter server, string page)
    {
      var url = String.Format("{0}/data/{1}/{2}", server.BaseUrl, server.Chapter.Hash, page);
      var request = new HttpRequestMessage(HttpMethod.Get, url);
      if (Token != null)
        request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Token.Session);
      request.Headers.UserAgent.Add(new System.Net.Http.Headers.ProductInfoHeaderValue("MDView", "0"));

      ApplyRateLimit();
      try {
        var client = HttpClientPages;
        if (server.BaseUrl.Contains("mangadex.org"))  // Sometimes pages come from mangadex's server
          client = HttpClient;

        var start = DateTime.Now;
        var response = client.SendAsync(request).Sync();
        var stream = response.Content.ReadAsStreamAsync().Sync();
        var end = DateTime.Now;

        return new Page(url, response, stream, end - start);
      }
      catch (Exception ex) {
        NetworkStatus(new NetworkStatus(url));
        throw new AggregateException(ex);
      }
    }

    /// <summary>
    /// Report network status of a page request
    /// </summary>
    /// <param name="page"></param>
    public void NetworkStatus(Page page)
    {
      NetworkStatus(new NetworkStatus(page));
    }

    /// <summary>
    /// Report network status of a failed request
    /// </summary>
    /// <param name="status"></param>
    private void NetworkStatus(NetworkStatus status)
    {
      Request(HttpMethod.Post, "https://api.mangadex.network/report", status);
    }

    /// <summary>
    /// Get details of an author/artist with the specified ID using a bulk request.
    /// </summary>
    /// <param name="mangaId"></param>
    /// <returns></returns>
    public IEnumerable<ObjectData<Author>> GetAuthor(IEnumerable<Guid> authorIds)
    {
      var rv = new List<ObjectData<Author>>();

      // API says maximum of 100 IDs/request, many HTTP libs assume a maximum length of URLs less than 4K
      // In particular, their own gateway appears to be one of them and throws a 502 somewhere between 70-90
      // So limit request length to prevent errors
      foreach (var idList in Util.Lists.TakeSubsets(50, authorIds)) {
        var query = "?limit=100&ids[]=" + string.Join("&ids[]=", idList);

        var result = Request<SearchResponse<Author>>(HttpMethod.Get, BASE_URI + "author" + query);

        result.Bind(
          e => throw ApiException.FromResponse(e),
          r => rv.AddRange(r.Data)
          );
      }

      return rv;
    }

    /// <summary>
    /// Get details of a scanlation group with the specified ID using a bulk request.
    /// </summary>
    /// <param name="scanlationGroupIds"></param>
    /// <returns></returns>
    public IEnumerable<ObjectData<ScanlationGroup>> GetScanlationGroup(IEnumerable<Guid> scanlationGroupIds)
    {
      var rv = new List<ObjectData<ScanlationGroup>>();

      // API says maximum of 100 IDs/request, many HTTP libs assume a maximum length of URLs less than 4K
      // In particular, their own gateway appears to be one of them and throws a 502 somewhere between 70-90
      // So limit request length to prevent errors
      foreach (var idList in Util.Lists.TakeSubsets(50, scanlationGroupIds)) {
        var query = "?limit=100&ids[]=" + string.Join("&ids[]=", idList);

        var result = Request<SearchResponse<ScanlationGroup>>(HttpMethod.Get, BASE_URI + "group" + query);

        result.Bind(
          e => throw ApiException.FromResponse(e),
          r => rv.AddRange(r.Data)
          );
      }

      return rv;
    }

    /// <summary>
    /// Get the API schema from the server
    /// </summary>
    /// <returns></returns>
    public System.IO.Stream GetSchema()
    {
      var result = Request(HttpMethod.Get, BASE_URI + "api.yaml");

      return result.Bind(
        e => throw ApiException.FromResponse(e),
        r => r
        );
    }
    #endregion

    #region HTTP Methods
    /// <summary>
    /// Make a request of the API and return the stream
    /// </summary>
    /// <param name="method"></param>
    /// <param name="uri"></param>
    /// <param name="input"></param>
    /// <returns></returns>
    private Either<ErrorResponse,System.IO.Stream> Request(HttpMethod method, string uri, HttpContent input = null)
    {
      var request = new HttpRequestMessage(method, uri);
      if( Token != null )
        request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Token.Session);

      request.Headers.UserAgent.Add(new System.Net.Http.Headers.ProductInfoHeaderValue("MDView","0"));

      if( input != null )
        request.Content = input;

      ApplyRateLimit();
      Console.Log("> {0} {1}", method, uri);
      var response = HttpClient.SendAsync(request).Sync();
      Console.Log("< {0}", response.StatusCode);

      if (response.StatusCode == System.Net.HttpStatusCode.OK) {
        return new Util.Either<ErrorResponse, System.IO.Stream>.Right(response.Content.ReadAsStreamAsync().Sync());
      }
      else if (response.StatusCode == System.Net.HttpStatusCode.NoContent) {
        return new Util.Either<ErrorResponse, System.IO.Stream>.Right(null);
      }
      else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest) {
        var responseJson = new DataContractJsonSerializer(typeof(ErrorResponse), DefaultJsonSettings);
        var responseStream = response.Content.ReadAsStreamAsync().Sync();
        return new Util.Either<ErrorResponse, System.IO.Stream>.Left((ErrorResponse)responseJson.ReadObject(responseStream));
      }
      else {
        throw new Exception( "Failure from MangaDex API: " + response.StatusCode.ToString());
      }
    }

    /// <summary>
    /// Make a request of the mangadex API rendering JSON request data and receiving JSON response
    /// </summary>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="O"></typeparam>
    /// <param name="method"></param>
    /// <param name="uri"></param>
    /// <param name="input"></param>
    /// <returns></returns>
    public Either<ErrorResponse, O> Request<I, O>(HttpMethod method, string uri, I input)
    {
      var requestJson = new DataContractJsonSerializer(typeof(I), DefaultJsonSettings);
      HttpContent content;
      using (var stream = new System.IO.MemoryStream()) {
        requestJson.WriteObject(stream, input);
        content = new ByteArrayContent(stream.GetBuffer(), 0, (int)stream.Length);
        content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
      }

      var response = Request(method, uri, content);
      return response.Bind(e => e, s => {
        if (s == null)
          return default(O);

        var responseJson = new DataContractJsonSerializer(typeof(O), DefaultJsonSettings);
        return (O)responseJson.ReadObject(s);
      });
    }


    /// <summary>
    /// Make a request of the mangadex API rendering JSON request data and discarding the result
    /// </summary>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="O"></typeparam>
    /// <param name="method"></param>
    /// <param name="uri"></param>
    /// <param name="input"></param>
    /// <returns></returns>
    public void Request<I>(HttpMethod method, string uri, I input)
    {
      var requestJson = new DataContractJsonSerializer(typeof(I), DefaultJsonSettings);
      HttpContent content;
      using (var stream = new System.IO.MemoryStream()) {
        requestJson.WriteObject(stream, input);
        content = new ByteArrayContent(stream.GetBuffer(), 0, (int)stream.Length);
        content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
      }

      Request(method, uri, content);
    }


    /// <summary>
    /// Make a request of the mangadex API with no content and receiving JSON response
    /// </summary>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="O"></typeparam>
    /// <param name="method"></param>
    /// <param name="uri"></param>
    /// <param name="input"></param>
    /// <returns></returns>
    public Either<ErrorResponse, O> Request<O>(HttpMethod method, string uri)
    {
      var response = Request(method, uri);
      return response.Bind(e => e, s => {
        if (s == null)
          return default(O);

        var responseJson = new DataContractJsonSerializer(typeof(O), DefaultJsonSettings);
        return (O)responseJson.ReadObject(s);
      });
    }
    #endregion
  }
}
