﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Basic MangaDex API response data
  /// </summary>
  [DataContract]
  public class Response
  {
    [DataMember(Name = "result")]
    public string Result;
  }

  /// <summary>
  /// MangaDex API response including an object
  /// </summary>
  /// <typeparam name="T"></typeparam>
  [DataContract]
  public class ObjectResponse<T> : Response
  {
    [DataMember(Name = "data")]
    public ObjectData<T> Data;

    /// <summary>
    /// Other data objects related to this one
    /// </summary>
    public IEnumerable<Relationship> Relationships => Data.Relationships;

    /// <summary>
    /// Object data returned from the API
    /// </summary>
    public T Attributes => Data.Attributes;

    /// <summary>
    /// Identifier of the object returned from the API
    /// </summary>
    public Guid Id => Data.Id;

    public override string ToString()
    {
      return Data.ToString();
    }
  }

  /// <summary>
  /// Wrapper around returned object data including its ID and type
  /// </summary>
  /// <typeparam name="T"></typeparam>
  [DataContract]
  public class ObjectData<T>
  {
    [DataMember(Name = "id")]
    public Guid Id;

    [DataMember(Name = "type")]
    public string Type;

    [DataMember(Name = "attributes")]
    public T Attributes;

    [DataMember(Name = "relationships")]
    public IEnumerable<Relationship> Relationships;

    public override string ToString()
    {
      return String.Format("{0}@{1}", Attributes.ToString(), Id);
    }
  }

  [DataContract]
  public abstract class SearchResponse
  {
    [DataMember(Name = "limit")]
    public int Limit;

    [DataMember(Name = "offset")]
    public int Offset;

    [DataMember(Name = "total")]
    public int Total;

    public abstract int Count { get; }
  }

  /// <summary>
  /// MangaDex API response to a search query including multiple objects and pagination data
  /// </summary>
  /// <typeparam name="T"></typeparam>
  [DataContract]
  public class SearchResponse<T> : SearchResponse
  {
    [DataMember(Name = "data")]
    public IEnumerable<ObjectData<T>> Data;

    public override int Count => Data.Count();
  }
}
