﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Response to the AtHomeServer API identifying the Base URL for page fetches
  /// </summary>
  [DataContract]
  public class AtHomeServerForChapter
  {
    [DataMember(Name = "baseUrl")]
    public string BaseUrl;

    [DataMember(Name = "chapter")]
    public ChapterUriComponents Chapter;
  }

  [DataContract]
  public class ChapterUriComponents
  {
    /// <summary>
    /// Chapter hash used to identify it to a MD@H server
    /// </summary>
    [DataMember(Name = "hash")]
    public string Hash;

    /// <summary>
    /// Page key used to retreive from a MD@H server
    /// </summary>
    [DataMember(Name = "data")]
    public string[] Data;
  }
}
