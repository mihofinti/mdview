﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Authentication token result
  /// </summary>
  [DataContract]
  public class TokenResult
  {
    [DataMember(Name = "result")]
    public string Result;

    [DataMember(Name = "token")]
    public Token Token;
  }

  /// <summary>
  /// Authentication token details
  /// </summary>
  [DataContract]
  public class Token
  {
    /// <summary>
    /// Session token to use for Bearer authentication
    /// </summary>
    [DataMember(Name = "session")]
    public string Session { get; set; }

    /// <summary>
    /// Refresh token to recover session
    /// </summary>
    [DataMember(Name = "refresh")]
    public string Refresh { get; set; }
  }
}
