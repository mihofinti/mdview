﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Manga details returned by the API
  /// </summary>
  [DataContract]
  public class Manga
  {
    /// <summary>
    /// Manga search query
    /// </summary>
    public class Search : SearchQuery<Manga>
    {
      // This probably doesn't need to be a subclass, but it looks better to the user.
      // Also could support future features specific to the manga search like setting the includedTagsMode/excludedTagsMode

      /// <summary>
      /// Create manga search query
      /// </summary>
      /// <param name="offset"></param>
      /// <param name="limit"></param>
      public Search(int offset = 0, int? limit = null) : base("manga/", offset, limit)
      {
      }

      public static readonly ISearchParameter<Manga, String> Title = new SearchParameterString<Manga>("title");
      public static readonly ISearchParameter<Manga, Guid> Author = new SearchParameterUuid<Manga>("authors[]");
      public static readonly ISearchParameter<Manga, Guid> Artist = new SearchParameterUuid<Manga>("artists[]");
      public static readonly ISearchParameter<Manga, Guid> IncludeTag = new SearchParameterUuid<Manga>("includedTags[]");
      public static readonly ISearchParameter<Manga, Guid> ExcludeTag = new SearchParameterUuid<Manga>("excludedTags[]");
      public static readonly ISearchParameter<Manga, PublicationStatus> PublicationStatus = new SearchParameterBasicEnum<Manga, PublicationStatus>("status");
      public static readonly ISearchParameter<Manga, PublicationDemographic> PublicationDemographic = new SearchParameterBasicEnum<Manga, PublicationDemographic>("publicationDemographic");
      public static readonly ISearchParameter<Manga, Guid> Id = new SearchParameterUuid<Manga>("ids[]");
      public static readonly ISearchParameter<Manga, ContentRating> ContentRating = new SearchParameterBasicEnum<Manga, ContentRating>("contentRating");
      public static readonly ISearchParameter<Manga, DateTime> CreatedSince = new SearchParameterDate<Manga>("createdAtSince");
      public static readonly ISearchParameter<Manga, DateTime> UpdatedSince = new SearchParameterDate<Manga>("updatedAtSince");
    }

    /// <summary>
    /// Titles per language code
    /// </summary>
    [DataMember(Name = "title")]
    public Dictionary<string, string> Title { get; set; }

    /// <summary>
    /// Get the title most likely to be meaningful to an reader
    /// </summary>
    public String DisplayTitle
    {
      get
      {
        String rv = null;
        // First attempt to get phonetic romanization of the original title
        if (Title.TryGetValue(OriginalLanguage + "-ro", out rv))
          return rv;

        // Then try the native title
        if (Title.TryGetValue(OriginalLanguage, out rv))
          return rv;

        // Then try an english translation
        if (Title.TryGetValue("en", out rv))
          return rv;

        // Finally, fallback on any available title
        return Title.Values.FirstOrDefault();
      }
    }

      /// <summary>
      /// Alternate titles per language code
      /// </summary>
      [DataMember(Name = "altTitles")]
    public IEnumerable<Dictionary<string, string>> AlternateTitles { get; set; }

    /// <summary>
    /// Description per language code
    /// </summary>
    [DataMember(Name = "description")]
    public Dictionary<string, string> Description { get; set; }

    /// <summary>
    /// True if locked
    /// </summary>
    [DataMember(Name = "isLocked")]
    public bool isLocked { get; set; }

    /// <summary>
    /// Links to external sites
    /// </summary>
    [DataMember(Name = "links")]
    private Dictionary<string, string> ApiLinks { get; set; }

    /// <summary>
    /// Get link to a particular site
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public bool TryGetLinkTo(LinkTarget target, out Link link)
    {
      if (target == LinkTarget.Unknown)
        throw new ArgumentException("Invalid link target");

      if (ApiLinks.TryGetValue(Link.TargetToApi(target), out var value)) {
        link = new Link(target, value);
        return true;
      }
      else {
        link = null;
        return false;
      }
    }

    /// <summary>
    /// Enumerate all links
    /// </summary>
    public IEnumerable<Link> Links
    {
      get
      {
        foreach (var kv in ApiLinks)
          yield return new Link(kv.Key, kv.Value);
      }
    }

    /// <summary>
    /// Original language
    /// </summary>
    [DataMember(Name = "originalLanguage")]
    public string OriginalLanguage { get; set; }

    /// <summary>
    /// Last volume
    /// </summary>
    [DataMember(Name = "lastVolume")]
    public string LastVolume { get; set; }

    /// <summary>
    /// Last chapter
    /// </summary>
    [DataMember(Name = "lastChapter")]
    public string LastChapter { get; set; }

    /// <summary>
    /// If true, chapter numbers can only be compared within a volume.
    /// Otherwise chapter numbers are always increasing.
    /// </summary>
    [DataMember(Name = "chapterNumberResetOnNewVolume")]
    public bool ChapterNumberResetOnNewVolume;

    [DataMember(Name = "publicationDemographic")]
    private string ApiPublicationDemographic { get; set; }

    /// <summary>
    /// Publication Demographic
    /// </summary>
    public PublicationDemographic PublicationDemographic
    {
      get
      {
        if (Enum.TryParse<PublicationDemographic>(ApiPublicationDemographic, true, out var result))
          return result;
        else
          return PublicationDemographic.Unknown;
      }
    }

    [DataMember(Name = "status")]
    private string Status { get; set; }

    /// <summary>
    /// Publication status
    /// </summary>
    public PublicationStatus PublicationStatus
    {
      get
      {
        if (Enum.TryParse<PublicationStatus>(Status, true, out var result))
          return result;
        else
          return PublicationStatus.Unknown;
      }
    }

    [DataMember(Name = "year")]
    public int? Year { get; set; }

    [DataMember(Name = "contentRating")]
    private string ApiContentRating { get; set; }

    /// <summary>
    /// Content rating
    /// </summary>
    public ContentRating ContentRating
    {
      get
      {
        if (Enum.TryParse<ContentRating>(ApiContentRating, true, out var result))
          return result;
        else
          return ContentRating.Unknown;
      }
    }

    [DataMember(Name = "tags")]
    public IEnumerable<ObjectData<Tag>> Tags;

    [DataMember(Name = "createdAt")]
    private string CreatedAtApi
    {
      get
      {
        return CreatedAt.HasValue ? CreatedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          CreatedAt = dateTime;
        else
          CreatedAt = null;
      }
    }

    /// <summary>
    /// Date created
    /// </summary>
    public DateTime? CreatedAt { get; set; }

    [DataMember(Name = "updatedAt")]
    private string UpdatedAtApi
    {
      get
      {
        return UpdatedAt.HasValue ? UpdatedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          UpdatedAt = dateTime;
        else
          UpdatedAt = null;
      }
    }

    /// <summary>
    /// Date last updated, appears to return 4/19 if predates new API
    /// </summary>
    public DateTime? UpdatedAt { get; set; }

    public override string ToString()
    {
      return String.Format("Manga:{0}", DisplayTitle);
    }
  }
}
