﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Author data object
  /// </summary>
  [DataContract]
  public class Author
  {
    /// <summary>
    /// Author's name
    /// </summary>
    [DataMember(Name = "name")]
    public string Name { get; set; }

    [DataMember(Name = "imageUrl")]
    public string ImageUrl;

    [DataMember(Name = "biography")]
    public Dictionary<string, string> Biography;

    [DataMember(Name = "createdAt")]
    private string CreatedAtApi
    {
      get
      {
        return CreatedAt.HasValue ? CreatedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          CreatedAt = dateTime;
        else
          CreatedAt = null;
      }
    }

    /// <summary>
    /// Date created
    /// </summary>
    public DateTime? CreatedAt { get; set; }

    [DataMember(Name = "updatedAt")]
    private string UpdatedAtApi
    {
      get
      {
        return UpdatedAt.HasValue ? UpdatedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          UpdatedAt = dateTime;
        else
          UpdatedAt = null;
      }
    }

    /// <summary>
    /// Date last updated, appears to return 4/19 if predates new API
    /// </summary>
    public DateTime? UpdatedAt { get; set; }

    public override string ToString()
    {
      return String.Format("Author:{0}", Name);
    }
  }
}
