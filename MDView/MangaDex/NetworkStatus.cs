﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
using System.Net.Http;

namespace MDView.MangaDex
{
  /// <summary>
  /// Network status update
  /// </summary>
  [DataContract]
  internal class NetworkStatus
  {
    /// <summary>
    /// URL retreived
    /// </summary>
    [DataMember(Name = "url")]
    public string Url;

    /// <summary>
    /// True if request was successful
    /// </summary>
    [DataMember(Name = "success")]
    public bool Success;

    /// <summary>
    /// Number of bytes read
    /// </summary>
    [DataMember(Name = "bytes")]
    public long Bytes;

    /// <summary>
    /// Ellapsed time
    /// </summary>
    [DataMember(Name = "duration")]
    public long Duration;

    /// <summary>
    /// True if result was served from cache
    /// </summary>
    [DataMember(Name = "cached")]
    public bool Cached;

    /// <summary>
    /// Create NetworkStatus request from a successful page load
    /// </summary>
    /// <param name="page"></param>
    public NetworkStatus(Page page)
    {
      Url = page.Url;
      Success = true;
      Bytes = page.Stream.Position;
      Duration = (long)page.Duration.TotalMilliseconds;
      Cached = page.Cached;
    }

    /// <summary>
    /// Create NetworkStatus request from a failed page load
    /// </summary>
    /// <param name="url"></param>
    public NetworkStatus(string url)
    {
      Url = url;
      Success = false;
      Bytes = 0;
      Duration = 0;
      Cached = false;
    }
  }
}
