﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.MangaDex
{
  /// <summary>
  /// Encapsulate a search query and build the URI.
  /// 
  /// Prefer usage of the SearchQuery&lt;T&gt; variants which enforce correctness via the type system.
  /// </summary>
  public class SearchQuery
  {
    /// <summary>
    /// Number of results already retreived
    /// </summary>
    private int Offset;

    /// <summary>
    /// Maximum results to retreive per search
    /// </summary>
    private int? Limit;

    /// <summary>
    /// Total size of the result set
    /// </summary>
    private int? Total;

    /// <summary>
    /// Parameter/value pairs to build into the URI
    /// </summary>
    private List<Tuple<string,string>> QueryParameters = new List<Tuple<string, string>>();

    /// <summary>
    /// API endpoint
    /// </summary>
    private string Endpoint;

    /// <summary>
    /// False if query has retreived all results
    /// </summary>
    public bool HasMore => !Total.HasValue || Offset < Total.Value;

    /// <summary>
    /// Initialize a new search query.
    /// 
    /// Optionally starting N entries in the results and overriding the default result length
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="limit"></param>
    public SearchQuery(string endpoint, int offset = 0, int? limit = null)
    {
      Endpoint = endpoint;
      Offset = offset;
      Limit = limit;
      Total = null;
    }

    public string BuildUri()
    {
      IEnumerable<Tuple<string,string>> qps = QueryParameters;
      qps = qps.Append(new Tuple<string, string>("offset", Offset.ToString()));

      if (Limit.HasValue)
        qps = qps.Append(new Tuple<string, string>("limit", Limit.Value.ToString()));

      return Endpoint + "?" + String.Join("&", qps.Select(qp => Uri.EscapeUriString(qp.Item1) + '=' + Uri.EscapeUriString(qp.Item2)));
    }

    /// <summary>
    /// Set a search parameter via the query parameter name and stringified value.
    /// 
    /// Prefer use of the type-specific method.
    /// </summary>
    /// <param name="parameter"></param>
    /// <param name="value"></param>
    public void Add(string parameter, string value)
    {
      QueryParameters.Add(new Tuple<string, string>(parameter, value));
    }

    /// <summary>
    /// Advance offset based on number of results retreived
    /// </summary>
    /// <param name="count"></param>
    internal void Advance(SearchResponse response)
    {
      Offset += response.Count;
      Total = response.Total;
    }
  }

  /// <summary>
  /// Search query with a specific expected search target type
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class SearchQuery<T> : SearchQuery
  {

    /// <summary>
    /// Initialize a new search query.
    /// 
    /// Optionally starting N entries in the results and overriding the default result length
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="limit"></param>
    public SearchQuery(string endpoint, int offset = 0, int? limit = null) : base(endpoint, offset, limit)
    {
    }

    /// <summary>
    /// Search on the specified field
    /// </summary>
    /// <typeparam name="V"></typeparam>
    /// <param name="parameter"></param>
    /// <param name="value"></param>
    public void Add<V>(ISearchParameter<T, V> parameter, V value) {
      Add(parameter.Parameter, parameter.RenderValue(value));
    }
  }

  /// <summary>
  /// Search parameter validating type of the target and query parameter
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="V"></typeparam>
  public interface ISearchParameter<T, V>
  {
    string RenderValue(V value);
    string Parameter { get; }
  }

  /// <summary>
  /// Search parameter accepting string value
  /// </summary>
  /// <typeparam name="T"></typeparam>
  internal class SearchParameterString<T> : ISearchParameter<T, String>
  {
    public string RenderValue(string v) => v;

    public string Parameter { get; private set; }

    public SearchParameterString(string parameter ) {
      Parameter = parameter;
    }
  }

  /// <summary>
  /// Search parameter accepting a date/time value
  /// </summary>
  /// <typeparam name="T"></typeparam>
  internal class SearchParameterDate<T> : ISearchParameter<T, DateTime>
  {
    public string RenderValue(DateTime v) => v.ToString( Service.DATE_TIME_PATTERN );

    public string Parameter { get; private set; }

    public SearchParameterDate(string parameter)
    {
      Parameter = parameter;
    }
  }

  /// <summary>
  /// Search parameter accepting a UUID
  /// </summary>
  /// <typeparam name="T"></typeparam>
  internal class SearchParameterUuid<T> : ISearchParameter<T, Guid>
  {
    public string RenderValue(Guid v) => v.ToString();

    public string Parameter { get; private set; }

    public SearchParameterUuid(string parameter)
    {
      Parameter = parameter;
    }
  }

  /// <summary>
  /// Search parameter accepting an Enum value where the API value of the enum is the lower-cased name
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <typeparam name="E"></typeparam>
  internal class SearchParameterBasicEnum<T, E> : ISearchParameter<T, E> where E : Enum
  {
    public string RenderValue(E v) => v.ToString().ToLower();

    public string Parameter { get; private set; }

    public SearchParameterBasicEnum(string parameter)
    {
      Parameter = parameter;
    }
  }
}
