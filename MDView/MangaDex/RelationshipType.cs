﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.MangaDex
{
  public enum RelationshipType
  {
    Monochrome,
    MainStory,
    AdaptedFrom,
    BasedOn,
    Prequel,
    SideStory,
    Doujinshi,
    SameFranchise,
    SharedUniverse,
    Sequel,
    SpinOff,
    AlternateStory,
    Preserialization,
    Colored,
    Serialization,
    AlternateVersion,
    Unknown
  }

  public static partial class EnumTranslations
  {
    private static readonly string[] RelationshipTypeApiValues = { "monochrome", "main_story", "adapted_from", "based_on", "prequel", "side_story", "doujinshi", "same_franchise", "shared_universe", "sequel", "spin_off", "alternate_story", "preserialization", "colored", "serialization", "alternate_version" };

    public static string ToApi(this RelationshipType relationshipType)
    {
      if ((int)relationshipType >= 0 && (int)relationshipType < RelationshipTypeApiValues.Length)
        return RelationshipTypeApiValues[(int)relationshipType];
      else
        return null;
    }

    public static RelationshipType ToRelationshipType(this string relationshipType)
    {
      for (int i = 0; i < RelationshipTypeApiValues.Length; i++) {
        if (RelationshipTypeApiValues[i] == relationshipType)
          return (RelationshipType)i;
      }
      return RelationshipType.Unknown;
    }

    private static RelationshipType[] RelationshipTypeInverse = {
      RelationshipType.Colored,
      RelationshipType.SideStory,
      RelationshipType.SpinOff,
      RelationshipType.Doujinshi,
      RelationshipType.Sequel,
      RelationshipType.MainStory,
      RelationshipType.BasedOn,
      RelationshipType.SameFranchise,
      RelationshipType.SharedUniverse,
      RelationshipType.Prequel,
      RelationshipType.AdaptedFrom,
      RelationshipType.AlternateStory,
      RelationshipType.Serialization,
      RelationshipType.Monochrome,
      RelationshipType.Preserialization,
      RelationshipType.AlternateVersion,
      RelationshipType.Unknown
    };

    /// <summary>
    /// Return the inverse of a given relationship
    /// </summary>
    /// <param name="relationship"></param>
    /// <returns></returns>
    public static RelationshipType Inverse(this RelationshipType relationship)
    {
      return RelationshipTypeInverse[(int)relationship];
    }
  }
}
