﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Request for the Login API
  /// </summary>
  [DataContract]
  public class Login
  {
    /// <summary>
    /// User name
    /// </summary>
    [DataMember(Name = "username")]
    public string UserName;

    /// <summary>
    /// Password
    /// </summary>
    [DataMember(Name = "password")]
    public string Password;
  }
}
