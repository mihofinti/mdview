﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Request to the Legacy Mapping API
  /// </summary>
  [DataContract]
  public class LegacyMappingRequest
  {
    /// <summary>
    /// Type of object to translate.
    /// One of the LegacyMappingType constants
    /// </summary>
    [DataMember(Name ="type")]
    private string Type;

    /// <summary>
    /// List of IDs to translate
    /// </summary>
    [DataMember(Name = "ids")]
    public IEnumerable<int> Ids;

    public LegacyMappingRequest(ObjectType type, params int[] ids)
    {
      Type = type.ToApi();
      Ids = ids;
    }
  }
}
