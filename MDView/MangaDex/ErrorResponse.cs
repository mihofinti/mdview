﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace MDView.MangaDex
{
  /// <summary>
  /// Error response from the MangaDex API
  /// </summary>
  [DataContract]
  public class ErrorResponse
  {
    /// <summary>
    /// Error list
    /// </summary>
    [DataMember(Name ="errors")]
    public IEnumerable<Error> Errors;
  }

  /// <summary>
  /// Specific errors from the MangaDex API
  /// </summary>
  [DataContract]
  public class Error
  {
    [DataMember(Name = "id")]
    public string Id;

    [DataMember(Name = "status")]
    public int Status;

    [DataMember(Name = "title")]
    public string Title;

    [DataMember(Name = "detail")]
    public string Detail;
  }
}
