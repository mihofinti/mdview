﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

using MDView.Util;

namespace MDView.MangaDex
{
  /// <summary>
  /// Response for any chapter queries
  /// </summary>
  [DataContract]
  public class Chapter
  {
    /// <summary>
    /// Chapter title
    /// </summary>
    [DataMember(Name = "title")]
    public string Title { get; set; }

    /// <summary>
    /// Chapter volume number (if present)
    /// </summary>
    [DataMember(Name = "volume")]
    public string Volume { get; set; }

    /// <summary>
    /// Chapter number or other identifier.
    /// </summary>
    [DataMember(Name = "chapter")]
    public string ChapterApi { get; set; }

    /// <summary>
    /// Parsed chapter number
    /// </summary>
    public Tuple<int,string> Number => Util.Parse.ChapterNumber(ChapterApi);

    /// <summary>
    /// Number of pages in the chapter
    /// </summary>
    [DataMember(Name = "pages")]
    public int Pages;

    /// <summary>
    /// Translation language code
    /// </summary>
    [DataMember(Name = "translatedLanguage")]
    public string TranslatedLanguage { get; set; }

    /// <summary>
    /// Link to non-mangadex source of the chapter
    /// </summary>
    [DataMember(Name = "externalUrl")]
    public string ExternalUrl { get; set; }

    [DataMember(Name = "publishedAt")]
    private string PublishedAtApi
    {
      get
      {
        return PublishedAt.HasValue ? PublishedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          PublishedAt = dateTime;
        else
          PublishedAt = null;
      }
    }

    /// <summary>
    /// Date published
    /// </summary>
    public DateTime? PublishedAt { get; set; }

    [DataMember(Name = "updatedAt")]
    private string UpdatedAtApi
    {
      get
      {
        return UpdatedAt.HasValue ? UpdatedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          UpdatedAt = dateTime;
        else
          UpdatedAt = null;
      }
    }

    /// <summary>
    /// Date updated
    /// </summary>
    public DateTime? UpdatedAt { get; set; }

    [DataMember(Name = "createdAt")]
    private string CreatedAtApi
    {
      get
      {
        return CreatedAt.HasValue ? CreatedAt.Value.ToString(Service.DATE_TIME_PATTERN) : null;
      }
      set
      {
        if (DateTime.TryParseExact(value, Service.DATE_TIME_PATTERN, null, System.Globalization.DateTimeStyles.AssumeUniversal, out var dateTime))
          CreatedAt = dateTime;
        else
          CreatedAt = null;
      }
    }

    /// <summary>
    /// Date created
    /// </summary>
    public DateTime? CreatedAt { get; set; }

    public override string ToString()
    {
      return String.Format("Chapter:{0}:{1}", ChapterApi, Title);
    }
  }
}
