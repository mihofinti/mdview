﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.MangaDex
{
  /// <summary>
  /// Publication demographic enumeration for Manga detail API
  /// </summary>
  public enum PublicationDemographic
  {
    Shounen,
    Shoujo,
    Josei,
    Seinen,
    Unknown,
  }
}
