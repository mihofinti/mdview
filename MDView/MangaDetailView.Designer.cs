﻿
namespace MDView
{
  partial class MangaDetailView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.label1 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.TitleDisplay = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.TagsDisplay = new System.Windows.Forms.Label();
      this.DescriptionDisplay = new System.Windows.Forms.Label();
      this.UpdateHistory = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.LinkPane = new System.Windows.Forms.FlowLayoutPanel();
      this.label6 = new System.Windows.Forms.Label();
      this.PublicationStatusDisplay = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.AuthorDisplay = new System.Windows.Forms.Label();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.TitleDisplay, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
      this.tableLayoutPanel1.Controls.Add(this.TagsDisplay, 1, 2);
      this.tableLayoutPanel1.Controls.Add(this.DescriptionDisplay, 1, 5);
      this.tableLayoutPanel1.Controls.Add(this.UpdateHistory, 1, 6);
      this.tableLayoutPanel1.Controls.Add(this.label2, 0, 6);
      this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
      this.tableLayoutPanel1.Controls.Add(this.LinkPane, 1, 4);
      this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
      this.tableLayoutPanel1.Controls.Add(this.PublicationStatusDisplay, 1, 3);
      this.tableLayoutPanel1.Controls.Add(this.label7, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.AuthorDisplay, 1, 1);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 7;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(596, 593);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(27, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Title";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(3, 26);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(31, 13);
      this.label3.TabIndex = 1;
      this.label3.Text = "Tags";
      // 
      // TitleDisplay
      // 
      this.TitleDisplay.AutoSize = true;
      this.TitleDisplay.Location = new System.Drawing.Point(69, 0);
      this.TitleDisplay.Name = "TitleDisplay";
      this.TitleDisplay.Size = new System.Drawing.Size(35, 13);
      this.TitleDisplay.TabIndex = 2;
      this.TitleDisplay.Text = "label2";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(3, 52);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(32, 13);
      this.label4.TabIndex = 1;
      this.label4.Text = "Links";
      // 
      // TagsDisplay
      // 
      this.TagsDisplay.AutoSize = true;
      this.TagsDisplay.Location = new System.Drawing.Point(69, 26);
      this.TagsDisplay.Name = "TagsDisplay";
      this.TagsDisplay.Size = new System.Drawing.Size(35, 13);
      this.TagsDisplay.TabIndex = 2;
      this.TagsDisplay.Text = "label2";
      // 
      // DescriptionDisplay
      // 
      this.DescriptionDisplay.AutoSize = true;
      this.DescriptionDisplay.Location = new System.Drawing.Point(69, 65);
      this.DescriptionDisplay.Name = "DescriptionDisplay";
      this.DescriptionDisplay.Size = new System.Drawing.Size(35, 13);
      this.DescriptionDisplay.TabIndex = 2;
      this.DescriptionDisplay.Text = "label2";
      // 
      // UpdateHistory
      // 
      this.UpdateHistory.Dock = System.Windows.Forms.DockStyle.Fill;
      this.UpdateHistory.Location = new System.Drawing.Point(69, 81);
      this.UpdateHistory.Name = "UpdateHistory";
      this.UpdateHistory.Size = new System.Drawing.Size(524, 509);
      this.UpdateHistory.TabIndex = 3;
      this.UpdateHistory.Paint += new System.Windows.Forms.PaintEventHandler(this.UpdateHistory_Paint);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 78);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(47, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Updates";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(3, 65);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(60, 13);
      this.label5.TabIndex = 1;
      this.label5.Text = "Description";
      // 
      // LinkPane
      // 
      this.LinkPane.AutoSize = true;
      this.LinkPane.Dock = System.Windows.Forms.DockStyle.Fill;
      this.LinkPane.Location = new System.Drawing.Point(66, 52);
      this.LinkPane.Margin = new System.Windows.Forms.Padding(0);
      this.LinkPane.Name = "LinkPane";
      this.LinkPane.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
      this.LinkPane.Size = new System.Drawing.Size(530, 13);
      this.LinkPane.TabIndex = 4;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(3, 39);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(37, 13);
      this.label6.TabIndex = 1;
      this.label6.Text = "Status";
      // 
      // PublicationStatusDisplay
      // 
      this.PublicationStatusDisplay.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.PublicationStatusDisplay.AutoSize = true;
      this.PublicationStatusDisplay.Location = new System.Drawing.Point(69, 39);
      this.PublicationStatusDisplay.Name = "PublicationStatusDisplay";
      this.PublicationStatusDisplay.Size = new System.Drawing.Size(37, 13);
      this.PublicationStatusDisplay.TabIndex = 1;
      this.PublicationStatusDisplay.Text = "Status";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(3, 13);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(38, 13);
      this.label7.TabIndex = 1;
      this.label7.Text = "Author";
      // 
      // AuthorDisplay
      // 
      this.AuthorDisplay.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.AuthorDisplay.AutoSize = true;
      this.AuthorDisplay.Location = new System.Drawing.Point(69, 13);
      this.AuthorDisplay.Name = "AuthorDisplay";
      this.AuthorDisplay.Size = new System.Drawing.Size(16, 13);
      this.AuthorDisplay.TabIndex = 1;
      this.AuthorDisplay.Text = "...";
      // 
      // MangaDetailView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.tableLayoutPanel1);
      this.Name = "MangaDetailView";
      this.Size = new System.Drawing.Size(596, 593);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label TitleDisplay;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label TagsDisplay;
    private System.Windows.Forms.Label DescriptionDisplay;
    private System.Windows.Forms.Panel UpdateHistory;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.FlowLayoutPanel LinkPane;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label PublicationStatusDisplay;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label AuthorDisplay;
  }
}
