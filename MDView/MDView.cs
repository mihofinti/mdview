﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView
{
  /// <summary>
  /// Top-level window
  /// 
  /// Handles views as a stack, allowing them to request movement to a new low level window while keeping history to go back
  /// </summary>
  public partial class MDView : Form
  {
    /// <summary>
    /// Views, current will be at the top of the stack
    /// </summary>
    private Stack<INavigableControl> Stack = new Stack<INavigableControl>();

    public MDView()
    {
      InitializeComponent();

      Application.ThreadException += Application_ThreadException;

#if !DEBUG
      // Hide debug menu for release builds
      DebugMenu.DropDownItems.Clear();
      DebugMenu.Click += About_Click;
#endif

      ShowView(new LoginView());
    }

    /// <summary>
    /// In case of any unhandled errors
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
    {
      Util.Util.ReportError(e.Exception);
    }

    /// <summary>
    /// Add a new view to the stack
    /// </summary>
    /// <param name="view"></param>
    private void ShowView(INavigableControl view)
    {
      Console.Log("# VIEW {0}", view);

      // Remove the previous view
      ActivePane.Controls.Clear();

      // Show the new view
      Stack.Push(view);
      view.Control.Dock = DockStyle.Fill;
      ActivePane.Controls.Add(view.Control);
      Text = "MDView - " + view.Title;

      // Subscript to requests to navigate away
      view.NavigationRequested += (s, e) => e.Destination.Bind(ShowView, ShowRelativeView);
      view.Control.Focus();

      ManageLists.Enabled = Cache.Database != null;
      SearchManga.Enabled = Cache.Service != null;
      view.OnShow();
    }

    /// <summary>
    /// Navigation to an existing view by relative description
    /// </summary>
    /// <param name="target"></param>
    private void ShowRelativeView(NavigationDestination target)
    {
      switch (target) {
        case NavigationDestination.Back: {
            if (Stack.Count <= 1)
              return;

            var prev = Stack.Pop();
            ActivePane.Controls.Clear();
            prev.Control.Dispose();

            var active = Stack.Peek();
            ActivePane.Controls.Add(active.Control);
            Text = "MDView - " + active.Title;

            // Signal the screen that it's back
            active.Control.Focus();
            active.OnShow();
            break;
          }

        case NavigationDestination.Root: {
            ActivePane.Controls.Clear();

            while (Stack.Count > 1) {
              var prev = Stack.Pop();
              prev.Control.Dispose();
            }

            var active = Stack.Peek();
            ActivePane.Controls.Add(active.Control);
            Text = "MDView - " + active.Title;

            // Signal the screen that it's back
            active.Control.Focus();
            active.OnShow();
            break;
          }

        default:
          break;
      }
    }

    /// <summary>
    /// Handle the back button, disposing the current view and showing the previous
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void NavigateBack_Click(object sender, EventArgs e)
    {
      ShowRelativeView(NavigationDestination.Back);
    }

    /// <summary>
    /// Raise the list management screen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ManageLists_Click(object sender, EventArgs e)
    {
      ShowView(new FollowListView());
    }

    /// <summary>
    /// Open search screen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void SearchManga_Click(object sender, EventArgs e)
    {
      ShowView(new MangaSearchView());
    }

    /// <summary>
    /// Set the screen to a fixed size to capture consistent screen shots for the documentation
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void setScreenSizeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.WindowState = FormWindowState.Normal;
      this.Size = new Size(640, 480);
    }

    /// <summary>
    /// Program summary
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void About_Click(object sender, EventArgs e)
    {
      MessageBox.Show($@"MDView - A simple viewer for the MangaDex API

Build: {Properties.Resources.BuildDate}
See https://gitlab.com/mihofinti/mdview for updates.");
    }

    /// <summary>
    /// Download schema and save to disk
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void downloadSchemaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var stream = Cache.Service.GetSchema();

      var dlg = new SaveFileDialog();
      dlg.Filter = "YAML documents (*.yaml)|*.yaml|All files (*.*)|*.*";
      dlg.RestoreDirectory = true;

      if (dlg.ShowDialog() != DialogResult.OK)
        return;

      using (var file = dlg.OpenFile()) {
        stream.CopyTo(file);
      }
    }

    private void showConsoleToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Console.ShowConsole();
    }
  }
}
