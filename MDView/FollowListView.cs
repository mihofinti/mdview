﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MDView.Config;

namespace MDView
{
  /// <summary>
  /// Display lists and allow for adding/removing/editing
  /// </summary>
  public partial class FollowListView : UserControl, INavigableControl
  {
    public Control Control => this;

    public String Title => "Manage Follow Lists";

    public event NavigationEventHandler NavigationRequested;

    /// <summary>
    /// Display name of list or dummy name for no-list
    /// </summary>
    private class FollowListAdaptor {
      public readonly FollowList FollowList;

      public string Name => FollowList?.Name ?? "< uncategorized >";

      public FollowListAdaptor(FollowList list)
      {
        FollowList = list;
      }
    }

    public FollowListView()
    {
      InitializeComponent();

      ReloadListData();
    }

    public void ReloadListData()
    {
      var lists = Cache.Database.GetLists().Select(l => new FollowListAdaptor(l)).ToList();
      var listsWithNull = new List<FollowListAdaptor>(lists);
      listsWithNull.Insert(0, new FollowListAdaptor(null));

      FollowLists.DataSource = lists;
      FollowListLeft.DataSource = new BindingList<FollowListAdaptor>(listsWithNull);
      FollowListRight.DataSource = new BindingList<FollowListAdaptor>(listsWithNull);
    }

    public void OnShow()
    {
    }

    /// <summary>
    /// Add a new follow list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void AddList_Click(object sender, EventArgs e)
    {
      var name = ListNameEntry.Text;
      Cache.Database.AddList(name);
      ReloadListData();
    }

    private void FollowListLeft_SelectedIndexChanged(object sender, EventArgs e)
    {
      LoadMangaInList(FollowListLeft, MangaListLeft);
    }

    private void FollowListRight_SelectedIndexChanged(object sender, EventArgs e)
    {
      LoadMangaInList(FollowListRight, MangaListRight);
    }

    /// <summary>
    /// Reload the lists
    /// </summary>
    /// <param name="listSelection"></param>
    /// <param name="mangaList"></param>
    private void LoadMangaInList(ComboBox listSelection, CheckedListBox mangaList)
    {
      mangaList.Items.Clear();

      var item = listSelection.SelectedItem as FollowListAdaptor;
      if (item == null)
        return;

      IEnumerable<Follow> follows;
      if (item.FollowList == null)
        follows = Cache.Database.GetUncategorizedFollows();
      else
        follows = Cache.Database.GetFollowList(item.FollowList);
      
      foreach (var follow in follows)
        mangaList.Items.Add( new UI.DisplayWrapper<Follow>( follow, follow.Title ), false);
    }

    private void CopyToRight_Click(object sender, EventArgs e)
    {
      Copy(FollowListLeft, MangaListLeft, FollowListRight, false);
    }

    private void MoveToRight_Click(object sender, EventArgs e)
    {
      Copy(FollowListLeft, MangaListLeft, FollowListRight, true);
    }

    private void CopyToLeft_Click(object sender, EventArgs e)
    {
      Copy(FollowListRight, MangaListRight, FollowListLeft, false);
    }

    private void MoveToLeft_Click(object sender, EventArgs e)
    {
      Copy(FollowListRight, MangaListRight, FollowListLeft, true);
    }

    /// <summary>
    /// Add selected items from one list to another.
    /// 
    /// Optionally remove the items from the source list.
    /// </summary>
    /// <param name="fromList"></param>
    /// <param name="fromItems"></param>
    /// <param name="toList"></param>
    /// <param name="removeFromCurrent"></param>
    private void Copy(ComboBox fromList, CheckedListBox fromItems, ComboBox toList, bool removeFromCurrent)
    {
      var from = fromList.SelectedItem as FollowListAdaptor;
      if (from == null)
        return;

      var to = toList.SelectedItem as FollowListAdaptor;
      if (to == null)
        return;

      foreach (var chitem in fromItems.CheckedItems) {
        var item = chitem as UI.DisplayWrapper<Follow>;
        if (item == null)
          continue;

        // No need to remove from the uncategorized list, that happens automatically when assigned to another list
        if (removeFromCurrent && from.FollowList != null)
          Cache.Database.RemoveFollowFromList(from.FollowList, item.Value);

        // To move an item to the uncategorized list, must remove from all others
        if (to.FollowList != null)
          Cache.Database.AddFollowToList(to.FollowList, item.Value);
        else
          Cache.Database.RemoveFollowFromAllLists(item.Value);
      }

      LoadMangaInList(FollowListLeft, MangaListLeft);
      LoadMangaInList(FollowListRight, MangaListRight);
    }
  }
}
