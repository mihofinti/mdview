﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView
{
  /// <summary>
  /// Display debug log
  /// </summary>
  public partial class Console : Form
  {
    /// <summary>
    /// Messages awaiting display
    /// </summary>
    private static ConcurrentQueue<string> Messages = new ConcurrentQueue<string>();

    /// <summary>
    /// Global console instance
    /// </summary>
    private static Console Instance;

    private Console()
    {
      InitializeComponent();
      ConsoleLines.Items.Clear();
    }

    /// <summary>
    /// Create console (if not already initialized) and display.
    /// </summary>
    public static void ShowConsole()
    {
      lock (typeof(Console)) {
        if (Instance == null)
          Instance = new Console();
      }
      Instance.Show();
    }

    /// <summary>
    /// Log a message to the console
    /// </summary>
    /// <param name="text"></param>
    /// <param name="args"></param>
    public static void Log(string text, params object[] args)
    {
      Messages.Enqueue(DateTime.Now.ToString("O") + " " + String.Format(text, args));
    }

    /// <summary>
    /// Log an exception to the console
    /// </summary>
    /// <param name="ex"></param>
    public static void Log(Exception ex)
    {
      var aex = ex as AggregateException;
      if (aex != null) {
        foreach (var iex in aex.Flatten().InnerExceptions)
          Log(iex);
      }
      else {
        for (; ex != null; ex = ex.InnerException) {
          Log("{0}: {1}", ex.GetType().Name, ex.Message);
        }
      }
    }

    /// <summary>
    /// Move pending messages to the UI
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void UpdateTick_Tick(object sender, EventArgs e)
    {
      while (Messages.TryDequeue(out var msg)) {
        ConsoleLines.Items.Add(msg);
      }
    }

    /// <summary>
    /// Handle Ctrl-V to copy the logs
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Console_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.Control && e.KeyCode == Keys.V) {
        var sb = new StringBuilder();

        if (ConsoleLines.SelectedItems.Count == 0) {
          foreach (var item in ConsoleLines.Items)
            sb.AppendLine(item.ToString());
        }
        else {
          foreach (var item in ConsoleLines.SelectedItems)
            sb.AppendLine(item.ToString());
        }
        Clipboard.SetText(sb.ToString());
      }
    }

    private void Console_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (e.CloseReason == CloseReason.UserClosing) {
        e.Cancel = true;
        Hide();
      }
    }
  }
}
