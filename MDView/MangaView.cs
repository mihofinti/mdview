﻿using MDView.Config;
using MDView.MangaDex;
using MDView.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView
{
  /// <summary>
  /// View the chapter list of a selected Manga
  /// </summary>
  public partial class MangaView : UserControl, INavigableControl
  {
    public Control Control => this;

    public String Title => Record.Title;

    public UI.MangaRecord Record;

    public override string ToString() => "Manga " + Record.Manga.Id;

    /// <summary>
    /// Adaptor to display chapters in the DataGridView
    /// </summary>
    private class ChapterAdaptor
    {
      /// <summary>
      /// Underlying chapter object
      /// </summary>
      public ObjectData<Chapter> Data;

      /// <summary>
      /// Reference to this manga's data (including follow status)
      /// </summary>
      private UI.MangaRecord Record;

      /// <summary>
      /// Volume & chapter number/identifier
      /// </summary>
      public string Chapter
      {
        get;
        private set;
      }

      /// <summary>
      /// Chapter title
      /// </summary>
      public string Title => Data.Attributes.Title;

      /// <summary>
      /// Scanlators's name
      /// </summary>
      public string ScanlationGroupName
      {
        get;
        private set;
      }

      /// <summary>
      /// Most recent of the creation/publication/update date formatted for display
      /// </summary>
      public string Date
      {
        get;
        private set;
      }

      /// <summary>
      /// Additional marker to indicate where you left off reading
      /// </summary>
      public string Marker
      {
        get
        {
          // Follow can be null if just previewing a chapter
          if (Record.Follow != null) {
            int cmp = Record.ChapterOrder.Compare(Record.Follow, Data);
            if (cmp == 0)
              return "Last";

            // Chapter is new if either it has a higher number than the last read chapter
            // Or was created/updated since the last time a chapter was read
            if (cmp < 0)
              return "New";

            if (Record.Follow.LastRead.HasValue && Record.Follow.LastRead < Util.Maybe.Maximum(DateTime.MinValue, Data.Attributes.CreatedAt, Data.Attributes.PublishedAt, Data.Attributes.UpdatedAt))
              return "New";

          }

          return "";
        }
      }

      /// <summary>
      /// Initialize from the API
      /// </summary>
      /// <param name="data"></param>
      public ChapterAdaptor(UI.MangaRecord record, ObjectData<Chapter> data, IEnumerable<ObjectData<ScanlationGroup>> scans, string maxVolumePad, string maxChapterPad)
      {
        Data = data;
        Record = record;

        if (scans != null && scans.Any()) {
          ScanlationGroupName = String.Join(", ", scans.Select(sg => sg.Attributes.Name));
        }

        // Pad out so the Volume: Chapter string lines up evenly
        //
        // [Figure Space | Volume Number] [Punctuation Space | :] [ ] [Figure Space | Chapter Number] [Chapter Suffix]

        string volume;
        if (data.Attributes.Volume != null) {
          volume = data.Attributes.Volume + ": ";
          if (volume.Length < maxVolumePad.Length)
            volume = maxVolumePad.Substring(0, maxVolumePad.Length - volume.Length) + volume;
        }
        else
          volume = maxVolumePad;

        string chapter;
        if (data.Attributes.Number != null) {
          chapter = data.Attributes.ChapterApi;
          var len = Util.Util.DigitCount(data.Attributes.Number.Item1);
          if (len < maxChapterPad.Length)
            chapter = maxChapterPad.Substring(0, maxChapterPad.Length - len) + chapter;
        }
        else
          chapter = maxChapterPad;

        Chapter = volume + chapter;

        var lastDate = Util.Maybe.Maximum(data.Attributes.CreatedAt, data.Attributes.PublishedAt, data.Attributes.UpdatedAt);
        Date = lastDate?.ToString("d") ?? "";
      }
    }

    /// <summary>
    /// Source data
    /// </summary>
    private BindingList<ChapterAdaptor> ChapterListData;

    /// <summary>
    /// List the current manga is from
    /// </summary>
    private List<UI.MangaRecord> ViewedList;

    /// <summary>
    /// Cached cell styles to highlight special rows
    /// </summary>
    private DataGridViewCellStyle LastReadRowStyle;
    
    /// <summary>
    /// Load manga data and populate
    /// </summary>
    /// <param name="mangaId"></param>
    public MangaView(UI.MangaRecord record, List<UI.MangaRecord> mangaList )
    {
      Record = record;
      ViewedList = mangaList;

      InitializeComponent();

      LastReadRowStyle = new DataGridViewCellStyle(ChapterList.DefaultCellStyle);
      LastReadRowStyle.BackColor = Color.Wheat;

      foreach (var m in mangaList) {
        var item = new UI.DisplayWrapper<UI.MangaRecord>(m, m.Title);
        MangaList.Items.Add(item);

        if (item.Value == record)
          MangaList.SelectedItem = item;
      }

      MouseWheel += UI.EventHelpers.MouseWheelScrolls(ChapterList);
    }

    private void LoadChapterList()
    {
      this.InBackground(
        () => {
          var chs = Record.Chapters;
          if( chs == null )
            chs = Cache.Service.GetChapters(Record.Manga.Id).ToList();

          var sgids = chs.SelectMany(ch => ch.Relationships.Where(r => r.ObjectType == ObjectType.ScanlationGroup))
                       .Select(sg => sg.Id)
                       .Distinct();
          var sgs = Cache.Service.GetScanlationGroup(sgids);

          return Tuple.Create(chs, sgs);
        },
        (res) => {
          var chapters = res.Item1;
          var groups = res.Item2; // Could turn this into an hashtable, but series are rarely translated by more than a few groups

          if (chapters.Any()) {
            var maxVolume = chapters.Max(ch => ch.Attributes.Volume?.Length ?? 0);
            var maxChapter = Util.Util.DigitCount(chapters.Max(ch => ch.Attributes.Number.Item1));

            var maxVolumePad = maxVolume == 0 ? "" : new String('\u2007', maxVolume) + "\u2008 ";
            var maxChapterPad = new String('\u2007', maxChapter);

            ChapterListData = new BindingList<ChapterAdaptor>(
              chapters
              .OrderByDescending( a => a, Record.ChapterOrder )
              .Select(ch => new ChapterAdaptor(Record, ch,
                ch.Relationships.Where(r => r.ObjectType == ObjectType.ScanlationGroup)
                  .Select(sgr => groups.FirstOrDefault(sg => sg.Id == sgr.Id))
                  .Where(sg => sg != null), maxVolumePad, maxChapterPad)
              )
              .ToList());
          }
          else {
            ChapterListData = new BindingList<ChapterAdaptor>();
          }

          ChapterList.DataSource = ChapterListData;
        }
        );
    }

    /// <summary>
    /// Navigation
    /// </summary>
    public event NavigationEventHandler NavigationRequested;

    /// <summary>
    /// Open the selected chapter in the chapter view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ChapterList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      if (ChapterList.SelectedRows.Count != 1)
        return;

      var row = ChapterList.SelectedRows[0].DataBoundItem as ChapterAdaptor;
      if (row == null)
        return;

      if (row.Data.Attributes.ExternalUrl != null) {
        System.Diagnostics.Process.Start(row.Data.Attributes.ExternalUrl);
      }
      else if (NavigationRequested != null) {
        var args = new NavigationEventArgs(new ChapterView(Record, row.Data));
        NavigationRequested(this, args);
      }
    }

    /// <summary>
    /// On return to this screen, Follow information should have been changed.
    /// 
    /// This can affect multiple rows (previous last read, new last read, potentially
    /// multiple rows that are no longer new).
    /// </summary>
    public void OnShow()
    {
      if (Record.Follow != null && Record.Follow.LastRead.HasValue) {
        if( String.IsNullOrWhiteSpace(Record.Follow.LastVolume) )
          LastRead.Text = String.Format("Last read chapter {0} on {1:d}", Record.Follow.LastChapter, Record.Follow.LastRead);
        else
          LastRead.Text = String.Format("Last read volume {2} chapter {0} on {1:d}", Record.Follow.LastChapter, Record.Follow.LastRead, Record.Follow.LastVolume);
      }
      else {
        LastRead.Text = "";
      }

      if (ChapterListData == null) {
        // Screen newly opened, load all the metadata
        LoadChapterList();
      }
      else  {
        // Return to this screen, refresh in case anything changed
        ChapterListData.ResetBindings();
      }
    }

    private void MangaDetails_Click(object sender, EventArgs e)
    {
      var view = new MangaDetailView(Record.Manga, ChapterListData.Select(a => a.Data));
      NavigationRequested(this, new NavigationEventArgs(view));
    }

    private void ChapterList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      if (e.Button == MouseButtons.XButton1) {
        NavigationRequested(this, new NavigationEventArgs(NavigationDestination.Back));
      }
      else if (e.Button == MouseButtons.Right && Record.Follow != null) {
        ChapterList.Rows[e.RowIndex].Selected = true;
        ChapterContextMenu.Show(Cursor.Position);
      }
    }

    private void PrevManga_Click(object sender, EventArgs e)
    {
      if (MangaList.SelectedIndex <= 0)
        return;

      MangaList.SelectedIndex--;
      MangaList_SelectionChangeCommitted(this, null);
    }

    private void NextManga_Click(object sender, EventArgs e)
    {
      if (MangaList.SelectedIndex + 1 >= MangaList.Items.Count)
        return;

      MangaList.SelectedIndex++;
      MangaList_SelectionChangeCommitted(this, null);
    }

    private void MangaList_SelectionChangeCommitted(object sender, EventArgs e)
    {
      var item = MangaList.SelectedItem as UI.DisplayWrapper<UI.MangaRecord>;
      if (item == null)
        return;

      if (item.Value == Record)
        return;

      Record = item.Value;
      LoadChapterList();
    }

    /// <summary>
    /// Explicitly set the last-read chapter
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ContextMenuSetLastRead_Click(object sender, EventArgs e)
    {
      if (ChapterList.SelectedRows.Count != 1)
        return;

      var row = ChapterList.SelectedRows[0].DataBoundItem as ChapterAdaptor;
      if (row == null)
        return;

      Cache.Database.ReadFollow(Record.Follow, Record, row.Data, true);

      ChapterListData.ResetBindings();
    }

    /// <summary>
    /// Highlight the last-read row
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ChapterList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      var row = ChapterList.Rows[e.RowIndex].DataBoundItem as ChapterAdaptor;
      if (row == null)
        return;

      // TODO, row markers should be an enumeration
      if (row.Marker == "Last") {
        e.CellStyle = LastReadRowStyle;
      }
    }
  }
}
