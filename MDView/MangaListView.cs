﻿using MDView.Config;
using MDView.MangaDex;
using MDView.UI;
using MDView.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView
{
  /// <summary>
  /// View list of followed manga with status of last update.
  /// </summary>
  public partial class MangaListView : UserControl, INavigableControl
  {
    /// <summary>
    /// Data to be displayed in the data grid view
    /// </summary>
    private class MangaListAdaptor
    {
      public MangaRecord Record;

      public string New => Record.HasUnreadChapters ? "New" : "";

      public string Next => Record.HasNextChapter ? "New" : "";

      /// <summary>
      /// Title of the manga, fallback to the saved title if data is not yet loaded from the server
      /// </summary>
      public string Title => Record.Title;

      /// <summary>
      /// Newest chapter
      /// </summary>
      public string LastChapter => Record.LatestUpdate?.ToString("d");

      /// <summary>
      /// Date last read
      /// </summary>
      public string LastRead => Record.Follow?.LastRead?.ToString("d");

      public MangaListAdaptor(MangaRecord record)
      {
        Record = record;
      }
    }

    /// <summary>
    /// Display name of list or dummy name for no-list
    /// </summary>
    private class FollowListAdaptor
    {
      public readonly FollowList FollowList;

      public string Name => FollowList?.Name ?? "< uncategorized >";

      public FollowListAdaptor(FollowList list)
      {
        FollowList = list;
      }

      public override bool Equals(object obj)
      {
        var other = obj as FollowListAdaptor;
        if (other == null)
          return false;

        if (this.FollowList == null || other.FollowList == null)
          return this.FollowList == null && other.FollowList == null;
        else
          return this.FollowList.Equals(other.FollowList);
      }

      public override int GetHashCode()
      {
        return FollowList == null ? 0 : FollowList.GetHashCode();
      }
    }

    public Control Control => this;

    public String Title => FixedList ? "Search Results" : "Followed Manga";

    public event NavigationEventHandler NavigationRequested;

    private BindingList<MangaListAdaptor> MangaListData = new BindingList<MangaListAdaptor>();

    /// <summary>
    /// Right-click context menu on a manga entry
    /// </summary>
    private ContextMenuStrip MangaContextMenu;

    /// <summary>
    /// For deleting followed manga
    /// </summary>
    private ToolStripMenuItem ContextDeleteFollow = new ToolStripMenuItem("Delete");

    /// <summary>
    /// For moving followed manga to a different list
    /// </summary>
    private ToolStripMenuItem ContextMoveFollow = new ToolStripMenuItem("Move");

    /// <summary>
    /// For adding follow to a new list
    /// </summary>
    private ToolStripMenuItem ContextAddFollow = new ToolStripMenuItem("Add");

    /// <summary>
    /// True if showing a fixed list (e.g. search results)
    /// </summary>
    private bool FixedList;

    private FollowList LastViewedList = null;

    /// <summary>
    /// Display a list of Manga.
    /// 
    /// If a mangaList is provided, display it.
    /// 
    /// Otherwise load the follow lists from the database and display those.
    /// </summary>
    public MangaListView(IEnumerable<ObjectData<Manga>> mangaList = null)
    {
      InitializeComponent();
      MangaList.AutoGenerateColumns = false;
      MouseWheel += UI.EventHelpers.MouseWheelScrolls(MangaList);

      var contextDetail = new ToolStripMenuItem("Details");
      contextDetail.Click += MangaDetail_Click;

      ContextDeleteFollow.Click += DeleteFollow_Click;

      MangaContextMenu = new ContextMenuStrip();
      MangaContextMenu.Items.Add(contextDetail);
      MangaContextMenu.Items.Add(ContextAddFollow);

      FixedList = mangaList != null;

      MangaList.DataSource = MangaListData;

      if (mangaList == null) {
        // Use follow lists from database

        // Follow list manipulation
        MangaContextMenu.Items.Add(ContextMoveFollow);
        MangaContextMenu.Items.Add(ContextDeleteFollow);
      }
      else {
        // List provided
        foreach (var manga in mangaList)
          MangaListData.Add(new MangaListAdaptor(new MangaRecord(null,manga,null)));

        // Don't need to list selection controls
        ControlePane.Visible = false;
      }
    }

    /// <summary>
    /// Load selected list and begin retreiving status from the API
    /// </summary>
    private void LoadManga()
    {
      var list = ListSelection.SelectedItem as FollowListAdaptor;
      if (list == null)
        return;

      // If this is the same list that was previously viewed
      // save and restore the scroll position after refresh
      int scrollTo = 0;
      MangaListAdaptor prevSelectedRow = null;
      if (FollowList.Equals(LastViewedList, list.FollowList)) {
        scrollTo = MangaList.FirstDisplayedScrollingRowIndex;

        if (MangaList.SelectedRows.Count == 1)
          prevSelectedRow = MangaList.SelectedRows[0].DataBoundItem as MangaListAdaptor;
      }
      LastViewedList = list.FollowList;

      // Load the follow list from disk
      IEnumerable<Follow> follows;
      if (list.FollowList == null)
        follows = Cache.Database.GetUncategorizedFollows();
      else
        follows = Cache.Database.GetFollowList(list.FollowList);

      MangaListData.Clear();

      int selectRowIdx = -1;
      foreach (var follow in follows) {
        MangaListAdaptor adaptor;

        if (Cache.TryGetManga(this, follow, LoadedManga, out var record)) {
          adaptor = new MangaListAdaptor(record);
        }
        else {
          adaptor = new MangaListAdaptor(new MangaRecord(follow, null, null));
        }

        if (prevSelectedRow != null && adaptor.Record.Follow.RowId == prevSelectedRow.Record.Follow.RowId)
          selectRowIdx = MangaListData.Count;

        MangaListData.Add(adaptor);
      }
      
      if (MangaListData.Any() && scrollTo >= 0) {
        if (selectRowIdx >= 0)
          MangaList.Rows[selectRowIdx].Selected = true;

        if (scrollTo >= MangaListData.Count)
          scrollTo = MangaListData.Count - 1;
        MangaList.FirstDisplayedScrollingRowIndex = scrollTo;
      }
    }

    /// <summary>
    /// Incremental load of data from the API
    /// </summary>
    /// <param name="manga"></param>
    public void LoadedManga(UI.MangaRecord record)
    {
      // Leaving this loop for now, in the future the background process should keep a ref
      // to the MangaRecord and the binding list should keep track of what needs to be
      // updated and this just pings the dgv to update
      for (int i = 0; i < MangaListData.Count; i++) {
        if (MangaListData[i].Record.Follow.Id == record.Follow.Id) {
          MangaListData[i].Record = record;
          MangaListData.ResetItem(i);
          break;
        }
      }
    }

    /// <summary>
    /// Add a new manga to be followed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Add_Click(object sender, EventArgs e)
    {
      var list = ListSelection.SelectedItem as FollowListAdaptor;
      if (list == null)
        return;

      using (var dlg = new OpenFileDialog()) {
        if (dlg.ShowDialog() != DialogResult.OK)
          return;

        var file = dlg.FileName;
        this.InBackground(
          () => {
            foreach (var line in System.IO.File.ReadAllLines(file)) {
              var id = Service.ParseUriOrId(line);

              // TODO: optomize this by building a list of legacy IDs and using the bulk translate
              // Then bulk lookup using the manga search
              var mangaId = id.Bind<Guid>(
                oldId => Cache.Service.TranslateLegacyId(ObjectType.Manga, oldId),
                uuid => uuid);

              var manga = Cache.Service.GetManga(mangaId);
              var follow = new Follow(manga);

              Cache.Database.AddFollow(follow);
              Cache.Database.AddFollowToList(list.FollowList, follow);
            }
          },
          OnShow );
      }
    }

    /// <summary>
    /// Open manga in the chapter list view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MangaList_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      if (MangaList.SelectedRows.Count != 1)
        return;

      var row = MangaList.SelectedRows[0].DataBoundItem as MangaListAdaptor;
      if (row == null)
        return;

      // Not loaded yet; XXX MangaView should be written such that it can lazy-load from a Follow
      if (row.Record.Manga == null)
        return;

      if (NavigationRequested != null) {
        var list = (FixedList ? MangaListData : MangaListData.Where(mld => mld.Record.HasUnreadChapters))
          .Select(mld => mld.Record)
          .ToList();

        var args = new NavigationEventArgs(new MangaView(row.Record, list));
        NavigationRequested(this, args);
      }
    }

    /// Select row and display context menu on right click
    private void MangaList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      if (e.Button == MouseButtons.Right) {
        MangaList.Rows[e.RowIndex].Selected = true;

        MangaContextMenu.Show(Cursor.Position);
      }
    }

    private MangaListAdaptor SelectedManga
    {
      get
      {
        if (MangaList.SelectedRows.Count != 1)
          return null;

        return MangaList.SelectedRows[0].DataBoundItem as MangaListAdaptor;
      }
    }

    /// <summary>
    /// View details of the selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MangaDetail_Click(object sender, EventArgs e)
    {
      var record = SelectedManga?.Record;
      if (record == null) return;

      NavigationRequested(this, new NavigationEventArgs(new MangaDetailView(record.Manga, record.Chapters)));
    }

    /// <summary>
    /// Delete requred for an item
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void DeleteFollow_Click(object sender, EventArgs e)
    {
      var row = SelectedManga;
      if (row == null) return;

      if (MessageBox.Show("Delete follow of\n" + row.Title, "Confirm Delete", MessageBoxButtons.YesNo) != DialogResult.Yes)
        return;

      Cache.Database.DeleteFollow(row.Record.Follow);
      MangaListData.Remove(row);
    }

    /// <summary>
    /// Add a follow to the selected list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void AddFollow_Click(object sender, EventArgs e)
    {
      var row = SelectedManga;
      if (row == null) return;

      var menuItem = sender as ToolStripMenuItem;
      if (menuItem == null)
        return;

      var list = menuItem.Tag as FollowList;

      if (row.Record.Follow == null) {
        row.Record.Follow = new Follow(row.Record.Manga);
        Cache.Database.AddFollow(row.Record.Follow);
      }

      if( list != null ) 
        Cache.Database.AddFollowToList(list, row.Record.Follow);
    }

    /// <summary>
    /// Move follow between lists
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MoveFollow_Click(object sender, EventArgs e)
    {
      var row = SelectedManga;
      if (row == null) return;

      var menuItem = sender as ToolStripMenuItem;
      if (menuItem == null)
        return;

      var list = menuItem.Tag as FollowList;

      if (row.Record.Follow == null) {
        row.Record.Follow = new Follow(row.Record.Manga);
        Cache.Database.AddFollow(row.Record.Follow);
      }

      Cache.Database.RemoveFollowFromAllLists(row.Record.Follow);
      if (list != null)
        Cache.Database.AddFollowToList(list, row.Record.Follow);
    }

    /// <summary>
    /// Refresh data
    /// </summary>
    public void OnShow()
    {
      // Reload the list data
      var lists = Cache.Database.GetLists().Select(l => new FollowListAdaptor(l)).ToList();
      var listsWithNull = new List<FollowListAdaptor>(lists);
      listsWithNull.Insert(0, new FollowListAdaptor(null));

      ContextAddFollow.DropDownItems.Clear();
      ContextMoveFollow.DropDownItems.Clear();

      foreach (var list in listsWithNull) {
        var menuItemAdd = new ToolStripMenuItem(list.Name);
        menuItemAdd.Tag = list.FollowList;
        menuItemAdd.Click += AddFollow_Click;
        ContextAddFollow.DropDownItems.Add(menuItemAdd);

        var menuItemMove = new ToolStripMenuItem(list.Name);
        menuItemMove.Tag = list.FollowList;
        menuItemMove.Click += MoveFollow_Click;
        ContextMoveFollow.DropDownItems.Add(menuItemMove);
      }

      if (!FixedList) {
        var selected = ListSelection.SelectedItem as FollowListAdaptor;

        // Follow list selection
        ListSelection.DataSource = listsWithNull;

        if (selected != null) {
          for (int i = 0; i < listsWithNull.Count; i++) {
            if (listsWithNull[i].Equals(selected)) {
              ListSelection.SelectedIndex = i;
              break;
            }
          }
        }
        else
          ListSelection.SelectedIndex = 0;
        ListSelection_SelectedIndexChanged(this, null);
        return;
      }

      // A subsequent screen may have updated the content
      MangaListData.ResetBindings();
    }

    private void ListSelection_SelectedIndexChanged(object sender, EventArgs e)
    {
      MangaList.Focus();
      LoadManga();
    }

    /// <summary>
    /// Clear cache and reload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Refresh_Click(object sender, EventArgs e)
    {
      Cache.ClearCache();
      LoadManga();
    }

    private void IgnoreChapterGaps_CheckedChanged(object sender, EventArgs e)
    {
      MarkerColumn.DataPropertyName = IgnoreChapterGaps.Checked ? "New" : "Next";
    }
  }
}
