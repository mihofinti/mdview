﻿
namespace MDView
{
  partial class FollowListView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.CopyToRight = new System.Windows.Forms.Button();
      this.MoveToRight = new System.Windows.Forms.Button();
      this.MoveToLeft = new System.Windows.Forms.Button();
      this.CopyToLeft = new System.Windows.Forms.Button();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.FollowLists = new System.Windows.Forms.ListBox();
      this.AddList = new System.Windows.Forms.Button();
      this.ListNamePrompt = new System.Windows.Forms.Label();
      this.ListNameEntry = new System.Windows.Forms.TextBox();
      this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
      this.MangaListLeft = new System.Windows.Forms.CheckedListBox();
      this.FollowListLeft = new System.Windows.Forms.ComboBox();
      this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
      this.MangaListRight = new System.Windows.Forms.CheckedListBox();
      this.FollowListRight = new System.Windows.Forms.ComboBox();
      this.tableLayoutPanel1.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.tableLayoutPanel3.SuspendLayout();
      this.tableLayoutPanel4.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 4;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 2, 0);
      this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 3, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 612F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(825, 612);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // flowLayoutPanel1
      // 
      this.flowLayoutPanel1.AutoSize = true;
      this.flowLayoutPanel1.Controls.Add(this.CopyToRight);
      this.flowLayoutPanel1.Controls.Add(this.MoveToRight);
      this.flowLayoutPanel1.Controls.Add(this.MoveToLeft);
      this.flowLayoutPanel1.Controls.Add(this.CopyToLeft);
      this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
      this.flowLayoutPanel1.Location = new System.Drawing.Point(475, 3);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new System.Drawing.Size(81, 606);
      this.flowLayoutPanel1.TabIndex = 2;
      // 
      // CopyToRight
      // 
      this.CopyToRight.Location = new System.Drawing.Point(3, 3);
      this.CopyToRight.Name = "CopyToRight";
      this.CopyToRight.Size = new System.Drawing.Size(75, 23);
      this.CopyToRight.TabIndex = 1;
      this.CopyToRight.Text = "Add ▶";
      this.CopyToRight.UseVisualStyleBackColor = true;
      this.CopyToRight.Click += new System.EventHandler(this.CopyToRight_Click);
      // 
      // MoveToRight
      // 
      this.MoveToRight.Location = new System.Drawing.Point(3, 32);
      this.MoveToRight.Name = "MoveToRight";
      this.MoveToRight.Size = new System.Drawing.Size(75, 23);
      this.MoveToRight.TabIndex = 0;
      this.MoveToRight.Text = "Move ▶";
      this.MoveToRight.UseVisualStyleBackColor = true;
      this.MoveToRight.Click += new System.EventHandler(this.MoveToRight_Click);
      // 
      // MoveToLeft
      // 
      this.MoveToLeft.Location = new System.Drawing.Point(3, 61);
      this.MoveToLeft.Name = "MoveToLeft";
      this.MoveToLeft.Size = new System.Drawing.Size(75, 23);
      this.MoveToLeft.TabIndex = 0;
      this.MoveToLeft.Text = "◀ Move";
      this.MoveToLeft.UseVisualStyleBackColor = true;
      this.MoveToLeft.Click += new System.EventHandler(this.MoveToLeft_Click);
      // 
      // CopyToLeft
      // 
      this.CopyToLeft.Location = new System.Drawing.Point(3, 90);
      this.CopyToLeft.Name = "CopyToLeft";
      this.CopyToLeft.Size = new System.Drawing.Size(75, 23);
      this.CopyToLeft.TabIndex = 0;
      this.CopyToLeft.Text = "◀ Add";
      this.CopyToLeft.UseVisualStyleBackColor = true;
      this.CopyToLeft.Click += new System.EventHandler(this.CopyToLeft_Click);
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 2;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel2.Controls.Add(this.FollowLists, 0, 2);
      this.tableLayoutPanel2.Controls.Add(this.AddList, 1, 1);
      this.tableLayoutPanel2.Controls.Add(this.ListNamePrompt, 0, 0);
      this.tableLayoutPanel2.Controls.Add(this.ListNameEntry, 1, 0);
      this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 4;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 606);
      this.tableLayoutPanel2.TabIndex = 3;
      // 
      // FollowLists
      // 
      this.tableLayoutPanel2.SetColumnSpan(this.FollowLists, 2);
      this.FollowLists.DisplayMember = "Name";
      this.FollowLists.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FollowLists.FormattingEnabled = true;
      this.FollowLists.Location = new System.Drawing.Point(3, 58);
      this.FollowLists.Name = "FollowLists";
      this.FollowLists.Size = new System.Drawing.Size(194, 269);
      this.FollowLists.TabIndex = 0;
      // 
      // AddList
      // 
      this.AddList.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.AddList.Location = new System.Drawing.Point(122, 29);
      this.AddList.Name = "AddList";
      this.AddList.Size = new System.Drawing.Size(75, 23);
      this.AddList.TabIndex = 3;
      this.AddList.Text = "Add List";
      this.AddList.UseVisualStyleBackColor = true;
      this.AddList.Click += new System.EventHandler(this.AddList_Click);
      // 
      // ListNamePrompt
      // 
      this.ListNamePrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.ListNamePrompt.AutoSize = true;
      this.ListNamePrompt.Location = new System.Drawing.Point(3, 6);
      this.ListNamePrompt.Name = "ListNamePrompt";
      this.ListNamePrompt.Size = new System.Drawing.Size(54, 13);
      this.ListNamePrompt.TabIndex = 1;
      this.ListNamePrompt.Text = "List Name";
      // 
      // ListNameEntry
      // 
      this.ListNameEntry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.ListNameEntry.Location = new System.Drawing.Point(63, 3);
      this.ListNameEntry.Name = "ListNameEntry";
      this.ListNameEntry.Size = new System.Drawing.Size(134, 20);
      this.ListNameEntry.TabIndex = 2;
      // 
      // tableLayoutPanel3
      // 
      this.tableLayoutPanel3.ColumnCount = 1;
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel3.Controls.Add(this.MangaListLeft, 0, 1);
      this.tableLayoutPanel3.Controls.Add(this.FollowListLeft, 0, 0);
      this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel3.Location = new System.Drawing.Point(209, 3);
      this.tableLayoutPanel3.Name = "tableLayoutPanel3";
      this.tableLayoutPanel3.RowCount = 2;
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel3.Size = new System.Drawing.Size(260, 606);
      this.tableLayoutPanel3.TabIndex = 4;
      // 
      // MangaListLeft
      // 
      this.MangaListLeft.Dock = System.Windows.Forms.DockStyle.Fill;
      this.MangaListLeft.FormattingEnabled = true;
      this.MangaListLeft.Location = new System.Drawing.Point(3, 30);
      this.MangaListLeft.Name = "MangaListLeft";
      this.MangaListLeft.Size = new System.Drawing.Size(254, 573);
      this.MangaListLeft.TabIndex = 0;
      // 
      // FollowListLeft
      // 
      this.FollowListLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.FollowListLeft.DisplayMember = "Name";
      this.FollowListLeft.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.FollowListLeft.FormattingEnabled = true;
      this.FollowListLeft.Location = new System.Drawing.Point(3, 3);
      this.FollowListLeft.Name = "FollowListLeft";
      this.FollowListLeft.Size = new System.Drawing.Size(254, 21);
      this.FollowListLeft.TabIndex = 1;
      this.FollowListLeft.SelectedIndexChanged += new System.EventHandler(this.FollowListLeft_SelectedIndexChanged);
      // 
      // tableLayoutPanel4
      // 
      this.tableLayoutPanel4.ColumnCount = 1;
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.Controls.Add(this.MangaListRight, 0, 1);
      this.tableLayoutPanel4.Controls.Add(this.FollowListRight, 0, 0);
      this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel4.Location = new System.Drawing.Point(562, 3);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 2;
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.Size = new System.Drawing.Size(260, 606);
      this.tableLayoutPanel4.TabIndex = 5;
      // 
      // MangaListRight
      // 
      this.MangaListRight.Dock = System.Windows.Forms.DockStyle.Fill;
      this.MangaListRight.FormattingEnabled = true;
      this.MangaListRight.Location = new System.Drawing.Point(3, 30);
      this.MangaListRight.Name = "MangaListRight";
      this.MangaListRight.Size = new System.Drawing.Size(254, 573);
      this.MangaListRight.TabIndex = 0;
      // 
      // FollowListRight
      // 
      this.FollowListRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      this.FollowListRight.DisplayMember = "Name";
      this.FollowListRight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.FollowListRight.FormattingEnabled = true;
      this.FollowListRight.Location = new System.Drawing.Point(3, 3);
      this.FollowListRight.Name = "FollowListRight";
      this.FollowListRight.Size = new System.Drawing.Size(254, 21);
      this.FollowListRight.TabIndex = 1;
      this.FollowListRight.SelectedIndexChanged += new System.EventHandler(this.FollowListRight_SelectedIndexChanged);
      // 
      // FollowListView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.tableLayoutPanel1);
      this.Name = "FollowListView";
      this.Size = new System.Drawing.Size(825, 612);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel2.PerformLayout();
      this.tableLayoutPanel3.ResumeLayout(false);
      this.tableLayoutPanel4.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    private System.Windows.Forms.Button CopyToRight;
    private System.Windows.Forms.Button MoveToRight;
    private System.Windows.Forms.Button MoveToLeft;
    private System.Windows.Forms.Button CopyToLeft;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private System.Windows.Forms.ListBox FollowLists;
    private System.Windows.Forms.Label ListNamePrompt;
    private System.Windows.Forms.TextBox ListNameEntry;
    private System.Windows.Forms.Button AddList;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    private System.Windows.Forms.CheckedListBox MangaListLeft;
    private System.Windows.Forms.ComboBox FollowListLeft;
    private System.Windows.Forms.CheckedListBox MangaListRight;
    private System.Windows.Forms.ComboBox FollowListRight;
  }
}
