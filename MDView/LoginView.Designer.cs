﻿
namespace MDView
{
  partial class LoginView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.LayoutPane = new System.Windows.Forms.TableLayoutPanel();
      this.UserNamePrompt = new System.Windows.Forms.Label();
      this.PasswordPrompt = new System.Windows.Forms.Label();
      this.UserNameEntry = new System.Windows.Forms.TextBox();
      this.PasswordEntry = new System.Windows.Forms.TextBox();
      this.Login = new System.Windows.Forms.Button();
      this.DataPathPrompt = new System.Windows.Forms.Label();
      this.DataPathEntry = new System.Windows.Forms.TextBox();
      this.ProxyPrompt = new System.Windows.Forms.Label();
      this.ProxyAddressPrompt = new System.Windows.Forms.Label();
      this.ProxySocks5 = new System.Windows.Forms.CheckBox();
      this.ProxyAddressEntry = new System.Windows.Forms.TextBox();
      this.ProxyUsePrompt = new System.Windows.Forms.Label();
      this.ProxyUsePanel = new System.Windows.Forms.FlowLayoutPanel();
      this.UseProxyAlways = new System.Windows.Forms.RadioButton();
      this.UseProxyMangadex = new System.Windows.Forms.RadioButton();
      this.LayoutPane.SuspendLayout();
      this.ProxyUsePanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // LayoutPane
      // 
      this.LayoutPane.ColumnCount = 7;
      this.LayoutPane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.LayoutPane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.LayoutPane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.LayoutPane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.LayoutPane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.LayoutPane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 182F));
      this.LayoutPane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.LayoutPane.Controls.Add(this.UserNamePrompt, 1, 2);
      this.LayoutPane.Controls.Add(this.PasswordPrompt, 1, 3);
      this.LayoutPane.Controls.Add(this.UserNameEntry, 2, 2);
      this.LayoutPane.Controls.Add(this.PasswordEntry, 2, 3);
      this.LayoutPane.Controls.Add(this.Login, 2, 4);
      this.LayoutPane.Controls.Add(this.DataPathPrompt, 1, 1);
      this.LayoutPane.Controls.Add(this.DataPathEntry, 2, 1);
      this.LayoutPane.Controls.Add(this.ProxyPrompt, 4, 1);
      this.LayoutPane.Controls.Add(this.ProxySocks5, 5, 1);
      this.LayoutPane.Controls.Add(this.ProxyAddressPrompt, 4, 3);
      this.LayoutPane.Controls.Add(this.ProxyAddressEntry, 5, 3);
      this.LayoutPane.Controls.Add(this.ProxyUsePrompt, 4, 2);
      this.LayoutPane.Controls.Add(this.ProxyUsePanel, 5, 2);
      this.LayoutPane.Dock = System.Windows.Forms.DockStyle.Fill;
      this.LayoutPane.Location = new System.Drawing.Point(0, 0);
      this.LayoutPane.Name = "LayoutPane";
      this.LayoutPane.RowCount = 6;
      this.LayoutPane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.LayoutPane.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.LayoutPane.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.LayoutPane.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.LayoutPane.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.LayoutPane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.LayoutPane.Size = new System.Drawing.Size(708, 369);
      this.LayoutPane.TabIndex = 0;
      // 
      // UserNamePrompt
      // 
      this.UserNamePrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.UserNamePrompt.AutoSize = true;
      this.UserNamePrompt.Location = new System.Drawing.Point(130, 163);
      this.UserNamePrompt.Name = "UserNamePrompt";
      this.UserNamePrompt.Size = new System.Drawing.Size(60, 13);
      this.UserNamePrompt.TabIndex = 0;
      this.UserNamePrompt.Text = "User Name";
      // 
      // PasswordPrompt
      // 
      this.PasswordPrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.PasswordPrompt.AutoSize = true;
      this.PasswordPrompt.Location = new System.Drawing.Point(130, 189);
      this.PasswordPrompt.Name = "PasswordPrompt";
      this.PasswordPrompt.Size = new System.Drawing.Size(53, 13);
      this.PasswordPrompt.TabIndex = 1;
      this.PasswordPrompt.Text = "Password";
      // 
      // UserNameEntry
      // 
      this.UserNameEntry.Location = new System.Drawing.Point(196, 160);
      this.UserNameEntry.Name = "UserNameEntry";
      this.UserNameEntry.Size = new System.Drawing.Size(100, 20);
      this.UserNameEntry.TabIndex = 1;
      // 
      // PasswordEntry
      // 
      this.PasswordEntry.Location = new System.Drawing.Point(196, 186);
      this.PasswordEntry.Name = "PasswordEntry";
      this.PasswordEntry.Size = new System.Drawing.Size(100, 20);
      this.PasswordEntry.TabIndex = 2;
      this.PasswordEntry.UseSystemPasswordChar = true;
      // 
      // Login
      // 
      this.Login.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.Login.Location = new System.Drawing.Point(221, 212);
      this.Login.Name = "Login";
      this.Login.Size = new System.Drawing.Size(75, 23);
      this.Login.TabIndex = 3;
      this.Login.Text = "Login";
      this.Login.UseVisualStyleBackColor = true;
      this.Login.Click += new System.EventHandler(this.Login_Click);
      // 
      // DataPathPrompt
      // 
      this.DataPathPrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.DataPathPrompt.AutoSize = true;
      this.DataPathPrompt.Location = new System.Drawing.Point(130, 137);
      this.DataPathPrompt.Name = "DataPathPrompt";
      this.DataPathPrompt.Size = new System.Drawing.Size(54, 13);
      this.DataPathPrompt.TabIndex = 5;
      this.DataPathPrompt.Text = "Data Files";
      // 
      // DataPathEntry
      // 
      this.DataPathEntry.Location = new System.Drawing.Point(196, 134);
      this.DataPathEntry.Name = "DataPathEntry";
      this.DataPathEntry.Size = new System.Drawing.Size(100, 20);
      this.DataPathEntry.TabIndex = 0;
      // 
      // ProxyPrompt
      // 
      this.ProxyPrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.ProxyPrompt.AutoSize = true;
      this.ProxyPrompt.Location = new System.Drawing.Point(322, 137);
      this.ProxyPrompt.Name = "ProxyPrompt";
      this.ProxyPrompt.Size = new System.Drawing.Size(33, 13);
      this.ProxyPrompt.TabIndex = 5;
      this.ProxyPrompt.Text = "Proxy";
      // 
      // ProxyAddressPrompt
      // 
      this.ProxyAddressPrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.ProxyAddressPrompt.AutoSize = true;
      this.ProxyAddressPrompt.Location = new System.Drawing.Point(322, 189);
      this.ProxyAddressPrompt.Name = "ProxyAddressPrompt";
      this.ProxyAddressPrompt.Size = new System.Drawing.Size(74, 13);
      this.ProxyAddressPrompt.TabIndex = 5;
      this.ProxyAddressPrompt.Text = "Proxy Address";
      // 
      // ProxySocks5
      // 
      this.ProxySocks5.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.ProxySocks5.AutoSize = true;
      this.ProxySocks5.Location = new System.Drawing.Point(402, 135);
      this.ProxySocks5.Name = "ProxySocks5";
      this.ProxySocks5.Size = new System.Drawing.Size(77, 17);
      this.ProxySocks5.TabIndex = 10;
      this.ProxySocks5.Text = "SOCKS v5";
      this.ProxySocks5.UseVisualStyleBackColor = true;
      this.ProxySocks5.CheckedChanged += new System.EventHandler(this.ProxySocks5_CheckedChanged);
      // 
      // ProxyAddressEntry
      // 
      this.ProxyAddressEntry.Location = new System.Drawing.Point(402, 186);
      this.ProxyAddressEntry.Name = "ProxyAddressEntry";
      this.ProxyAddressEntry.Size = new System.Drawing.Size(100, 20);
      this.ProxyAddressEntry.TabIndex = 11;
      // 
      // ProxyUsePrompt
      // 
      this.ProxyUsePrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.ProxyUsePrompt.AutoSize = true;
      this.ProxyUsePrompt.Location = new System.Drawing.Point(322, 163);
      this.ProxyUsePrompt.Name = "ProxyUsePrompt";
      this.ProxyUsePrompt.Size = new System.Drawing.Size(73, 13);
      this.ProxyUsePrompt.TabIndex = 5;
      this.ProxyUsePrompt.Text = "Use Proxy For";
      // 
      // ProxyUsePanel
      // 
      this.ProxyUsePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.ProxyUsePanel.AutoSize = true;
      this.ProxyUsePanel.Controls.Add(this.UseProxyAlways);
      this.ProxyUsePanel.Controls.Add(this.UseProxyMangadex);
      this.ProxyUsePanel.Location = new System.Drawing.Point(399, 157);
      this.ProxyUsePanel.Margin = new System.Windows.Forms.Padding(0);
      this.ProxyUsePanel.Name = "ProxyUsePanel";
      this.ProxyUsePanel.Size = new System.Drawing.Size(182, 26);
      this.ProxyUsePanel.TabIndex = 12;
      // 
      // UseProxyAlways
      // 
      this.UseProxyAlways.AutoSize = true;
      this.UseProxyAlways.Location = new System.Drawing.Point(3, 3);
      this.UseProxyAlways.Name = "UseProxyAlways";
      this.UseProxyAlways.Size = new System.Drawing.Size(75, 17);
      this.UseProxyAlways.TabIndex = 0;
      this.UseProxyAlways.TabStop = true;
      this.UseProxyAlways.Text = "Everything";
      this.UseProxyAlways.UseVisualStyleBackColor = true;
      // 
      // UseProxyMangadex
      // 
      this.UseProxyMangadex.AutoSize = true;
      this.UseProxyMangadex.Location = new System.Drawing.Point(84, 3);
      this.UseProxyMangadex.Name = "UseProxyMangadex";
      this.UseProxyMangadex.Size = new System.Drawing.Size(92, 17);
      this.UseProxyMangadex.TabIndex = 1;
      this.UseProxyMangadex.TabStop = true;
      this.UseProxyMangadex.Text = "mangadex.org";
      this.UseProxyMangadex.UseVisualStyleBackColor = true;
      // 
      // LoginView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.LayoutPane);
      this.Name = "LoginView";
      this.Size = new System.Drawing.Size(708, 369);
      this.Load += new System.EventHandler(this.LoginView_Load);
      this.LayoutPane.ResumeLayout(false);
      this.LayoutPane.PerformLayout();
      this.ProxyUsePanel.ResumeLayout(false);
      this.ProxyUsePanel.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel LayoutPane;
    private System.Windows.Forms.Label UserNamePrompt;
    private System.Windows.Forms.Label PasswordPrompt;
    private System.Windows.Forms.TextBox UserNameEntry;
    private System.Windows.Forms.TextBox PasswordEntry;
    private System.Windows.Forms.Button Login;
    private System.Windows.Forms.Label DataPathPrompt;
    private System.Windows.Forms.TextBox DataPathEntry;
    private System.Windows.Forms.Label ProxyPrompt;
    private System.Windows.Forms.Label ProxyAddressPrompt;
    private System.Windows.Forms.CheckBox ProxySocks5;
    private System.Windows.Forms.TextBox ProxyAddressEntry;
    private System.Windows.Forms.Label ProxyUsePrompt;
    private System.Windows.Forms.FlowLayoutPanel ProxyUsePanel;
    private System.Windows.Forms.RadioButton UseProxyAlways;
    private System.Windows.Forms.RadioButton UseProxyMangadex;
  }
}
