﻿
namespace MDView
{
  partial class MDView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDView));
      this.MenuBar = new System.Windows.Forms.ToolStrip();
      this.NavigateBack = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.ManageLists = new System.Windows.Forms.ToolStripButton();
      this.SearchManga = new System.Windows.Forms.ToolStripButton();
      this.DebugMenu = new System.Windows.Forms.ToolStripDropDownButton();
      this.setScreenSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.downloadSchemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ActivePane = new System.Windows.Forms.Panel();
      this.showConsoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.MenuBar.SuspendLayout();
      this.SuspendLayout();
      // 
      // MenuBar
      // 
      this.MenuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NavigateBack,
            this.toolStripSeparator1,
            this.ManageLists,
            this.SearchManga,
            this.DebugMenu});
      this.MenuBar.Location = new System.Drawing.Point(0, 0);
      this.MenuBar.Name = "MenuBar";
      this.MenuBar.Size = new System.Drawing.Size(800, 25);
      this.MenuBar.TabIndex = 0;
      this.MenuBar.Text = "toolStrip1";
      // 
      // NavigateBack
      // 
      this.NavigateBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.NavigateBack.Image = ((System.Drawing.Image)(resources.GetObject("NavigateBack.Image")));
      this.NavigateBack.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.NavigateBack.Name = "NavigateBack";
      this.NavigateBack.Size = new System.Drawing.Size(23, 22);
      this.NavigateBack.Text = "Navigate Back";
      this.NavigateBack.Click += new System.EventHandler(this.NavigateBack_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
      // 
      // ManageLists
      // 
      this.ManageLists.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.ManageLists.Image = global::MDView.Properties.Resources.FollowList;
      this.ManageLists.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.ManageLists.Name = "ManageLists";
      this.ManageLists.Size = new System.Drawing.Size(23, 22);
      this.ManageLists.Text = "Manage Lists";
      this.ManageLists.Click += new System.EventHandler(this.ManageLists_Click);
      // 
      // SearchManga
      // 
      this.SearchManga.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.SearchManga.Image = global::MDView.Properties.Resources.Search;
      this.SearchManga.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.SearchManga.Name = "SearchManga";
      this.SearchManga.Size = new System.Drawing.Size(23, 22);
      this.SearchManga.Text = "Search Manga";
      this.SearchManga.Click += new System.EventHandler(this.SearchManga_Click);
      // 
      // DebugMenu
      // 
      this.DebugMenu.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
      this.DebugMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.DebugMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setScreenSizeToolStripMenuItem,
            this.downloadSchemaToolStripMenuItem,
            this.showConsoleToolStripMenuItem});
      this.DebugMenu.Image = global::MDView.Properties.Resources.About;
      this.DebugMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.DebugMenu.Name = "DebugMenu";
      this.DebugMenu.Size = new System.Drawing.Size(29, 22);
      this.DebugMenu.Text = "DebugMenu";
      // 
      // setScreenSizeToolStripMenuItem
      // 
      this.setScreenSizeToolStripMenuItem.Name = "setScreenSizeToolStripMenuItem";
      this.setScreenSizeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
      this.setScreenSizeToolStripMenuItem.Text = "Set Screen Size";
      this.setScreenSizeToolStripMenuItem.Click += new System.EventHandler(this.setScreenSizeToolStripMenuItem_Click);
      // 
      // downloadSchemaToolStripMenuItem
      // 
      this.downloadSchemaToolStripMenuItem.Name = "downloadSchemaToolStripMenuItem";
      this.downloadSchemaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
      this.downloadSchemaToolStripMenuItem.Text = "Download Schema";
      this.downloadSchemaToolStripMenuItem.Click += new System.EventHandler(this.downloadSchemaToolStripMenuItem_Click);
      // 
      // ActivePane
      // 
      this.ActivePane.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ActivePane.Location = new System.Drawing.Point(0, 25);
      this.ActivePane.Name = "ActivePane";
      this.ActivePane.Size = new System.Drawing.Size(800, 425);
      this.ActivePane.TabIndex = 1;
      // 
      // showConsoleToolStripMenuItem
      // 
      this.showConsoleToolStripMenuItem.Name = "showConsoleToolStripMenuItem";
      this.showConsoleToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
      this.showConsoleToolStripMenuItem.Text = "Show Console";
      this.showConsoleToolStripMenuItem.Click += new System.EventHandler(this.showConsoleToolStripMenuItem_Click);
      // 
      // MDView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(800, 450);
      this.Controls.Add(this.ActivePane);
      this.Controls.Add(this.MenuBar);
      this.Name = "MDView";
      this.Text = "MDView";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.MenuBar.ResumeLayout(false);
      this.MenuBar.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ToolStrip MenuBar;
    private System.Windows.Forms.ToolStripButton NavigateBack;
    private System.Windows.Forms.Panel ActivePane;
    private System.Windows.Forms.ToolStripButton ManageLists;
    private System.Windows.Forms.ToolStripButton SearchManga;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripDropDownButton DebugMenu;
    private System.Windows.Forms.ToolStripMenuItem setScreenSizeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem downloadSchemaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem showConsoleToolStripMenuItem;
  }
}