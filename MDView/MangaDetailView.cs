﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MDView.MangaDex;
using MDView.Util;

namespace MDView
{
  public partial class MangaDetailView : UserControl, INavigableControl
  {
    public string Title => Manga.Attributes.DisplayTitle;

    private ObjectData<Manga> Manga;

    private IEnumerable<ObjectData<Chapter>> Chapters;

    private int[] WeeklyUpdates = new int[24];

    private int[] MonthlyUpdates = new int[24];

    public MangaDetailView(ObjectData<Manga> manga, IEnumerable<ObjectData<Chapter>> chapters )
    {
      Manga = manga;
      Chapters = chapters;

      InitializeComponent();

      // Titles
      TitleDisplay.Text = Manga.Attributes.DisplayTitle;
      foreach (var title in Manga.Attributes.AlternateTitles) {
        // Replace spaces in titles with non-breaking space.
        // Breaking a title over lines makes it hard to read.
        // TBD; may need to also but ZWJs between kanji too.
        // Also API is currently returning all titles under "en" even when they're clearly not,
        // may need to update this code later when that's fixed
        if (title.TryGetValue("en", out var value))
          TitleDisplay.Text += ", " + value.Replace(' ', '\u00a0');
      }

      // Kick off background process to load the author
      // TODO: assuming that Authors and Artists are the same thing in the MangaDex API
      // as there's no Artist endpoint and as people can move between one or another,
      // but I have no reason to believe this is true.
      this.InBackground(
        () => {
          var ids = Manga.Relationships
            .Where(r => r.ObjectType == ObjectType.Author || r.ObjectType == ObjectType.Artist)
            .Select( r => r.Id );

          return Cache.Service.GetAuthor(ids);
        },
        (authList) => {
          AuthorDisplay.Text = String.Join(", ", authList.Select(a => a.Attributes.Name));
        });

      // Tags
      TagsDisplay.Text = String.Join(", ", Manga.Attributes.Tags.Select(tag => tag.Attributes.Name["en"]));

      foreach (var link in manga.Attributes.Links) {
        var linkLabel = new LinkLabel();
        linkLabel.Text = link.Target.ToString();
        linkLabel.Tag = link.Url;
        linkLabel.LinkClicked += LinkLabel_LinkClicked;

        LinkPane.Controls.Add(linkLabel);
      }

      // Publication Status
      PublicationStatusDisplay.Text = Manga.Attributes.PublicationStatus.ToString();

      // Description
      DescriptionDisplay.MaximumSize = new Size((int)(DescriptionDisplay.Font.Size * 60), int.MaxValue);
      // Existing descriptions have some BBCode mixed in, parse that out
      var description = new Util.BBCode(Manga.Attributes.Description["en"]);
      DescriptionDisplay.Text = description.Text;

      // Cache how many chapters were created in the past 24 weeks and 24 months
      DateTime week = DateTime.UtcNow;
      DateTime month = DateTime.UtcNow;
      for (int i = 0; i < 24; i++) {
        var prevWeek = week.AddDays(-7);
        var prevMonth = month.AddDays(-30.45); // Close enough

        WeeklyUpdates[i] = Chapters.Count(c => c.Attributes.CreatedAt.HasValue && c.Attributes.CreatedAt.Value > prevWeek && c.Attributes.CreatedAt.Value <= week);
        MonthlyUpdates[i] = Chapters.Count(c => c.Attributes.CreatedAt.HasValue && c.Attributes.CreatedAt.Value > prevMonth && c.Attributes.CreatedAt.Value <= month);

        week = prevWeek;
        month = prevMonth;
      }
    }

    /// <summary>
    /// Handle link click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void LinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      var link = sender as LinkLabel;
      if (link == null)
        return;

      var uri = link.Tag as string;
      if (uri == null)
        return;

      System.Diagnostics.Process.Start(uri); // implicit shell execute
    }

    private void UpdateHistory_Paint(object sender, PaintEventArgs e)
    {
      var black = new SolidBrush(SystemColors.ControlText);
      var solid = new SolidBrush(SystemColors.MenuHighlight);

      for (int i = 0; i < 24; i++) {
        var weekCount = WeeklyUpdates[i];
        if (weekCount > 5)
          weekCount = 5;
        weekCount *= 20;
        if (weekCount == 0)
          weekCount = 2;
        e.Graphics.FillRectangle(solid, new Rectangle(20 + 25 * i, 120 - weekCount, 20, weekCount));

        var montCount = MonthlyUpdates[i];
        if (montCount > 10)
          montCount = 10;
        montCount *= 10;
        if (montCount == 0)
          montCount = 2;
        e.Graphics.FillRectangle(solid, new Rectangle(20 + 25 * i, 240 - montCount, 20, montCount));
      }
      //var text = new GraphicsPath();
      //text.AddString("Past Weeks", SystemFonts.DefaultFont.FontFamily, (int)SystemFonts.DefaultFont.Style, SystemFonts.DefaultFont.Size, new Point(20, 122), StringFormat.GenericTypographic);

      e.Graphics.DrawString("Past Weeks", SystemFonts.DefaultFont, black, 20, 122);
      e.Graphics.DrawString("Past Months", SystemFonts.DefaultFont, black, 20, 242);
    }

    public Control Control => this;

    public event NavigationEventHandler NavigationRequested;

    public void OnShow()
    {
    }
  }
}
