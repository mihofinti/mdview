﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Config
{
  /// <summary>
  /// Details of a follow list
  /// </summary>
  public class FollowList
  {
    /// <summary>
    /// Cheap DBO mapping, maybe switch to something reflection based in the future
    /// </summary>
    private readonly Dictionary<string, Action<FollowList, SQLiteDataReader, int>> DatabaseMapping = new Dictionary<string, Action<FollowList, SQLiteDataReader, int>>() {
      { "rowid",        (f,r,i) => f.RowId = r.GetInt64(i) },
      { "name",         (f,r,i) => f.Name = r.GetString(i) },
    };

    /// <summary>
    /// Database ID
    /// </summary>
    public long RowId;

    /// <summary>
    /// List name
    /// </summary>
    public string Name;

    public override string ToString()
    {
      return String.Format("List:{0}@{1}", Name, RowId);
    }

    /// <summary>
    /// Load from database query result
    /// </summary>
    /// <param name="reader"></param>
    public FollowList(SQLiteDataReader reader)
    {
      for (int i = 0; i < reader.FieldCount; i++) {
        if (DatabaseMapping.TryGetValue(reader.GetName(i).ToLower(), out var setter)) {
          setter(this, reader, i);
        }
      }
    }

    public override bool Equals(object obj)
    {
      var other = obj as FollowList;
      if (other == null)
        return false;

      return this.RowId == other.RowId;
    }

    public static bool Equals(FollowList a, FollowList b)
    {
      if (a == null && b == null)
        return true;
      if (a == null || b == null)
        return false;
      return a.RowId == b.RowId;
    }

    public override int GetHashCode()
    {
      return RowId.GetHashCode();
    }
  }
}
