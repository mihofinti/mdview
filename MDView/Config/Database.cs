﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SQLite;

namespace MDView.Config
{
  /// <summary>
  /// Wrapper around user-configuration database
  /// </summary>
  public class Database : IDisposable
  {
    private SQLiteConnection Connection;

    /// <summary>
    /// Open an existing or create a new user-configuration database
    /// </summary>
    public Database()
    {
      Connection = new SQLiteConnection();
      Connection.ConnectionString = String.Format("Data Source={0}; BinaryGUID=False; FailIfMissing=False", Properties.Settings.Default.ConfigurationPath);
      Connection.Open();

      // Table stores manga the user follows
      // Id => MangaDex ID
      // Title => Title
      // LastRead => Date user last viewed a chapter from this manga
      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
CREATE TABLE IF NOT EXISTS Follows
  ( Id          TEXT    NOT NULL
  , Title       TEXT    NOT NULL
  , LastRead    TEXT    NULL
  , LastChapter TEXT    NULL
  , CONSTRAINT Pk PRIMARY KEY ( Id )
  )";
        cmd.ExecuteNonQuery();

        cmd.CommandText = @"
CREATE TABLE IF NOT EXISTS Lists
  ( Name        TEXT    NOT NULL
  )";
        cmd.ExecuteNonQuery();

        cmd.CommandText = @"
CREATE TABLE IF NOT EXISTS FollowToList
  ( FollowId    INT     NOT NULL
  , ListId      INT     NOT NULL
  , CONSTRAINT Pk PRIMARY KEY ( FollowId, ListId )
  )";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "PRAGMA user_version";
        long version = (long)cmd.ExecuteScalar();

        // Database Update 3/10/2022
        // Also record last read volume number
        if (version < 1) {
          cmd.CommandText = @"
ALTER TABLE Follows
ADD COLUMN LastVolume  TEXT  NULL
";
          cmd.ExecuteNonQuery();

          cmd.CommandText = "PRAGMA user_version = 1";
          cmd.ExecuteNonQuery();
        }
      }
    }

    public void Dispose()
    {
      Connection.Dispose();
    }

    /// <summary>
    /// Get all followed manga on a particular list
    /// </summary>
    /// <param name="listNo"></param>
    /// <returns></returns>
    public IEnumerable<Follow> GetFollowList(FollowList list)
    {
      var rv = new List<Follow>();

      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
SELECT Follows.rowid, Follows.Id, Follows.Title, Follows.LastRead, Follows.LastChapter
FROM Follows
JOIN FollowToList ON ( Follows.rowid = FollowToList.FollowId )
WHERE FollowToList.ListId = @ListId
ORDER BY Follows.Title
";
        cmd.Parameters.AddWithValue("ListId", list.RowId);
        using (var reader = cmd.ExecuteReader()) {
          while (reader.Read()) {
            rv.Add(new Follow(reader));
          }
        }
      }

      return rv;
    }


    /// <summary>
    /// Get all followed manga no assigned to a list
    /// </summary>
    /// <param name="listNo"></param>
    /// <returns></returns>
    public IEnumerable<Follow> GetUncategorizedFollows()
    {
      var rv = new List<Follow>();

      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
SELECT rowid, Id, Title, LastRead, LastChapter
FROM Follows
WHERE NOT EXISTS (SELECT rowId FROM FollowToList WHERE FollowToList.FollowId = Follows.rowid)
ORDER BY Title
";
        using (var reader = cmd.ExecuteReader()) {
          while (reader.Read()) {
            rv.Add(new Follow(reader));
          }
        }
      }

      return rv;
    }

    /// <summary>
    /// Get all followed manga on any list
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Follow> GetAllFollows()
    {
      var rv = new List<Follow>();

      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
SELECT rowid, Id, Title, LastRead, LastChapter
FROM Follows
ORDER BY Title
";
        using (var reader = cmd.ExecuteReader()) {
          while (reader.Read()) {
            rv.Add(new Follow(reader));
          }
        }
      }

      return rv;
    }

    /// <summary>
    /// Add a new followed manga
    /// </summary>
    /// <param name="follow"></param>
    public void AddFollow(Follow follow)
    {
      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"INSERT INTO Follows ( Id, Title, LastRead ) VALUES ( @Id, @Title, @LastRead )";
        cmd.Parameters.AddWithValue("Id", follow.Id);
        cmd.Parameters.AddWithValue("Title", follow.Title);
        cmd.Parameters.AddWithValue("LastRead", follow.LastRead);
        cmd.Parameters.AddWithValue("LastChapter", follow.LastChapter);
        cmd.ExecuteNonQuery();

        follow.RowId = Connection.LastInsertRowId;
      }
    }

    /// <summary>
    /// Delete a follow
    /// </summary>
    /// <param name="follow"></param>
    public void DeleteFollow(Follow follow)
    {
      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"DELETE FROM Follows WHERE Id=@Id";
        cmd.Parameters.AddWithValue("Id", follow.Id);
        cmd.ExecuteNonQuery();

        cmd.CommandText = @"DELETE FROM FollowToList WHERE FollowId=@Id";
        cmd.ExecuteNonQuery();
      }
    }

    /// <summary>
    /// Update last read date for a followed manga.
    /// 
    /// 
    /// </summary>
    /// <param name="follow"></param>
    public void ReadFollow(Follow follow, UI.MangaRecord manga, MangaDex.ObjectData<MangaDex.Chapter> chapter, bool allowEarlier )
    {
      var last = Util.Parse.ChapterNumber(follow.LastChapter);

      if (allowEarlier || manga.ChapterOrder.Compare(chapter, follow) > 0) {
        follow.LastVolume = chapter.Attributes.Volume;
        follow.LastChapter = chapter.Attributes.ChapterApi;
      }
      follow.LastRead = DateTime.UtcNow;

      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"UPDATE Follows SET LastRead=@LastRead, LastChapter=@LastChapter, LastVolume=@LastVolume WHERE Id=@Id";
        cmd.Parameters.AddWithValue("Id", follow.Id);
        cmd.Parameters.AddWithValue("LastRead", DateTime.UtcNow);
        cmd.Parameters.AddWithValue("LastChapter", follow.LastChapter);
        cmd.Parameters.AddWithValue("LastVolume", follow.LastVolume);
        cmd.ExecuteNonQuery();
      }
    }

    /// <summary>
    /// Add a new list
    /// </summary>
    /// <param name="name"></param>
    public void AddList(string name)
    {
      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
INSERT INTO Lists ( Name )
VALUES ( @Name )
";
        cmd.Parameters.AddWithValue("Name", name);
        cmd.ExecuteNonQuery();
      }
    }

    /// <summary>
    /// Get Lists
    /// </summary>
    /// <returns></returns>
    public IEnumerable<FollowList> GetLists()
    {
      var rv = new List<FollowList>();

      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
SELECT rowid, Name
FROM Lists
ORDER BY Name
";
        using (var reader = cmd.ExecuteReader()) {
          while (reader.Read()) {
            rv.Add(new FollowList(reader));
          }
        }
      }

      return rv;
    }

    /// <summary>
    /// Add a follow to a list
    /// </summary>
    /// <param name="list"></param>
    /// <param name="follow"></param>
    public void AddFollowToList(FollowList list, Follow follow)
    {
      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
INSERT INTO FollowToList ( ListId, FollowId )
VALUES ( @ListId, @FollowId )
";

        cmd.Parameters.AddWithValue("ListId", list.RowId);
        cmd.Parameters.AddWithValue("FollowId", follow.RowId);
        cmd.ExecuteNonQuery();
      }
    }

    /// <summary>
    /// Remove a follow from a list
    /// </summary>
    /// <param name="list"></param>
    /// <param name="follow"></param>
    public void RemoveFollowFromList(FollowList list, Follow follow)
    {
      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
DELETE FROM FollowToList
WHERE ListId = @ListId
  AND FollowId = @FollowId
";

        cmd.Parameters.AddWithValue("ListId", list.RowId);
        cmd.Parameters.AddWithValue("FollowId", follow.RowId);
        cmd.ExecuteNonQuery();
      }
    }

    /// <summary>
    /// Remove a follow from all lists leaving it uncategorized
    /// </summary>
    /// <param name="follow"></param>
    public void RemoveFollowFromAllLists(Follow follow)
    {
      using (var cmd = Connection.CreateCommand()) {
        cmd.CommandText = @"
DELETE FROM FollowToList
WHERE FollowId = @FollowId
";

        cmd.Parameters.AddWithValue("FollowId", follow.RowId);
        cmd.ExecuteNonQuery();
      }
    }
  }
}
