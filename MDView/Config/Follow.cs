﻿using MDView.MangaDex;
using MDView.Util;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Config
{
  /// <summary>
  /// Details of a follwed manga
  /// </summary>
  public class Follow
  {
    /// <summary>
    /// Cheap DBO mapping, maybe switch to something reflection based in the future
    /// </summary>
    private readonly Dictionary<string, Action<Follow, SQLiteDataReader, int>> DatabaseMapping = new Dictionary<string, Action<Follow, SQLiteDataReader, int>>() {
      { "rowid",        (f,r,i) => f.RowId = r.GetInt64(i) },
      { "id",           (f,r,i) => f.Id = r.GetGuid(i) },
      { "title",        (f,r,i) => f.Title = r.GetString(i) },
      { "lastread",     (f,r,i) => f.LastRead = r.IsDBNull(i) ? (DateTime?)null : r.GetDateTime(i) },
      { "lastchapter",  (f,r,i) => f.LastChapter = r.IsDBNull(i) ? null : r.GetString(i) },
      { "lastvolume",   (f,r,i) => f.LastVolume = r.IsDBNull(i) ? null : r.GetString(i) },
    };

    /// <summary>
    /// Sqlite-internal ID
    /// </summary>
    internal long RowId;

    /// <summary>
    /// MangaDex ID
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Manga Title
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// Date user last viewed the chapters
    /// </summary>
    public DateTime? LastRead { get; set; }

    /// <summary>
    /// Last chapter read
    /// </summary>
    public string LastChapter { get; set; }

    /// <summary>
    /// Last volume read
    /// </summary>
    public string LastVolume { get; set; }

    public override string ToString()
    {
      return String.Format("Follow:{0}@{1}", Title, Id);
    }

    /// <summary>
    /// Load from database query result
    /// </summary>
    /// <param name="reader"></param>
    public Follow(SQLiteDataReader reader)
    {
      for (int i = 0; i < reader.FieldCount; i++) {
        if (DatabaseMapping.TryGetValue(reader.GetName(i).ToLower(), out var setter)) {
          setter(this, reader, i);
        }
      }
    }

    /// <summary>
    /// Initialize from manga information
    /// </summary>
    /// <param name="manga"></param>
    /// <param name="list"></param>
    public Follow(ObjectData<Manga> manga)
    {
      Id = manga.Id;
      Title = manga.Attributes.DisplayTitle;
      LastRead = null;
      LastChapter = null;
    }
  }
}
