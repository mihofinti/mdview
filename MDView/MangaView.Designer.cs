﻿
namespace MDView
{
  partial class MangaView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.ChapterList = new System.Windows.Forms.DataGridView();
      this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.MangaDetails = new System.Windows.Forms.Button();
      this.PrevManga = new System.Windows.Forms.Button();
      this.NextManga = new System.Windows.Forms.Button();
      this.MangaList = new System.Windows.Forms.ComboBox();
      this.ChapterContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.ContextMenuSetLastRead = new System.Windows.Forms.ToolStripMenuItem();
      this.LastRead = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.ChapterList)).BeginInit();
      this.tableLayoutPanel1.SuspendLayout();
      this.ChapterContextMenu.SuspendLayout();
      this.SuspendLayout();
      // 
      // ChapterList
      // 
      this.ChapterList.AllowUserToAddRows = false;
      this.ChapterList.AllowUserToDeleteRows = false;
      this.ChapterList.AllowUserToResizeRows = false;
      this.ChapterList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.ChapterList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column1,
            this.Column2,
            this.Column5,
            this.Column3});
      this.ChapterList.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ChapterList.Location = new System.Drawing.Point(0, 0);
      this.ChapterList.MultiSelect = false;
      this.ChapterList.Name = "ChapterList";
      this.ChapterList.ReadOnly = true;
      this.ChapterList.RowHeadersVisible = false;
      this.ChapterList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.ChapterList.Size = new System.Drawing.Size(738, 433);
      this.ChapterList.TabIndex = 1;
      this.ChapterList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ChapterList_CellFormatting);
      this.ChapterList.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ChapterList_CellMouseClick);
      this.ChapterList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ChapterList_CellMouseDoubleClick);
      // 
      // Column4
      // 
      this.Column4.DataPropertyName = "Marker";
      this.Column4.HeaderText = " ";
      this.Column4.Name = "Column4";
      this.Column4.ReadOnly = true;
      this.Column4.Width = 50;
      // 
      // Column1
      // 
      this.Column1.DataPropertyName = "Chapter";
      this.Column1.HeaderText = "Chapter";
      this.Column1.Name = "Column1";
      this.Column1.ReadOnly = true;
      // 
      // Column2
      // 
      this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.Column2.DataPropertyName = "Title";
      this.Column2.HeaderText = "Title";
      this.Column2.Name = "Column2";
      this.Column2.ReadOnly = true;
      // 
      // Column5
      // 
      this.Column5.DataPropertyName = "ScanlationGroupName";
      this.Column5.HeaderText = "Scanlation Group";
      this.Column5.Name = "Column5";
      this.Column5.ReadOnly = true;
      this.Column5.Width = 200;
      // 
      // Column3
      // 
      this.Column3.DataPropertyName = "Date";
      this.Column3.HeaderText = "Date";
      this.Column3.Name = "Column3";
      this.Column3.ReadOnly = true;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.AutoSize = true;
      this.tableLayoutPanel1.ColumnCount = 6;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.Controls.Add(this.MangaDetails, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.PrevManga, 3, 0);
      this.tableLayoutPanel1.Controls.Add(this.NextManga, 5, 0);
      this.tableLayoutPanel1.Controls.Add(this.MangaList, 4, 0);
      this.tableLayoutPanel1.Controls.Add(this.LastRead, 1, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 433);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size = new System.Drawing.Size(738, 29);
      this.tableLayoutPanel1.TabIndex = 2;
      // 
      // MangaDetails
      // 
      this.MangaDetails.Location = new System.Drawing.Point(3, 3);
      this.MangaDetails.Name = "MangaDetails";
      this.MangaDetails.Size = new System.Drawing.Size(75, 23);
      this.MangaDetails.TabIndex = 0;
      this.MangaDetails.Text = "Details";
      this.MangaDetails.UseVisualStyleBackColor = true;
      this.MangaDetails.Click += new System.EventHandler(this.MangaDetails_Click);
      // 
      // PrevManga
      // 
      this.PrevManga.Location = new System.Drawing.Point(469, 3);
      this.PrevManga.Name = "PrevManga";
      this.PrevManga.Size = new System.Drawing.Size(27, 23);
      this.PrevManga.TabIndex = 3;
      this.PrevManga.Text = "◀";
      this.PrevManga.UseVisualStyleBackColor = true;
      this.PrevManga.Click += new System.EventHandler(this.PrevManga_Click);
      // 
      // NextManga
      // 
      this.NextManga.Location = new System.Drawing.Point(708, 3);
      this.NextManga.Name = "NextManga";
      this.NextManga.Size = new System.Drawing.Size(27, 23);
      this.NextManga.TabIndex = 4;
      this.NextManga.Text = "▶";
      this.NextManga.UseVisualStyleBackColor = true;
      this.NextManga.Click += new System.EventHandler(this.NextManga_Click);
      // 
      // MangaList
      // 
      this.MangaList.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.MangaList.DisplayMember = "Display";
      this.MangaList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.MangaList.FormattingEnabled = true;
      this.MangaList.Location = new System.Drawing.Point(502, 4);
      this.MangaList.Name = "MangaList";
      this.MangaList.Size = new System.Drawing.Size(200, 21);
      this.MangaList.TabIndex = 5;
      this.MangaList.SelectionChangeCommitted += new System.EventHandler(this.MangaList_SelectionChangeCommitted);
      // 
      // ChapterContextMenu
      // 
      this.ChapterContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextMenuSetLastRead});
      this.ChapterContextMenu.Name = "ChapterContextMenu";
      this.ChapterContextMenu.Size = new System.Drawing.Size(144, 26);
      // 
      // ContextMenuSetLastRead
      // 
      this.ContextMenuSetLastRead.Name = "ContextMenuSetLastRead";
      this.ContextMenuSetLastRead.Size = new System.Drawing.Size(143, 22);
      this.ContextMenuSetLastRead.Text = "Set Last Read";
      this.ContextMenuSetLastRead.Click += new System.EventHandler(this.ContextMenuSetLastRead_Click);
      // 
      // LastRead
      // 
      this.LastRead.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.LastRead.AutoSize = true;
      this.LastRead.Location = new System.Drawing.Point(84, 8);
      this.LastRead.Name = "LastRead";
      this.LastRead.Size = new System.Drawing.Size(56, 13);
      this.LastRead.TabIndex = 6;
      this.LastRead.Text = "Last Read";
      // 
      // MangaView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.ChapterList);
      this.Controls.Add(this.tableLayoutPanel1);
      this.Name = "MangaView";
      this.Size = new System.Drawing.Size(738, 462);
      ((System.ComponentModel.ISupportInitialize)(this.ChapterList)).EndInit();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ChapterContextMenu.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView ChapterList;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Button MangaDetails;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    private System.Windows.Forms.Button PrevManga;
    private System.Windows.Forms.Button NextManga;
    private System.Windows.Forms.ComboBox MangaList;
    private System.Windows.Forms.ContextMenuStrip ChapterContextMenu;
    private System.Windows.Forms.ToolStripMenuItem ContextMenuSetLastRead;
    private System.Windows.Forms.Label LastRead;
  }
}
