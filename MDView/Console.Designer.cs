﻿
namespace MDView
{
  partial class Console
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.ConsoleLines = new System.Windows.Forms.ListBox();
      this.UpdateTick = new System.Windows.Forms.Timer(this.components);
      this.SuspendLayout();
      // 
      // ConsoleLines
      // 
      this.ConsoleLines.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ConsoleLines.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.ConsoleLines.FormattingEnabled = true;
      this.ConsoleLines.ItemHeight = 18;
      this.ConsoleLines.Location = new System.Drawing.Point(0, 0);
      this.ConsoleLines.Name = "ConsoleLines";
      this.ConsoleLines.Size = new System.Drawing.Size(800, 450);
      this.ConsoleLines.TabIndex = 0;
      this.ConsoleLines.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Console_KeyUp);
      // 
      // UpdateTick
      // 
      this.UpdateTick.Enabled = true;
      this.UpdateTick.Tick += new System.EventHandler(this.UpdateTick_Tick);
      // 
      // Console
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(800, 450);
      this.Controls.Add(this.ConsoleLines);
      this.Name = "Console";
      this.Text = "Console";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Console_FormClosing);
      this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Console_KeyUp);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListBox ConsoleLines;
    private System.Windows.Forms.Timer UpdateTick;
  }
}