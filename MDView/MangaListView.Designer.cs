﻿
namespace MDView
{
  partial class MangaListView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.MangaList = new System.Windows.Forms.DataGridView();
      this.MarkerColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ControlePane = new System.Windows.Forms.TableLayoutPanel();
      this.Add = new System.Windows.Forms.Button();
      this.ListSelection = new System.Windows.Forms.ComboBox();
      this.Refresh = new System.Windows.Forms.Button();
      this.IgnoreChapterGaps = new System.Windows.Forms.CheckBox();
      ((System.ComponentModel.ISupportInitialize)(this.MangaList)).BeginInit();
      this.ControlePane.SuspendLayout();
      this.SuspendLayout();
      // 
      // MangaList
      // 
      this.MangaList.AllowUserToAddRows = false;
      this.MangaList.AllowUserToDeleteRows = false;
      this.MangaList.AllowUserToResizeRows = false;
      this.MangaList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.MangaList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MarkerColumn,
            this.Column1,
            this.Column2,
            this.Column4});
      this.MangaList.Dock = System.Windows.Forms.DockStyle.Fill;
      this.MangaList.Location = new System.Drawing.Point(0, 0);
      this.MangaList.MultiSelect = false;
      this.MangaList.Name = "MangaList";
      this.MangaList.ReadOnly = true;
      this.MangaList.RowHeadersVisible = false;
      this.MangaList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.MangaList.Size = new System.Drawing.Size(434, 409);
      this.MangaList.TabIndex = 1;
      this.MangaList.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MangaList_CellMouseClick);
      this.MangaList.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.MangaList_CellMouseDoubleClick);
      // 
      // MarkerColumn
      // 
      this.MarkerColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
      this.MarkerColumn.DataPropertyName = "New";
      this.MarkerColumn.HeaderText = "New";
      this.MarkerColumn.Name = "MarkerColumn";
      this.MarkerColumn.ReadOnly = true;
      this.MarkerColumn.Width = 54;
      // 
      // Column1
      // 
      this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.Column1.DataPropertyName = "Title";
      this.Column1.HeaderText = "Title";
      this.Column1.Name = "Column1";
      this.Column1.ReadOnly = true;
      // 
      // Column2
      // 
      this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
      this.Column2.DataPropertyName = "LastChapter";
      this.Column2.HeaderText = "Last Chapter";
      this.Column2.Name = "Column2";
      this.Column2.ReadOnly = true;
      this.Column2.Width = 92;
      // 
      // Column4
      // 
      this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
      this.Column4.DataPropertyName = "LastRead";
      this.Column4.HeaderText = "Last Read";
      this.Column4.Name = "Column4";
      this.Column4.ReadOnly = true;
      this.Column4.Width = 81;
      // 
      // ControlePane
      // 
      this.ControlePane.ColumnCount = 5;
      this.ControlePane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.ControlePane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.ControlePane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.ControlePane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.ControlePane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.ControlePane.Controls.Add(this.Add, 0, 0);
      this.ControlePane.Controls.Add(this.ListSelection, 4, 0);
      this.ControlePane.Controls.Add(this.Refresh, 1, 0);
      this.ControlePane.Controls.Add(this.IgnoreChapterGaps, 2, 0);
      this.ControlePane.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.ControlePane.Location = new System.Drawing.Point(0, 409);
      this.ControlePane.Name = "ControlePane";
      this.ControlePane.RowCount = 1;
      this.ControlePane.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.ControlePane.Size = new System.Drawing.Size(434, 31);
      this.ControlePane.TabIndex = 2;
      // 
      // Add
      // 
      this.Add.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.Add.AutoSize = true;
      this.Add.Location = new System.Drawing.Point(3, 4);
      this.Add.Name = "Add";
      this.Add.Size = new System.Drawing.Size(46, 23);
      this.Add.TabIndex = 2;
      this.Add.Text = "Import";
      this.Add.UseVisualStyleBackColor = true;
      this.Add.Click += new System.EventHandler(this.Add_Click);
      // 
      // ListSelection
      // 
      this.ListSelection.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.ListSelection.DisplayMember = "Name";
      this.ListSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.ListSelection.FormattingEnabled = true;
      this.ListSelection.Location = new System.Drawing.Point(310, 5);
      this.ListSelection.Name = "ListSelection";
      this.ListSelection.Size = new System.Drawing.Size(121, 21);
      this.ListSelection.TabIndex = 3;
      this.ListSelection.SelectionChangeCommitted += new System.EventHandler(this.ListSelection_SelectedIndexChanged);
      // 
      // Refresh
      // 
      this.Refresh.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.Refresh.AutoSize = true;
      this.Refresh.Location = new System.Drawing.Point(55, 4);
      this.Refresh.Name = "Refresh";
      this.Refresh.Size = new System.Drawing.Size(54, 23);
      this.Refresh.TabIndex = 2;
      this.Refresh.Text = "Refresh";
      this.Refresh.UseVisualStyleBackColor = true;
      this.Refresh.Click += new System.EventHandler(this.Refresh_Click);
      // 
      // IgnoreChapterGaps
      // 
      this.IgnoreChapterGaps.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.IgnoreChapterGaps.AutoSize = true;
      this.IgnoreChapterGaps.Checked = true;
      this.IgnoreChapterGaps.CheckState = System.Windows.Forms.CheckState.Checked;
      this.IgnoreChapterGaps.Location = new System.Drawing.Point(115, 7);
      this.IgnoreChapterGaps.Name = "IgnoreChapterGaps";
      this.IgnoreChapterGaps.Size = new System.Drawing.Size(124, 17);
      this.IgnoreChapterGaps.TabIndex = 4;
      this.IgnoreChapterGaps.Text = "Ignore Chapter Gaps";
      this.IgnoreChapterGaps.UseVisualStyleBackColor = true;
      this.IgnoreChapterGaps.CheckedChanged += new System.EventHandler(this.IgnoreChapterGaps_CheckedChanged);
      // 
      // MangaListView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.MangaList);
      this.Controls.Add(this.ControlePane);
      this.Name = "MangaListView";
      this.Size = new System.Drawing.Size(434, 440);
      ((System.ComponentModel.ISupportInitialize)(this.MangaList)).EndInit();
      this.ControlePane.ResumeLayout(false);
      this.ControlePane.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion
    private System.Windows.Forms.DataGridView MangaList;
    private System.Windows.Forms.TableLayoutPanel ControlePane;
    private System.Windows.Forms.Button Add;
    private System.Windows.Forms.ComboBox ListSelection;
    private System.Windows.Forms.Button Refresh;
    private System.Windows.Forms.CheckBox IgnoreChapterGaps;
    private System.Windows.Forms.DataGridViewTextBoxColumn MarkerColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
  }
}
