﻿
namespace MDView
{
  partial class MangaSearchView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.SearchTitlePrompt = new System.Windows.Forms.Label();
      this.SearchTitleEntry = new System.Windows.Forms.TextBox();
      this.Search = new System.Windows.Forms.Button();
      this.MangaDexIdPrompt = new System.Windows.Forms.Label();
      this.MangaDexIdEntry = new System.Windows.Forms.TextBox();
      this.MangaDexIdHint = new System.Windows.Forms.Label();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 4;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.Search, 2, 4);
      this.tableLayoutPanel1.Controls.Add(this.SearchTitlePrompt, 1, 3);
      this.tableLayoutPanel1.Controls.Add(this.MangaDexIdPrompt, 1, 1);
      this.tableLayoutPanel1.Controls.Add(this.SearchTitleEntry, 2, 3);
      this.tableLayoutPanel1.Controls.Add(this.MangaDexIdEntry, 2, 1);
      this.tableLayoutPanel1.Controls.Add(this.MangaDexIdHint, 2, 2);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 6;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(908, 545);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // SearchTitlePrompt
      // 
      this.SearchTitlePrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.SearchTitlePrompt.AutoSize = true;
      this.SearchTitlePrompt.Location = new System.Drawing.Point(201, 270);
      this.SearchTitlePrompt.Name = "SearchTitlePrompt";
      this.SearchTitlePrompt.Size = new System.Drawing.Size(27, 13);
      this.SearchTitlePrompt.TabIndex = 0;
      this.SearchTitlePrompt.Text = "Title";
      // 
      // SearchTitleEntry
      // 
      this.SearchTitleEntry.Location = new System.Drawing.Point(307, 267);
      this.SearchTitleEntry.Name = "SearchTitleEntry";
      this.SearchTitleEntry.Size = new System.Drawing.Size(400, 20);
      this.SearchTitleEntry.TabIndex = 1;
      // 
      // Search
      // 
      this.Search.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.Search.Location = new System.Drawing.Point(632, 293);
      this.Search.Name = "Search";
      this.Search.Size = new System.Drawing.Size(75, 23);
      this.Search.TabIndex = 2;
      this.Search.Text = "Search";
      this.Search.UseVisualStyleBackColor = true;
      this.Search.Click += new System.EventHandler(this.Search_Click);
      // 
      // MangaDexIdPrompt
      // 
      this.MangaDexIdPrompt.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.MangaDexIdPrompt.AutoSize = true;
      this.MangaDexIdPrompt.Location = new System.Drawing.Point(201, 231);
      this.MangaDexIdPrompt.Name = "MangaDexIdPrompt";
      this.MangaDexIdPrompt.Size = new System.Drawing.Size(100, 13);
      this.MangaDexIdPrompt.TabIndex = 0;
      this.MangaDexIdPrompt.Text = "MangaDex ID/URL";
      // 
      // MangaDexIdEntry
      // 
      this.MangaDexIdEntry.Location = new System.Drawing.Point(307, 228);
      this.MangaDexIdEntry.Name = "MangaDexIdEntry";
      this.MangaDexIdEntry.Size = new System.Drawing.Size(400, 20);
      this.MangaDexIdEntry.TabIndex = 1;
      // 
      // MangaDexIdHint
      // 
      this.MangaDexIdHint.Anchor = System.Windows.Forms.AnchorStyles.Right;
      this.MangaDexIdHint.AutoSize = true;
      this.MangaDexIdHint.Location = new System.Drawing.Point(415, 251);
      this.MangaDexIdHint.Name = "MangaDexIdHint";
      this.MangaDexIdHint.Size = new System.Drawing.Size(292, 13);
      this.MangaDexIdHint.TabIndex = 0;
      this.MangaDexIdHint.Text = "(If MangaDex ID supplied, no other search will be performed)";
      // 
      // MangaSearchView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.tableLayoutPanel1);
      this.Name = "MangaSearchView";
      this.Size = new System.Drawing.Size(908, 545);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label SearchTitlePrompt;
    private System.Windows.Forms.TextBox SearchTitleEntry;
    private System.Windows.Forms.Button Search;
    private System.Windows.Forms.Label MangaDexIdPrompt;
    private System.Windows.Forms.TextBox MangaDexIdEntry;
    private System.Windows.Forms.Label MangaDexIdHint;
  }
}
