﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView.UI
{
  /// <summary>
  /// Event handler generators for common use cases
  /// </summary>
  public static class EventHelpers
  {
    /// <summary>
    /// Redirect mouse wheel eventsto a DataGridView
    /// </summary>
    /// <param name="dataGridView"></param>
    /// <returns></returns>
    public static MouseEventHandler MouseWheelScrolls(DataGridView dataGridView)
    {
      return (sender, eventArgs) => {
        if (dataGridView.RowCount == 0)
          return;

        var delta = eventArgs.Delta / SystemInformation.MouseWheelScrollDelta;
        delta *= SystemInformation.MouseWheelScrollLines;

        var index = dataGridView.FirstDisplayedScrollingRowIndex;
        index -= delta;

        if (index < 0)
          index = 0;
        else if (index >= dataGridView.RowCount)
          index = dataGridView.RowCount - 1;

        dataGridView.FirstDisplayedScrollingRowIndex = index;
      };
    }
  }
}

