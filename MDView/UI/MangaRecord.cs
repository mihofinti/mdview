﻿using MDView.Config;
using MDView.MangaDex;
using MDView.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.UI
{
  /// <summary>
  /// Capture all details of a Manga along with properties that cover most UI elements
  /// </summary>
  public class MangaRecord
  {
    /// <summary>
    /// Follow or null if not yet followed
    /// </summary>
    public Follow Follow { get; set; }

    /// <summary>
    /// Manga information
    /// </summary>
    public ObjectData<Manga> Manga { get; set; }

    /// <summary>
    /// Chapter information
    /// </summary>
    public List<ObjectData<Chapter>> Chapters { get; set; }

    public MangaRecord(Follow follow, ObjectData<Manga> manga, List<ObjectData<Chapter>> chapters)
    {
      Follow = follow;
      Manga = manga;
      Chapters = chapters;
    }

    /// <summary>
    /// Title using the Manga data (if loaded) otherwise the follow data
    /// </summary>
    public string Title => Manga?.Attributes.DisplayTitle ?? Follow?.Title;

    /// <summary>
    /// Most recent time anything has been updated
    /// </summary>
    public DateTime? LatestUpdate
    {
      get
      {
        if (Manga == null)
          return null;

        DateTime? max = Manga.Attributes.UpdatedAt;

        if (Chapters != null && Chapters.Any()) {
          // Service.GetChapters orders by updatedAt so that we can do this cheaply
          max = Util.Maybe.Maximum(max, Chapters[0].Attributes.UpdatedAt);
        }

        return max;
      }
    }

    /// <summary>
    /// True if the manga should be shown as having new chapters
    /// </summary>
    public bool HasUnreadChapters
    {
      get
      {
        // Don't show as new if we're waiting for data to load
        // Or aren't following this series
        if (Follow == null || Manga == null || Chapters == null)
          return false;

        // Has no chapters
        if (!Chapters.Any())
          return false;

        // Show as new if we've never read from this
        if (!Follow.LastRead.HasValue || Follow.LastChapter == null)
          return true;

        // Show as new if any chapter has been updated since we last read a chapter
        if (Chapters.Any(c => Util.Maybe.Maximum(c.Attributes.CreatedAt, c.Attributes.PublishedAt, c.Attributes.UpdatedAt) > Follow.LastRead.Value))
          return true;

        // Show as new if the last read chapter isn't the highest numbered
        var lastPubChapter = Chapters.Max(ChapterOrder);
        return ChapterOrder.Compare(lastPubChapter,Follow) > 0;
      }
    }

    /// <summary>
    /// True if there exists a chapter that (probably) follows the last read chapter
    /// </summary>
    public bool HasNextChapter
    {
      get
      {
        // Don't show as new if we're waiting for data to load
        // Or aren't following this series
        if (Follow == null || Manga == null || Chapters == null)
          return false;

        // Has no chapters
        if (!Chapters.Any())
          return false;

        // Some series will have a chapter 0, most will start at 1
        // Consider either the next chapter if none have been read yet
        if (Follow.LastChapter == null)
          return Chapters.Any(ch => ch.Attributes.Number.Item1 <= 1);

        // Otherwise check for a chapter that follows the one that was
        // last read
        return Chapters.Any(ch => ChapterOrder.IsSequential(Follow, ch));
      }
    }

    public ChapterComparer ChapterOrder
    {
      get
      {
        return new ChapterComparer(Manga.Attributes.ChapterNumberResetOnNewVolume);
      }
    }

    /// <summary>
    /// Good enough comparison of chapter values.
    /// 
    /// First compare the Volume number (if present, assume no volume to be before any other)
    /// Then extra a leading numeric portion of the chapter number and compare.
    /// Then perform string comparison on the remainder.
    /// 
    /// This covers the common cases of strings like
    ///     Vol 1, Chapter 2
    ///     Vol 1, Chapter 2.5
    ///     Vol 1, Chapter 2.Omake
    /// And so on.  It fails if someone has 10+ 'subchapters' or if the chapters have alpha identifiers.
    /// But there's only 8 chars in the Chapter field so we're not expecting people to get too creative.
    /// </summary>
    public class ChapterComparer : IComparer<ObjectData<Chapter>>
    {
      private bool VolumeMeaningful;

      public ChapterComparer(bool volumeMeaningful)
      {
        VolumeMeaningful = volumeMeaningful;
      }

      public int Compare(ObjectData<Chapter> x, ObjectData<Chapter> y)
      {
        return Compare(x.Attributes.Volume, x.Attributes.Number, y.Attributes.Volume, y.Attributes.Number);
      }

      public int Compare(ObjectData<Chapter> x, Follow y)
      {
        return Compare(x.Attributes.Volume, x.Attributes.Number, y.LastVolume, Util.Parse.ChapterNumber(y.LastChapter));
      }

      public int Compare(Follow x, ObjectData<Chapter> y)
      {
        return Compare(x.LastVolume, Util.Parse.ChapterNumber(x.LastChapter), y.Attributes.Volume, y.Attributes.Number);
      }

      private int Compare(string xVolume, Tuple<int, string> xChapter, string yVolume, Tuple<int, string> yChapter)
      {
        if (VolumeMeaningful && xVolume != yVolume) {
          int.TryParse(xVolume, out var xVolNum);
          int.TryParse(yVolume, out var yVolNum);

          return xVolNum.CompareTo(yVolNum);
        }

        if (xChapter.Item1 != yChapter.Item1)
          return xChapter.Item1.CompareTo(yChapter.Item1);

        return xChapter.Item2.CompareTo(yChapter.Item2);
      }

      /// <summary>
      /// Determine if two chapters are (probably) sequential.
      /// </summary>
      /// <param name="x"></param>
      /// <param name="y"></param>
      /// <returns></returns>
      public bool IsSequential(Follow x, ObjectData<Chapter> y)
      {
        // If the chapter numbers are specific to a volume and the volume numbers are different
        // This can only be sequential if y is the first chapter of the next volume
        if (VolumeMeaningful && x.LastVolume != y.Attributes.Volume) {
          int.TryParse(x.LastVolume, out var xVolNum);
          int.TryParse(y.Attributes.Volume, out var yVolNum);

          return xVolNum + 1 == yVolNum && y.Attributes.Number.Item1 <= 1;
        }

        // Otherwise (they are in the same volume, or volume numbers do not matter)
        // They are sequential if y is the next chapter, or an extra chapter
        var xCh = Util.Parse.ChapterNumber(x.LastChapter);

        if (xCh.Item1 == y.Attributes.Number.Item1)
          return xCh.Item2.CompareTo(y.Attributes.Number.Item2) < 0;
        else
          return xCh.Item1 + 1 == y.Attributes.Number.Item1;
      }
    }
  }
}
