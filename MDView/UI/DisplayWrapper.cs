﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.UI
{
  /// <summary>
  /// Associate a string value with an object.
  /// Covers the simple case where a control lacks a DisplayMember property
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class DisplayWrapper<T>
  {
    public readonly T Value;

    public readonly String Display;

    public DisplayWrapper(T value, string display)
    {
      Value = value;
      Display = display;
    }

    public override string ToString()
    {
      return Display;
    }
  }
}
