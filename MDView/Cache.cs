﻿using MDView.Config;
using MDView.MangaDex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Collections.Concurrent;

namespace MDView
{
  /// <summary>
  /// Global resources
  /// </summary>
  public class Cache
  {
    /// <summary>
    /// Connection to the MangaDex API
    /// </summary>
    public static Service Service;

    /// <summary>
    /// Connection to the user configuration database
    /// </summary>
    public static Database Database;

    private static CancellationTokenSource IsShutdown = new CancellationTokenSource();

    /// <summary>
    /// Intitialize global resources and validate user credentials
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    public static void Connect(string userName, string password, UseProxy useProxy, string proxyAddress, int proxyPort)
    {
      Database = new Database();

      Service = new Service(useProxy, proxyAddress, proxyPort);
      if (!String.IsNullOrWhiteSpace(userName))
        Service.Login(userName, password);

      Task.Run(BackgroundMangaLoader);
      Task.Run(BackgroundChapterLoader);
    }

    /// <summary>
    /// Dispose global resources
    /// </summary>
    public static void Shutdown()
    {
      IsShutdown.Cancel();
      Service?.Dispose();
      Database?.Dispose();
    }


    /// <summary>
    /// Wrapper around an action that knows the UI control it should be invoked on.
    /// </summary>
    private class DiscardableCallback
    {
      /// <summary>
      /// Callback to be executed
      /// </summary>
      private readonly Action<UI.MangaRecord> Callback;

      /// <summary>
      /// UI Control the callback should be run on.
      /// </summary>
      private readonly System.Windows.Forms.Control Control;

      public bool IsAlive => !Control.IsDisposed;

      public DiscardableCallback(System.Windows.Forms.Control control, Action<UI.MangaRecord> callback)
      {
        Callback = callback;
        Control = control;
      }

      /// <summary>
      /// Execute the callback if the UI control is still around
      /// </summary>
      /// <param name="record"></param>
      public void Call(UI.MangaRecord record)
      {
        if (IsAlive)
          Control.Invoke(Callback, record);
      }
    }

    /// <summary>
    /// List of MangaId that a UI screen wants to have looked up
    /// </summary>
    private static BlockingCollection<Tuple<DiscardableCallback, Follow>> PendingManga = new BlockingCollection<Tuple<DiscardableCallback, Follow>>();

    /// <summary>
    /// List of MangaRecords that need chapter information retreived
    /// </summary>
    private static BlockingCollection<Tuple<DiscardableCallback, UI.MangaRecord>> RequestedManga = new BlockingCollection<Tuple<DiscardableCallback, UI.MangaRecord>>();

    /// <summary>
    /// List of cached manga
    /// </summary>
    private static Dictionary<Guid, UI.MangaRecord> CachedManga = new Dictionary<Guid, UI.MangaRecord>();

    /// <summary>
    /// Get details of a manga and invoke a callback when ready
    /// </summary>
    /// <param name="control"></param>
    /// <param name="follow"></param>
    /// <param name="callback"></param>
    public static void GetManga(System.Windows.Forms.Control control, Follow follow, Action<UI.MangaRecord> callback)
    {
      if (CachedManga.TryGetValue(follow.Id, out var record)) {
        callback(record);
      }
      else {
        PendingManga.Add(Tuple.Create(new DiscardableCallback(control, callback), follow));
      }
    }

    /// <summary>
    /// Get details of a manga and invoke a callback when ready.
    /// 
    /// If the details are already present return the value instead for immediate processing
    /// </summary>
    /// <param name="control"></param>
    /// <param name="follow"></param>
    /// <param name="callback"></param>
    public static bool TryGetManga(System.Windows.Forms.Control control, Follow follow, Action<UI.MangaRecord> callback, out UI.MangaRecord record)
    {
      if (CachedManga.TryGetValue(follow.Id, out record)) {
        return true;
      }

      PendingManga.Add(Tuple.Create(new DiscardableCallback(control, callback), follow));
      return false;
    }

    /// <summary>
    /// Remove all cached values
    /// </summary>
    public static void ClearCache()
    {
      CachedManga.Clear();
    }

    /// <summary>
    /// Background thread for loading Manga series information
    /// </summary>
    public static void BackgroundMangaLoader()
    {
      try {
        while (!IsShutdown.IsCancellationRequested) {

          // Get pending manga to look up
          var requests = new List<Tuple<DiscardableCallback, Follow>>();
          requests.Add(PendingManga.Take(IsShutdown.Token));

          // Attempt to get MAX_BATCH_REQUESTS, but if there aren't that
          // many in the queue, start the look up with whatever is available
          while (requests.Count < Service.MAX_BATCH_REQUESTS) {
            if (PendingManga.TryTake(out var request))
              requests.Add(request);
            else
              break;
          }

          var manga = Service.GetManga(requests.Select(r => r.Item2.Id));

          foreach (var result in manga) {
            // Linear search should be fast enough when there's at most 50 recs.
            var request = requests.First(r => r.Item2.Id == result.Id);
            var record = new UI.MangaRecord(request.Item2, result, null);

            RequestedManga.Add(Tuple.Create(request.Item1, record));
          }
        }
      }
      catch (OperationCanceledException) {
      }
    }

    /// <summary>
    /// Background thread for loading chapter information
    /// </summary>
    public static void BackgroundChapterLoader()
    {
      try {
        while (!IsShutdown.IsCancellationRequested) {
          var request = RequestedManga.Take(IsShutdown.Token);
          if (!request.Item1.IsAlive)
            continue;

          request.Item2.Chapters = Service.GetChapters(request.Item2.Manga.Id).ToList();

          CachedManga[request.Item2.Manga.Id] = request.Item2;
          request.Item1.Call(request.Item2);
        }
      }
      catch (OperationCanceledException) {
      }
    }
  }
}
