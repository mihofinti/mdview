﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Util
{
  public static class Dictionary
  {
    public static V FirstValuePresent<K, V>(this Dictionary<K, V> dictionary, V defaultValue, params K[] keys)
    {
      foreach (var key in keys) {
        if (dictionary.ContainsKey(key))
          return dictionary[key];
      }
      return defaultValue;
    }
  }
}
