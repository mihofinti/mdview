﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Util
{
  /// <summary>
  /// Quick and dirty parser for text with interspersed BBCode.
  /// 
  /// Intended to provide clean text as a first priority without any BBCode.
  /// As a secondary goal, record the extents of any BBCode so that in the future a
  /// RichTextBox could be used to render as it was originally intended.
  /// </summary>
  public class BBCode
  {
    /// <summary>
    /// Text with all BBCode removed
    /// </summary>
    public readonly string Text;

    /// <summary>
    /// List of BBCodes and the span of text affected.
    /// </summary>
    public readonly IEnumerable<Tag> Tags;

    public BBCode(string text)
    {
      var parser = new Parser();
      parser.ParseText(text);

      Text = parser.Text.ToString();
      Tags = parser.Tags;
    }

    /// <summary>
    /// BBCode tag and the text extent affected
    /// </summary>
    public class Tag
    {
      /// <summary>
      /// Starting index
      /// </summary>
      public int Start;

      /// <summary>
      /// Ending index
      /// </summary>
      public int End;
      
      /// <summary>
      /// Name of the tag
      /// </summary>
      public string Name;

      /// <summary>
      /// Any additional text between the tag name and the closing bracket
      /// </summary>
      public string Option;
    }

    private class Parser
    {
      private Stack<Tag> Stack = new Stack<Tag>();

      /// <summary>
      /// Text with all BBCode removed
      /// </summary>
      public StringBuilder Text = new StringBuilder();

      /// <summary>
      /// List of BBCode tags and what text they affect
      /// </summary>
      public List<Tag> Tags = new List<Tag>();

      /// <summary>
      /// Parse text containing BBCode
      /// </summary>
      /// <param name="bbtext"></param>
      public void ParseText(string bbtext)
      {
        // This is why we can't have nice things.
        int index = 0;

        while (index < bbtext.Length) {
          var nextIndex = bbtext.IndexOf('[', index);
          if (nextIndex < 0)
            nextIndex = bbtext.Length;

          if (nextIndex > index) {
            // text before the next tag
            // Also seeing a lot of HTML escapes, clean those up to
            var ampIndex = bbtext.IndexOf('&', index);
            if (ampIndex > index && ampIndex < nextIndex)
              nextIndex = ampIndex;

            if (ampIndex == index) {
              // Only consider this to be an HTML escape if there's no whitespace before the next ;
              var semiIndex = bbtext.IndexOf(';', index);
              if (semiIndex > 0 && semiIndex < nextIndex) {
                var escape = bbtext.Substring(index, semiIndex - index + 1);
                if (!escape.Any(Char.IsWhiteSpace)) {
                  var text = System.Web.HttpUtility.HtmlDecode(escape);
                  Text.Append(text);
                  index = semiIndex + 1;
                  continue;
                }
              }
            }

            Text.Append(bbtext.Substring(index, nextIndex - index));
            index = nextIndex;
            continue;
          }

          // At a tag
          var closeBrace = bbtext.IndexOf(']', index);
          var nextOpen = bbtext.IndexOf('[', index + 1);
          if (closeBrace < 0 || (nextOpen > 0 && nextOpen < closeBrace)) {
            // Looks like a typo, ignore it
            index += 1;
            continue;
          }

          // Looking for one of the following:
          //    [TAG]
          //    [/TAG]
          //    [TAG=OPTION]
          //    [TAG OPTION=OPTION]
          var tagData = bbtext.Substring(index + 1, closeBrace - index - 1);
          index = closeBrace + 1;

          // Handle the closing tag case
          if (tagData.StartsWith("/")) {
            tagData = tagData.Substring(1).ToLower();

            if (Stack.Any(t => t.Name == tagData)) {
              // If there's a matching tag in the stack, close it and all unclosed tags in between
              Tag endTag = Stack.Pop();
              while (endTag.Name != tagData) {
                CloseTag(endTag);
                endTag = Stack.Pop();
              }
              CloseTag(endTag);
            }
            else {
              // Otherwise ignore it
            }
            continue;
          }

          // New tag
          var tag = new Tag();
          tag.Start = Text.Length;
          // tag.End will be provided when we hit the closing tag

          var nextSpace = tagData.IndexOf(' ');
          if (nextSpace < 0)
            nextSpace = tagData.Length;
          var nextEq = tagData.IndexOf('=');
          if (nextEq < 0)
            nextEq = tagData.Length;

          var endOfName = Math.Min(nextSpace, nextEq);

          tag.Name = tagData.Substring(0, endOfName).ToLower();
          tag.Option = tagData.Substring(endOfName);

          Stack.Push(tag);
        }

        // Close off any open tags
        while (Stack.Any()) {
          CloseTag(Stack.Pop());
        }
      }

      /// <summary>
      /// Set the end point of a tag and add it to the list
      /// </summary>
      /// <param name="tag"></param>
      private void CloseTag( Tag tag )
      {
        // Special case, [URL=XXX] we want the XXX in the resulting text
        if (tag.Name == "url" && tag.Option.Length > 0) {
          Text.Append(" (" + tag.Option.Substring(1) + ") ");
        }

        tag.End = Text.Length;
        Tags.Add(tag);
      }
    }


  }

}
