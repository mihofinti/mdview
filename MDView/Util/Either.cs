﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Util
{
  /// <summary>
  /// Haskell-ish Either type (or generalization of the Nullable type to two possible content types).
  /// 
  /// Stores one of two possible values allowing for processing without specific knowledge of the content.
  /// </summary>
  /// <typeparam name="L">Left value type</typeparam>
  /// <typeparam name="R">Right value type</typeparam>
  public abstract class Either<L, R>
  {
    /// <summary>
    /// Return the LeftValue (or throws if Right)
    /// </summary>
    public abstract L LeftValue { get; }

    /// <summary>
    /// Return the RightValue (or throws if Left)
    /// </summary>
    public abstract R RightValue { get; }

    /// <summary>
    /// True if Left
    /// </summary>
    public abstract bool HasLeft { get; }

    /// <summary>
    /// True if Right
    /// </summary>
    public abstract bool HasRight { get; }

    /// <summary>
    /// Apply a function to the contained value
    /// </summary>
    /// <param name="doLeft"></param>
    /// <param name="doRight"></param>
    public void Bind(Action<L> doLeft, Action<R> doRight)
    {
      if (HasLeft)
        doLeft(LeftValue);
      else
        doRight(RightValue);
    }

    /// <summary>
    /// Process the contained value and return the result
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="doLeft"></param>
    /// <param name="doRight"></param>
    /// <returns></returns>
    public T Bind<T>(Func<L, T> doLeft, Func<R, T> doRight)
    {
      if (HasLeft)
        return doLeft(LeftValue);
      else
        return doRight(RightValue);
    }

    /// <summary>
    /// Process the contained value and return the result
    /// </summary>
    /// <typeparam name="L2"></typeparam>
    /// <typeparam name="R2"></typeparam>
    /// <param name="doLeft"></param>
    /// <param name="doRight"></param>
    /// <returns></returns>
    public Either<L2, R2> Bind<L2, R2>(Func<L, L2> doLeft, Func<R, R2> doRight)
    {
      if (HasLeft)
        return new Either<L2, R2>.Left(doLeft(LeftValue));
      else
        return new Either<L2, R2>.Right(doRight(RightValue));
    }

    /// <summary>
    /// Process the Left value
    /// </summary>
    /// <typeparam name="L2"></typeparam>
    /// <param name="doLeft"></param>
    /// <returns></returns>
    public Either<L2, R> BindLeft<L2>(Func<L, L2> doLeft)
    {
      return Bind(doLeft, r => r);
    }

    /// <summary>
    /// Process the right value
    /// </summary>
    /// <typeparam name="R2"></typeparam>
    /// <param name="doRight"></param>
    /// <returns></returns>
    public Either<L, R2> BindRight<R2>(Func<R, R2> doRight)
    {
      return Bind(l => l, doRight);
    }

    /// <summary>
    /// Left value
    /// </summary>
    public class Left : Either<L, R>
    {
      private readonly L Value;

      public override L LeftValue => Value;

      public override R RightValue => throw new ArgumentException();

      public override bool HasLeft => true;

      public override bool HasRight => false;

      public Left(L value) {
        Value = value;
      }

      public override string ToString()
      {
        return "Left." + Value.ToString();
      }
    }

    /// <summary>
    /// Right value
    /// </summary>
    public class Right : Either<L, R>
    {
      private readonly R Value;

      public override L LeftValue => throw new ArgumentException();

      public override R RightValue => Value;

      public override bool HasLeft => false;

      public override bool HasRight => true;

      public Right(R value)
      {
        Value = value;
      }

      public override string ToString()
      {
        return "Right." + Value.ToString();
      }
    }
  }
}
