﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Util
{
  public static class Function
  {
    /// <summary>
    /// Function composition
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <param name="f"></param>
    /// <param name="g"></param>
    /// <returns></returns>
    public static Func<A, C> Compose<A, B, C>(this Func<B, C> f, Func<A, B> g)
    {
      return a => f(g(a));
    }

    public static readonly Func<bool,bool> Not = a => !a;
  }
}
