﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView.Util
{
  /// <summary>
  /// General utility functions
  /// </summary>
  public static class Util
  {
    /// <summary>
    /// Count digits in a value.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static int DigitCount(int? value)
    {
      if (!value.HasValue || value.Value < 0)
        return 0;

      return value < 10 ? 1
        : value < 100 ? 2
        : value < 1000 ? 3
        : 4; // Good enough unless chinese webnovels get manga adaptions
    }

    /// <summary>
    /// Try to find a meaningful exception to report on the UI
    /// </summary>
    /// <param name="ex"></param>
    /// <returns></returns>
    public static Exception StripWrappers(this Exception ex)
    {
      var aex = ex as AggregateException;
      if (aex != null)
        return aex.Flatten().GetBaseException();
      else
        return ex;
    }

    /// <summary>
    /// Run a task in the background returning to the UI thread on completion.
    /// 
    /// Reports exceptions thrown in the background thread to the user, then invokes the onError action on the UI thread.
    /// </summary>
    /// <param name="control"></param>
    /// <param name="background"></param>
    /// <param name="foreground"></param>
    /// <param name="onError"></param>
    public static void InBackground(this Control control, Action background, Action foreground, Action onError = null)
    {
      // I really want macros.
      Task.Run(() => {
        try {
          background();
          if( !control.IsDisposed )
            control.Invoke(foreground);
        }
        catch (Exception ex) {
          ReportError(ex);
          if ( onError != null && !control.IsDisposed)
            control.Invoke(onError);
        }
      });
    }

    /// <summary>
    /// Run a task in the background forwarding the result to a UI thread action on completion.
    /// 
    /// Reports exceptions thrown in the background thread to the user, then invokes the onError action on the UI thread.
    /// </summary>
    /// <param name="control"></param>
    /// <param name="background"></param>
    /// <param name="foreground"></param>
    /// <param name="onError"></param>
    public static void InBackground<T>(this Control control, Func<T> background, Action<T> foreground, Action onError = null)
    {
      // I really want macros.
      Task.Run(() => {
        try {
          var result = background();
          if (!control.IsDisposed)
            control.Invoke(foreground, result);
        }
        catch (Exception ex) {
          ReportError(ex);
          if (onError != null && !control.IsDisposed)
            control.Invoke(onError);
        }
      });
    }

    /// <summary>
    /// Run a task in the background forwarding results generates as they complete to a UI thread action.
    /// 
    /// Reports exceptions thrown in the background thread to the user, then invokes the onError action on the UI thread.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="control"></param>
    /// <param name="background"></param>
    /// <param name="foreground"></param>
    /// <param name="onError"></param>
    public static void InBackground<T>(this Control control, Func<IEnumerable<T>> background, Action<T> foreground, Action onError = null)
    {
      Task.Run(() => {
        try {
          foreach (var result in background()) {
            if (!control.IsDisposed)
              control.Invoke(foreground, result);
            else
              break;
          }
        }
        catch (Exception ex) {
          ReportError(ex);
          if (onError != null && !control.IsDisposed)
            control.Invoke(onError);
        }
      });
    }

    /// <summary>
    /// Report error to the user and log
    /// </summary>
    /// <param name="ex"></param>
    public static void ReportError(Exception ex)
    {
      Console.Log(ex);
      MessageBox.Show(ex.StripWrappers().Message, "Error");
    }

    /// <summary>
    /// Unwrap async Task
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="task"></param>
    /// <returns></returns>
    public static T Sync<T>(this Task<T> task)
    {
      task.Wait();
      return task.Result;
    }
  }
}
