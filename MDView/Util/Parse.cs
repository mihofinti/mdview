﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Util
{
  public class Parse
  {
    /// <summary>
    /// Most Manga will have chapter numbers that look something like:
    ///   123,  123.1, 123.Omake, Oneshot
    /// Usually an initial number followed by some text that can usually be
    /// sorted with naive string comparison.
    /// 
    /// In most cases comparisons can be done with the ChapterNumber, ChapterSuffix values
    /// which also breaks out a numeric and text portion
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Tuple<int, string> ChapterNumber(string value)
    {
      if (value == null)
        return new Tuple<int, string>(-1, "");

      int idx = value.IndexWhere(Function.Not.Compose<char, bool, bool>(Char.IsDigit));
      if (idx < 0)
        idx = value.Length;

      if (idx == 0) {
        return new Tuple<int, string>(-1, value);
      }
      else {
        return new Tuple<int, string>(int.Parse(value.Substring(0, idx)), value.Substring(idx));
      }
    }
  }
}
