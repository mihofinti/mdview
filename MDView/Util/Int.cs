﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Util
{
  public static class Int
  {
    /// <summary>
    /// Clamp a value to a range
    /// </summary>
    /// <param name="value"></param>
    /// <param name="minimum"></param>
    /// <param name="maximum"></param>
    /// <returns></returns>
    public static int Clamp(this int value, int minimum, int maximum = int.MaxValue)
    {
      if (value < minimum)
        return minimum;
      else if (value > maximum)
        return maximum;
      else
        return value;
    }
  }
}
