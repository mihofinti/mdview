﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Util
{
  public static class Lists
  {
    /// <summary>
    /// Return the first index where condition is true
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="condition"></param>
    /// <returns></returns>
    public static int IndexWhere<T>(this IEnumerable<T> list, Func<T, bool> condition)
    {
      int index = 0;
      foreach (var item in list) {
        if (condition(item))
          return index;
        index++;
      }
      return -1;
    }

    /// <summary>
    /// Break a enumeration into subsets of length n
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="n"></param>
    /// <param name=""></param>
    /// <returns></returns>
    public static IEnumerable<IEnumerable<T>> TakeSubsets<T>(int n, IEnumerable<T> list ) {
      while (list.Any()) {
        yield return list.Take(n);
        list = list.Skip(n);
      }
    }

    /// <summary>
    /// Find the maximum element of a list given a specific comparison method.
    /// 
    /// Returns the default value for an empty list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="comparer"></param>
    /// <returns></returns>
    public static T Max<T>(this IEnumerable<T> list, IComparer<T> comparer)
    {
      var iter = list.GetEnumerator();
      if (!iter.MoveNext())
        return default(T);

      var maximum = iter.Current;
      while (iter.MoveNext()) {
        if (comparer.Compare(maximum, iter.Current) < 0)
          maximum = iter.Current;
      }

      return maximum;
    }
  }
}
