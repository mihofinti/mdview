﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDView.Util
{
  /// <summary>
  /// Utility functions around nullable values
  /// </summary>
  public static class Maybe
  {
    /// <summary>
    /// Compare two nullable values sorting null as the minimum
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="left"></param>
    /// <param name="right"></param>
    /// <returns></returns>
    public static int Compare<T>(Nullable<T> left, Nullable<T> right) where T : struct, IComparable
    {
      if (left == null && right == null)
        return 0;
      if (left == null)
        return -1;
      if (right == null)
        return 1;
      return left.Value.CompareTo(right.Value);
    }

    /// <summary>
    /// Return maximum of a list of values with out default
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="minimum"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    public static Nullable<T> Maximum<T>(params Nullable<T>[] values) where T : struct, IComparable
    {
      Nullable<T> maximum = null;

      foreach (var value in values) {
        if (value.HasValue) {
          if (maximum.HasValue && maximum.Value.CompareTo(value.Value) < 0)
            maximum = value;
          else
            maximum = value;
        }
      }

      return maximum;
    }

    /// <summary>
    /// Return maximum of a list of values with default
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="minimum"></param>
    /// <param name="values"></param>
    /// <returns></returns>
    public static T Maximum<T>(T minimum, params Nullable<T>[] values) where T : struct, IComparable
    {
      var maximum = minimum;

      foreach (var value in values) {
        if (value.HasValue && value.Value.CompareTo(maximum) > 0)
          maximum = value.Value;
      }

      return maximum;
    }
  }
}
