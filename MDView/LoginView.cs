﻿using MDView.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView
{
  /// <summary>
  /// Login Screen
  /// </summary>
  public partial class LoginView : UserControl, INavigableControl
  {
    public Control Control => this;

    public String Title => "Login";

    public event NavigationEventHandler NavigationRequested;

    public LoginView()
    {
      InitializeComponent();

      DataPathEntry.Text = Properties.Settings.Default.ConfigurationPath ?? "mdview.db";
      ProxySocks5.Checked = Properties.Settings.Default.ProxyEnabled;
      ProxyAddressEntry.Text = Properties.Settings.Default.ProxyAddress ?? "";

      if (Properties.Settings.Default.ProxyUse == (int)MangaDex.UseProxy.Always)
        UseProxyAlways.Checked = true;
      else
        UseProxyMangadex.Checked = true;

      ProxySocks5_CheckedChanged(this, null);
    }

    private void LoginView_Load(object sender, EventArgs e)
    {
      // Most of the time the database path won't be touched
      UserNamePrompt.Focus();
    }

    /// <summary>
    /// Perform login, moving the followed manga list on success
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Login_Click(object sender, EventArgs e)
    {
      LayoutPane.Enabled = false;

      var userName = UserNameEntry.Text;
      var password = PasswordEntry.Text;

      string proxyAddress = null;
      int proxyPort = 0;
      MangaDex.UseProxy useProxy = UseProxyAlways.Checked ? MangaDex.UseProxy.Always : MangaDex.UseProxy.Mangadex;

      if (ProxySocks5.Checked) {
        string[] proxyInfo = ProxyAddressEntry.Text.Split(':');

        if (proxyInfo.Length != 2 || !int.TryParse( proxyInfo[1], out proxyPort) ) {
          MessageBox.Show("Proxy address not in expected format:\n  {address}:{port}", "Error");
          return;
        }
        proxyAddress = proxyInfo[0];
      }

      Properties.Settings.Default.ConfigurationPath = DataPathEntry.Text;
      Properties.Settings.Default.ProxyEnabled = ProxySocks5.Checked;
      Properties.Settings.Default.ProxyAddress = ProxyAddressEntry.Text;
      Properties.Settings.Default.ProxyUse = (int)useProxy;
      Properties.Settings.Default.Save();

      this.InBackground(
        () => Cache.Connect(userName, password, useProxy, proxyAddress, proxyPort),
        () => {
          LayoutPane.Enabled = true;
          NavigationRequested(this, new NavigationEventArgs(new MangaListView()));
        },
        () => LayoutPane.Enabled = true
        );
    }


    public void OnShow()
    {
      // No-op
    }

    private void ProxySocks5_CheckedChanged(object sender, EventArgs e)
    {
      ProxyAddressEntry.Enabled = ProxySocks5.Checked;
      ProxyAddressPrompt.Enabled = ProxySocks5.Checked;
      ProxyUsePanel.Enabled = ProxySocks5.Checked;
    }
  }
}
