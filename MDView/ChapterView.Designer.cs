﻿
namespace MDView
{
  partial class ChapterView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ViewPane = new System.Windows.Forms.FlowLayoutPanel();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.PrevPage = new System.Windows.Forms.Button();
      this.LoadStatus = new System.Windows.Forms.Label();
      this.NextPage = new System.Windows.Forms.Button();
      this.PageList = new System.Windows.Forms.ComboBox();
      this.ViewStyleSelection = new System.Windows.Forms.CheckBox();
      this.FitScreenWidth = new System.Windows.Forms.CheckBox();
      this.PageScroll = new System.Windows.Forms.VScrollBar();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // ViewPane
      // 
      this.ViewPane.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.ViewPane.AutoScroll = true;
      this.ViewPane.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
      this.ViewPane.Location = new System.Drawing.Point(0, 0);
      this.ViewPane.Name = "ViewPane";
      this.ViewPane.Size = new System.Drawing.Size(598, 369);
      this.ViewPane.TabIndex = 0;
      this.ViewPane.WrapContents = false;
      this.ViewPane.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ViewPane_PreviewKeyDown);
      this.ViewPane.Resize += new System.EventHandler(this.ViewPane_Resize);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.AutoSize = true;
      this.tableLayoutPanel1.ColumnCount = 7;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
      this.tableLayoutPanel1.Controls.Add(this.PrevPage, 4, 0);
      this.tableLayoutPanel1.Controls.Add(this.LoadStatus, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.NextPage, 6, 0);
      this.tableLayoutPanel1.Controls.Add(this.PageList, 5, 0);
      this.tableLayoutPanel1.Controls.Add(this.ViewStyleSelection, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.FitScreenWidth, 2, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 371);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
      this.tableLayoutPanel1.Size = new System.Drawing.Size(598, 29);
      this.tableLayoutPanel1.TabIndex = 1;
      // 
      // PrevPage
      // 
      this.PrevPage.Location = new System.Drawing.Point(478, 3);
      this.PrevPage.Name = "PrevPage";
      this.PrevPage.Size = new System.Drawing.Size(27, 23);
      this.PrevPage.TabIndex = 2;
      this.PrevPage.Text = "◀";
      this.PrevPage.UseVisualStyleBackColor = true;
      this.PrevPage.Click += new System.EventHandler(this.PrevPage_Click);
      // 
      // LoadStatus
      // 
      this.LoadStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.LoadStatus.AutoSize = true;
      this.LoadStatus.Location = new System.Drawing.Point(3, 8);
      this.LoadStatus.Name = "LoadStatus";
      this.LoadStatus.Size = new System.Drawing.Size(66, 13);
      this.LoadStatus.TabIndex = 0;
      this.LoadStatus.Text = "Loaded: 0/?";
      // 
      // NextPage
      // 
      this.NextPage.Location = new System.Drawing.Point(568, 3);
      this.NextPage.Name = "NextPage";
      this.NextPage.Size = new System.Drawing.Size(27, 23);
      this.NextPage.TabIndex = 1;
      this.NextPage.Text = "▶";
      this.NextPage.UseVisualStyleBackColor = true;
      this.NextPage.Click += new System.EventHandler(this.NextPage_Click);
      // 
      // PageList
      // 
      this.PageList.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.PageList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.PageList.FormattingEnabled = true;
      this.PageList.Location = new System.Drawing.Point(511, 4);
      this.PageList.Name = "PageList";
      this.PageList.Size = new System.Drawing.Size(51, 21);
      this.PageList.TabIndex = 3;
      this.PageList.SelectedIndexChanged += new System.EventHandler(this.PageList_SelectedIndexChanged);
      // 
      // ViewStyleSelection
      // 
      this.ViewStyleSelection.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.ViewStyleSelection.AutoSize = true;
      this.ViewStyleSelection.Location = new System.Drawing.Point(75, 6);
      this.ViewStyleSelection.Name = "ViewStyleSelection";
      this.ViewStyleSelection.Size = new System.Drawing.Size(109, 17);
      this.ViewStyleSelection.TabIndex = 4;
      this.ViewStyleSelection.Text = "View Single Page";
      this.ViewStyleSelection.UseVisualStyleBackColor = true;
      this.ViewStyleSelection.CheckedChanged += new System.EventHandler(this.ViewStyleSelection_CheckedChanged);
      // 
      // FitScreenWidth
      // 
      this.FitScreenWidth.Anchor = System.Windows.Forms.AnchorStyles.Left;
      this.FitScreenWidth.AutoSize = true;
      this.FitScreenWidth.Checked = true;
      this.FitScreenWidth.CheckState = System.Windows.Forms.CheckState.Checked;
      this.FitScreenWidth.Location = new System.Drawing.Point(190, 6);
      this.FitScreenWidth.Name = "FitScreenWidth";
      this.FitScreenWidth.Size = new System.Drawing.Size(117, 17);
      this.FitScreenWidth.TabIndex = 5;
      this.FitScreenWidth.Text = "Fit to Screen Width";
      this.FitScreenWidth.UseVisualStyleBackColor = true;
      this.FitScreenWidth.CheckedChanged += new System.EventHandler(this.FitScreenWidth_CheckedChanged);
      // 
      // PageScroll
      // 
      this.PageScroll.Dock = System.Windows.Forms.DockStyle.Right;
      this.PageScroll.Location = new System.Drawing.Point(581, 0);
      this.PageScroll.Name = "PageScroll";
      this.PageScroll.Size = new System.Drawing.Size(17, 371);
      this.PageScroll.TabIndex = 2;
      this.PageScroll.ValueChanged += new System.EventHandler(this.PageScroll_ValueChanged);
      // 
      // ChapterView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.PageScroll);
      this.Controls.Add(this.tableLayoutPanel1);
      this.Controls.Add(this.ViewPane);
      this.Name = "ChapterView";
      this.Size = new System.Drawing.Size(598, 400);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.FlowLayoutPanel ViewPane;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label LoadStatus;
    private System.Windows.Forms.Button NextPage;
    private System.Windows.Forms.Button PrevPage;
    private System.Windows.Forms.ComboBox PageList;
    private System.Windows.Forms.CheckBox ViewStyleSelection;
    private System.Windows.Forms.CheckBox FitScreenWidth;
    private System.Windows.Forms.VScrollBar PageScroll;
  }
}
