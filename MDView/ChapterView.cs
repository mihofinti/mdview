﻿using MDView.Config;
using MDView.MangaDex;
using MDView.Util;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MDView
{
  /// <summary>
  /// Control to view a chapter of a Manga.
  /// 
  /// On creation, loads the pages in the background making them available on completion.
  /// </summary>
  public partial class ChapterView : UserControl, INavigableControl
  {
    public override string ToString() => "Chapter " + Chapter.Id;

    /// <summary>
    /// Manga being viewed
    /// </summary>
    private UI.MangaRecord Record;

    /// <summary>
    /// Chapter being viewed
    /// </summary>
    private ObjectData<Chapter> Chapter;

    /// <summary>
    /// Loaded pages
    /// </summary>
    private List<PictureBox> Pages = new List<PictureBox>();

    /// <summary>
    /// Currently displayed page index
    /// </summary>
    private int PageIndex;

    /// <summary>
    /// true = view one page at a time
    /// false = view all pages at once
    /// </summary>
    private bool ViewSinglePages;

    public Control Control => this;

    public String Title => Record.Title + " - Chapter " + Chapter.Attributes.ChapterApi;

    public event NavigationEventHandler NavigationRequested;

    /// <summary>
    /// U+2007 spaces equal to the maximum chapter number width
    /// </summary>
    private string DigitPadding;

    /// <summary>
    /// Vertical offsets of the divisions between each page
    /// </summary>
    private int[] PageOffsets;

    /// <summary>
    /// Create control and begin loading chapter
    /// </summary>
    /// <param name="chapter"></param>
    public ChapterView(UI.MangaRecord record, ObjectData<Chapter> chapter )
    {
      Record = record;
      Chapter = chapter;

      InitializeComponent();
      ViewPane.VerticalScroll.Maximum = 0;
      ViewPane.VerticalScroll.Enabled = false;
      ViewPane.VerticalScroll.Visible = false;
      
      // Doesn't appear to be any indication of the format in the manga details,
      // so we'll just assume that only Korean-language material is long-form
      ViewSinglePages = Record.Manga.Attributes.OriginalLanguage != "ko";
      ViewStyleSelection.Checked = ViewSinglePages;

      ViewPane.MouseEnter += (s, e) => PageScroll.Focus();  // Give this focus so that mouse wheel events are sent there

      PageList.Items.Clear();

      DigitPadding = new string('\u2007', Chapter.Attributes.Pages.ToString().Length);

      this.InBackground(
        GetPages,
        ShowPage);
    }

    private IEnumerable<Page> GetPages()
    {
      var server = Cache.Service.GetServerForChapter(Chapter.Id);

      PageOffsets = new int[server.Chapter.Data.Length];
      for (int i = 0; i < PageOffsets.Length; i++)
        PageOffsets[i] = int.MaxValue;

      foreach (var pageId in server.Chapter.Data) {
        var page = Cache.Service.GetPage(server, pageId);
        Cache.Service.NetworkStatus(page);
        yield return page;
      }
    }

    /// <summary>
    /// Add a newly loaded page
    /// </summary>
    /// <param name="chapter"></param>
    /// <param name="page"></param>
    private void ShowPage(Page page)
    {
      var pictureBox = new PictureBox();
      pictureBox.Image = page.Image;
      pictureBox.Click += NextPage_Click;
      pictureBox.Padding = Padding.Empty;
      pictureBox.Margin = Padding.Empty;
      SetPageSize(pictureBox);

      Pages.Add(pictureBox);

      if (Pages.Count == 1 && Record.Follow != null) {
        // Successful load of the first page
        Cache.Database.ReadFollow(Record.Follow, Record, Chapter, false);
      }

      if (Pages.Count == 1 || (!ViewSinglePages && Pages.Count < PageIndex + 3 ) ) {
        // Either the first page, or we're viewing all pages at once and this falls in the
        // subset of three pages being displayed at present
        ViewPane.Controls.Add(pictureBox);
      }

      if (Pages.Count == 1)
        PageOffsets[0] = page.Image.Height;
      else {
        PageOffsets[Pages.Count - 1] = PageOffsets[Pages.Count - 2] + page.Image.Height;
      }

      // Update the maximum extent of the scroll bar used in all pages mode
      if (ViewSinglePages && Pages.Count == 1)
        PageScroll.Maximum = page.Image.Height;
      else if( !ViewSinglePages )
        PageScroll.Maximum = PageOffsets[Pages.Count - 1];

      var pageNo = DigitPadding + Pages.Count.ToString();
      pageNo = pageNo.Substring(pageNo.Length - DigitPadding.Length);
      PageList.Items.Add(pageNo);

      LoadStatus.Text = String.Format("Loaded: {0}/{1}", Pages.Count, Chapter.Attributes.Pages);
    }

    /// <summary>
    /// Advance page (if loaded) or return to chapter list at end of chapter
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void NextPage_Click(object sender, EventArgs e)
    {
      // The mouse 'Back' button
      var me = e as MouseEventArgs;
      if (me != null && me.Button == MouseButtons.XButton1) {
        NavigationRequested(this, new NavigationEventArgs(NavigationDestination.Back));
        return;
      }

      if (PageIndex + 1 < Pages.Count) {
        ShowPageNumber(PageIndex + 1);
      }
      else if( PageIndex + 1 == Chapter.Attributes.Pages) {
        NavigationRequested(this, new NavigationEventArgs(NavigationDestination.Back));
      }
    }

    /// <summary>
    /// Move to previous page if not at the first
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void PrevPage_Click(object sender, EventArgs e)
    {
      if (PageIndex > 0) {
        ShowPageNumber(PageIndex - 1);
      }
    }

    /// <summary>
    /// Move to a specific page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void PageList_SelectedIndexChanged(object sender, EventArgs e)
    {
      // Already displaying this page
      if (PageIndex == PageList.SelectedIndex)
        return;

      ShowPageNumber(PageList.SelectedIndex);
    }

    /// <summary>
    /// Display a specific page
    /// </summary>
    /// <param name="index"></param>
    private void ShowPageNumber(int index)
    {
      PageIndex = index;
      PageList.SelectedIndex = PageIndex;

      if (ViewSinglePages) {
        // Swap out the image
        ViewPane.Controls.Clear();
        ViewPane.Controls.Add(Pages[PageIndex]);

        // Reset view to top left
        PageScroll.Maximum = Pages[PageIndex].Height;
        PageScroll.Value = 0;
      }
      else {
        // Scroll the specified image into position
        PageScroll.Value = PageOffsets[PageIndex == 0 ? 0 : PageIndex - 1];
      }

      PageScroll.Focus();
    }

    public void OnShow()
    {
      // Set scroll increments
      PageScroll.SmallChange = ViewPane.Height / 10;
      PageScroll.LargeChange = ViewPane.Height;
    }

    private void ViewStyleSelection_CheckedChanged(object sender, EventArgs e)
    {
      ViewSinglePages = ViewStyleSelection.Checked;
      if (ViewSinglePages) {
        // What page is currently in view?
        var page = ViewPane.GetChildAtPoint(new Point(1, 1)) as PictureBox;
        if (page != null)
          PageIndex = Pages.IndexOf(page);
        if (PageIndex < 0)
          PageIndex = 0;

        // Single page
        ViewPane.Controls.Clear();

        if (Pages.Any()) {
          ViewPane.Controls.Add(Pages[PageIndex]);
          PageScroll.Value = 0;
          PageScroll.Maximum = Pages[PageIndex].Height;
        }
      }
      else {
        // All pages
        ViewPane.Controls.Clear();

        foreach( var page in Pages )
          ViewPane.Controls.Add(page);

        PageScroll.Maximum = PageOffsets[PageOffsets.Length - 1];
        PageScroll.Value = PageIndex == 0 ? 0 : PageOffsets[PageIndex - 1];
      }

      PageScroll.Focus();
    }

    private void ViewPane_PreviewKeyDown(object sender, PreviewKeyDownEventArgs eventArgs)
    {
      // Let keypress pass through for other controls
      if (!ViewPane.Focused)
        return;

      // Scroll
      if (eventArgs.KeyCode == Keys.PageUp) {
        PerformScroll(true, -1);
      }
      else if (eventArgs.KeyCode == Keys.PageDown) {
        PerformScroll(true, 1);
      }

      // TODO: Widows forms is using the arrow keys for navigation
      // which causes the ViewPane to lose focus and makes using
      // them to scroll or advance pages tricky.  Would be useful
      // to override this behavior if possible
    }

    private void PerformScroll(bool isLargeScroll, int scrollDirection ) {
      var position = PageScroll.Value;
      position += scrollDirection * (isLargeScroll ? PageScroll.LargeChange : PageScroll.SmallChange);

      if (position < 0)
        position = 0;
      else if(position > PageScroll.Maximum )
        position = PageScroll.Maximum;

      PageScroll.Value = position;
    }

    /// <summary>
    /// Resize all images when the screen width setting is changed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void FitScreenWidth_CheckedChanged(object sender, EventArgs e)
    {
      foreach (var page in Pages)
        SetPageSize(page);
    }

    /// <summary>
    /// Size the picturebox according to the setting of the FitScreenWidth setting
    /// </summary>
    /// <param name="pictureBox"></param>
    private void SetPageSize(PictureBox pictureBox)
    {
      if (FitScreenWidth.Checked) {
        var panelWidth = ViewPane.Width - SystemInformation.VerticalScrollBarWidth;

        if (pictureBox.Image.Width < panelWidth) {
          // Already fits within the screen
          pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
        }
        else {
          // Needs to zoom out
          pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
          var factor = (double)panelWidth / (double)pictureBox.Image.Width;
          pictureBox.Size = new Size(panelWidth, (int)(factor * pictureBox.Image.Height));
        }
      }
      else {
        // No resizing has been requested
        pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
      }
    }

    /// <summary>
    /// Resize all images when the screen width is changed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ViewPane_Resize(object sender, EventArgs e)
    {
      foreach (var page in Pages)
        SetPageSize(page);
    }

    private void PageScroll_ValueChanged(object sender, EventArgs e)
    {
      if (ViewSinglePages) {
        // Trivial case, set the scroll to match
        ViewPane.AutoScrollPosition = new Point(0, PageScroll.Value);
        //ViewPane.VerticalScroll.Value = PageScroll.Value.Clamp( ViewPane.VerticalScroll.Minimum, ViewPane.VerticalScroll.Maximum );
        return;
      }

      // PageView will always contain a set of three images (since I've seen some
      // series in the wild that have very short pages, but none that would make
      // it worth complicating this code to support N pages at a time.
      //
      // +--------+
      // | Page 2 |  +------+
      // +--------+  | View |
      // | Page 3 |  |      |
      // +--------+  | Port |
      // | Page 4 |  +------+
      // +--------+
      //
      // When the top of the view port moves past the bottom of Page 2, we then unload page 2
      // and load Page 4.  And the reverse when it hits the top of Page 2.
      //
      // TODO to make this smoother, should probably load/unload only when the viewport reaches the bottom
      // of Page 4 so there's some buffer and the user can't go back and forth over the threshold.

      // Find what page the scroll bar is in the middle of
      int pageNum = 0;
      while (PageOffsets[pageNum] < PageScroll.Value)
        pageNum++;

      if (PageIndex != pageNum ) {
        // User scrolled forward/back a page
        PageIndex = pageNum;

        ViewPane.Controls.Clear();
        ViewPane.Controls.Add(Pages[PageIndex]);
        if (PageIndex + 1 < Pages.Count)
          ViewPane.Controls.Add(Pages[PageIndex + 1]);
        if (PageIndex + 2 < Pages.Count)
          ViewPane.Controls.Add(Pages[PageIndex + 2]);
      }

      // Adjust the scroll within the viewport
      if (PageIndex == 0)
        ViewPane.AutoScrollPosition = new Point(0, PageScroll.Value);
      else
        ViewPane.AutoScrollPosition = new Point(0, PageScroll.Value - PageOffsets[PageIndex - 1]);
    }
  }
}
