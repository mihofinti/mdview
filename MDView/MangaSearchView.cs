﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MDView.MangaDex;
using MDView.Util;

namespace MDView
{
  /// <summary>
  /// Collect search query for manga
  /// </summary>
  public partial class MangaSearchView : UserControl, INavigableControl
  {
    public MangaSearchView()
    {
      InitializeComponent();
    }

    public Control Control => this;

    public String Title => "Search";

    public event NavigationEventHandler NavigationRequested;

    public void OnShow()
    {
    }

    /// <summary>
    /// Perform the search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Search_Click(object sender, EventArgs e)
    {
      if (!String.IsNullOrWhiteSpace(MangaDexIdEntry.Text)) {
        // Search based on ID
        Either<int, Guid> id;
        try {
          id = Service.ParseUriOrId(MangaDexIdEntry.Text);
        }
        catch {
          MessageBox.Show("ID could not be parsed.");
          return;
        }

        if( id.HasLeft ) {
          this.InBackground(
            () => {
              var guid = Cache.Service.TranslateLegacyId(MangaDex.ObjectType.Manga, id.LeftValue);
              return Cache.Service.GetManga(guid);
            },
            m => {
              NavigationRequested(this, new NavigationEventArgs(new MangaListView(new ObjectData<Manga>[] { m })));
            }
          );
        }
        else {
          this.InBackground(
            () => Cache.Service.GetManga(id.RightValue),
            m => NavigationRequested(this, new NavigationEventArgs(new MangaListView(new ObjectData<Manga>[] { m })))
          );
        }
      }
      else {
        // Search based on criteria
        var query = new Manga.Search();

        if (!String.IsNullOrWhiteSpace(SearchTitleEntry.Text))
          query.Add(Manga.Search.Title, SearchTitleEntry.Text);

        this.InBackground(
          () => Cache.Service.Search(query),
          r => {
            if (r.Any())
              NavigationRequested(this, new NavigationEventArgs(new MangaListView(r)));
            else
              MessageBox.Show("No matches found.");
          });
      }
    }
  }
}
