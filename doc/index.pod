=pod

=head1 MDView - A simple viewer for the MangaDex API

MDView is a Windows/.NET client around the new MangaDex API.
It is tailored to my specific use case of managing a reading list from MangaDex bookmarks and tracking updates.
But it also suitable for just reading manga until the official site is back.


=head2 Flow

Using a search or from an old MangaDex URL, you can identify and B<Follow> a series.
The B<Follow> can then be added to one or more B<List> to further categorize it.
The B<Follows> and B<Lists> are stored in the B<Database File>.
You can take the B<Database File> with you, back it up, edit it with your favorite SQLite tools.


=head2 Screen Format

At the top of the screen is the tool bar.
This remains consistent for all screens in the application.
The remainder of the screen is the particular view you have navigated to.

=Image MDView/Resources/LeftArrow.png

Returns to the previous screen.

=Image MDView/Resources/FollowList.png

Opens the B<Follow List> screen allowing you to create lists and view the followed manga in that list.
You can also move follows in bulk between lists.

=Image MDView/Resources/Search.png

Opens the B<Search> screen allowing you to find new manga on MangaDex


=head2 Login Screen

=Image doc/login.png

Provide the name of the desired B<Database File>.
The name of the last-used file is saved.

Optionally provide your MangaDex credentials.
In the past I have found some features to be unavailable without a valid login.
But basic features like checking the chapter list or description of a manga should still work.

Optionally provide proxy server details as K<address:port>.
The specified SOCKS v5 proxy will be used for connections to the MangaDex API.
If you need to save bandwidth, and do not have issues accessing the MD@H servers, the proxy can be limited to just connections to mangadex.org.

Selecting K<Login> will open the Manga List.


=head2 Manga List

=Image doc/mangalist.png

Displays the contents of a B<Follow List> along with the date of the last update on MangaDex.
If a manga has been updated since the last time you have viewed a chapter, it is flagged as T<New>.
B<Follows> that don't belong to any particular list are shown by default; you can switch to another list via the menu at the bottom right.

Right-Click on any of the entries displays the context menu.
This allows you to K<Delete> a B<Follow> completely.
K<Move> a B<Follow> to a different list (and removing it from any other list it is on).
Or K<Copy> a B<Follow> to a different list also keeping it on the current list.

Double-Click on any of the entries displays the B<Chapter List>.

Selecting K<Import> will prompt for a text file.
Each line of this file must contain an URL from the old MangaDex site or a MangaDex ID.
The specified manga will be added to the currently selected list.

Selecting K<Refresh> will check Mangadex for new chapters.

=head2 Chapter List

=Image doc/chapterlist.png

Displays all translated chapters of a selected manga.
The last chapter you have read is marked as T<Last>, and chapters updated after you last read the manga are marked as T<New>.

Select K<Detail> at the bottom left to view the manga in the B<Manga Detail> screen.

At the bottom right is a list of all new manga from the viewed manga list (or all results from the search screen) this screen was opened from.
Selecting a manga or using the left or right arrow buttons can be used to switch to the next manga without returning to the previous screen.

Double-Click any chapter opens it in the B<Reader> screen.


=head2 Manga Detail

=Image doc/mangadetail.png

Displays information from the MangaDex database about the manga such as alternate title translations and genre tags.
This also includes links to external websites like MangaUpdates or an official translation.
A bar chart of the chapters updated in the past 24 weeks and 24 months is also displayed.


=head2 Reader

=Image doc/pageview.png

Displays pages from the selected chapter.
This may take some time to load (API is rate limited); the current loading status is displayed at the bottom of the screen.

By default, Korean-language series are displayed in long form (all images displayed in a vertical strip).
All others are displayed as individual pages.
This can be switched with the "View Single Page" checkbox.

Clicking on the image advances to the next page.
After the final page, you are returned to the B<Chapter List>.
The current page is displayed at the bottom of the screen.


=head2 Search

=Image doc/search.png

Enter criteria to search for and matching manga will be displayed in the B<Manga List> screen.
This can then be used as normal to view more details or read chapters.
If you want to save the manga for later, the Right-Click context menu works as normal to add it to a list.


